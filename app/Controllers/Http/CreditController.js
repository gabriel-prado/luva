'use strict'

const CompanyModel = use('App/Models/Company')

class CreditController {

  static get inject () {
    return ['App/Services/Credit']
  }

  constructor (CreditService) {
    this.creditService = CreditService
  }

  async request({ request, auth, params }) {
    const body = request.body
    const companyId = Number(params.company_id)

    await this.creditService.request(body.value, companyId, auth.user)
  }

  async add ({ request, params }) {
    const { value, expire } = request.all()
    const company = await CompanyModel.findOrFail(Number(params.company_id))

    return this.creditService.add(company, value, expire)
  }

  async balance ({ params, auth }) {
    try {
      return await this.creditService.balance(Number(params.company_id), auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

}

module.exports = CreditController
