'use strict'

class TransactionController {

  static get inject () {
    return ['App/Services/Transaction']
  }

  constructor (TransactionService) {
    this.transactionService = TransactionService
  }

  async index({ request, auth, params }) {
    const filter = request.get()
    const companyId = Number(params.company_id)

    const page = (filter.all) ? null : Number(filter.page) || 1
    const perPage = (filter.all) ? null : Number(filter.perPage) || 5

    filter.order_by = filter.order_by || 'id'
    filter.direction = filter.direction || 'ASC'

    try {
      return await this.transactionService.list(page, perPage, filter, auth.user, companyId, filter.direction, filter.order_by)
    } catch(e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

}

module.exports = TransactionController
