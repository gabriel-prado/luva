'use strict'

const moment = use('moment')

const SolicitationHook = module.exports = {}

/**
 * Fill file fields.
 *
 * @method
 *
 * @param  {Object} solicitationInstance
 *
 * @return {void}
 */

SolicitationHook.setSlaStart = async (solicitationInstance) => {
  solicitationInstance.sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
}
