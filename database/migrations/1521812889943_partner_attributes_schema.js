'use strict'

const Schema = use('Schema')

class PartnerAttributesSchema extends Schema {
  up () {
    this.table('partners', (table) => {
      table.string('rg', 20)
      table.specificType('files', 'JSON')
      table.specificType('specialty', 'JSON')
      table.specificType('address', 'JSON')
      table.specificType('contact', 'JSON')
      table.specificType('bank', 'JSON')
      table.specificType('complementary_data', 'JSON')
    })
  }

  down () {
    this.table('partners', (table) => {
      table.dropColumn('rg')
      table.dropColumn('files')
      table.dropColumn('specialty')
      table.dropColumn('address')
      table.dropColumn('contact')
      table.dropColumn('bank')
      table.dropColumn('complementary_data')
    })
  }
}

module.exports = PartnerAttributesSchema
