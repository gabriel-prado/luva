'use strict'

const Route = use('Route')

Route
  .post('partner', 'UserController.storePartner')
  .validator('StorePartnerUser')

Route
  .get('partner/:id/company', 'PartnerController.fetchCompanies')
  .middleware(['auth', 'acl:PARTNER'])

Route
  .resource('partner', 'PartnerController')
  .only(['index', 'update', 'show'])
  .middleware(new Map([
    [['partner.index'], ['auth', 'acl:ADMIN', 'access:USER']],
    [['partner.show'], ['auth', 'acl:ADMIN,PARTNER', 'access:USER']],
    [['partner.update'], ['auth', 'acl:ADMIN,PARTNER', 'access:USER']]
  ]))
  .validator(new Map([
    [['partner.update'], ['UpdatePartnerUser']]
  ]))

Route.group('Partner File', () => {
  Route.post('', 'PartnerController.uploadFile')
  Route.get('', 'PartnerController.getFile')
}).prefix('partner/:id/file').middleware(['auth', 'acl:ADMIN,PARTNER', 'access:USER'])
