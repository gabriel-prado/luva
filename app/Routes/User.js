'use strict'

const Route = use('Route')

Route
  .put('me', 'UserController.update')
  .middleware(['auth', 'acl:ADMIN,CLIENT'])
  .validator('UpdateUser')

Route.group('User', () => {
  Route.get('notification', 'UserController.notification').middleware(['auth', 'acl:ADMIN,CLIENT,PARTNER'])
  Route.post('contact', 'UserController.contact').validator('ContactUser')
}).prefix('user/')

Route.group('Admin', () => {
  Route.get('', 'UserController.listAdmins')
  Route.get('/:id', 'UserController.showAdmin')
  Route.post('', 'UserController.createAdmin').validator('StoreAdminUser')
  Route.put('/:id', 'UserController.updateAdmin').validator('UpdateUser')
}).prefix('admin').middleware(['auth', 'acl:ADMIN', 'access:ADMIN'])
