'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Balance - GET /company/id/credit')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make()
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Transaction').create({
    operation: 'ADD',
    value: 300,
    expire: true,
    balance_expiring: 300,
    balance_not_expiring: 12,
    company_id: defaultCompany.id
  })
})

test('should return balance for admin user and get 200', async ({ client, assert}) => {
  const response =
    await
      client
        .get(`company/${defaultCompany.id}/credit`)
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.credit, 312)
})

test('should try return balance for non-existent company and get 404', async ({ client, assert}) => {
  const response =
    await
      client
        .get(`company/12/credit`)
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  assert.equal(response.status, 404)
})

test('should return balance for client user and get 200', async ({ client, assert}) => {
  const response =
    await
      client
        .get(`company/${defaultCompany.id}/credit`)
        .query({company_id: 1})
        .loginVia(defaultClient)
        .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.credit, 312)
})

test('should try return balance for another client user and get 401', async ({ client, assert}) => {
  const company = await Factory.model('App/Models/Company').create()
  const response =
    await
      client
        .get(`company/${company.id}/credit`)
        .query({company_id: 1})
        .loginVia(defaultClient)
        .end()

  assert.equal(response.status, 401)
  assert.equal(response.body.message, 'COMPANY_DOES_NOT_BELONG_TO_USER')
})

test('should try get balance as PARTNER and get 401 - Unauthorized', async({ client, assert}) => {
  const partnerUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })

  const response =
    await
      client
        .get(`company/${defaultCompany.id}/credit`)
        .query({company_id: 1})
        .loginVia(partnerUser)
        .end()

  assert.equal(response.status, 401)
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response =
    await
      client
        .get(`company/${defaultCompany.id}/credit`)
        .query({company_id: 1})
        .end()

  assert.equal(response.status, 401)
})
