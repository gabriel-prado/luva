'use strict'

const Schema = use('Schema')

class AddTagToSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.specificType('tags', 'JSON')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('tags')
    })
  }
}

module.exports = AddTagToSolicitationSchema
