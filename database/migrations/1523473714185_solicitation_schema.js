'use strict'

const Schema = use('Schema')

class SolicitationSchema extends Schema {
  up () {
    this.create('solicitations', (table) => {
      table.increments()
      table.string('name', 80).notNullable()
      table.integer('client_id').notNullable().unsigned()
      table.foreign('client_id').references('users.id')
      table.integer('partner_id').unsigned()
      table.foreign('partner_id').references('partners.id')
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.float('demand_value')
      table.specificType('client_files', 'JSON')
      table.specificType('partner_files', 'JSON')
      table.integer('solicitation_type_id').unsigned()
      table.foreign('solicitation_type_id').references('solicitation_types.id')
      table.string('specialty', 80)
      table.enum('status', ['WAITING_FOR_CLASSIFICATION']).defaultTo('WAITING_FOR_CLASSIFICATION').notNullable()
      table.datetime('client_viewed_in')
      table.datetime('partner_viewed_in')
      table.datetime('admin_viewed_in')
      table.timestamps()
    })
  }

  down () {
    this.drop('solicitations')
  }
}

module.exports = SolicitationSchema
