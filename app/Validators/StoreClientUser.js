'use strict'

class StoreClientUser {
  get rules () {
    return {
      name: 'required|string',
      email: 'required|email|unique:users,email',
      password: 'required',
      type: 'required|in:CLIENT',
      photo_url: 'string',
      'company.name': 'required|string',
      'company.cnpj': 'required|string',
      'company.status': 'required|in:ACTIVE,INACTIVE'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'password',
      'photo_url'
    ])

    body.type = 'CLIENT'
    body.company = this.ctx.request.input('company', {})
    body.company.status = 'ACTIVE'

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreClientUser
