'use strict'

const SolicitationModel = use('App/Models/Solicitation')
const MessageModel = use('App/Models/Message')

class SearchService {

  async search (search, user) {
    search = `%${search}%`

    const promises = await Promise.all([
      this._findSolicitations(search, user),
      this._findMessages(search, user)
    ])

    const resp = promises[0].concat(promises[1])

    return resp.splice(0,5)
  }

  async _findMessages (search, user) {
    let messages =
      MessageModel
        .query()
        .with('solicitation', (builder) => {
          if (user.type !== 'ADMIN') {
            builder.setHidden(['note', 'favorite', 'tags'])
          }
        })
        .where('text', 'like', search)
        .whereNot('type', 'ACTION')

    if(user.type !== 'ADMIN') {
      messages.where('chat_type', user.type)
      messages.whereHas('solicitation', (builder) => {
        this._setSolicitationQuery(builder, user)
      })
    }

    messages = await messages.paginate(1, 5)
    messages =  messages.toJSON()
    return messages.data.map(m => ({data: m, type: 'MESSAGE'}))
  }

  async _findSolicitations (search, user) {
    let solicitations =
      SolicitationModel
        .query()
        .where((builder) => {
          builder.where('name', 'like', search)

          if (user.type === 'ADMIN') {
            const textSearch = search.replace(/%/g, '')

            builder.orWhere('note', 'like', search)
            builder.orWhereRaw("JSON_CONTAINS(solicitations.tags, ?)", `["${textSearch}"]`)
          }
        })

    if (user.type !== 'ADMIN') {
      solicitations.setHidden(['note', 'favorite', 'tags'])
      this._setSolicitationQuery(solicitations,user)
    }

    solicitations = await solicitations.paginate(1, 5)
    solicitations = solicitations.toJSON()
    return solicitations.data.map(s => ({data: s, type: 'SOLICITATION'}))
  }

  _setSolicitationQuery (builder, user) {
    if (user.type === 'PARTNER') builder.where('partner_id', user.id)
    else builder.whereHas('company', (builder) => {
      builder.whereHas('users', (builder) => {
        builder.where('user_id', user.id)
        builder.whereRaw("JSON_CONTAINS(`company_user`.`permissions`, ?)", '["SOLICITATION"]')
      })
    })
  }

}

module.exports = SearchService
