'use strict'

class PermissionChecker {

  async handle ({ response, auth, request }, next, permissions) {
    const user = auth.user.toJSON()
    const { company_id } = request.all()

    if (user.type === 'ADMIN' && !this._hasPermissions(user.permissions, permissions)) {
      return response.status(401).send({})
    } else if (user.type === 'CLIENT') {
      if (!company_id) return response.status(401).send({})
      const userCompany = await auth.user.companies().pivotQuery().where('company_id', company_id).first()
      if (!userCompany || !this._hasPermissions(userCompany.toJSON().permissions, permissions)) {
        return response.status(401).send({})
      }
    }

    return await next()
  }

  _hasPermissions (userPermissions, permissions) {
    for (const permission of permissions) {
      if (userPermissions.some(p => permissions.includes(p))) {
        return true
      }
    }
    return false
  }

}

module.exports = PermissionChecker
