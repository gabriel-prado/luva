'use strict'

const Schema = use('Schema')

class SlaColunmSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.datetime('sla_start_date')
      table.datetime('sla_end_date')
      table.datetime('partner_sla_start_date')
      table.datetime('partner_sla_end_date')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('sla_start_date')
      table.dropColumn('sla_end_date')
      table.dropColumn('partner_sla_start_date')
      table.dropColumn('partner_sla_end_date')
    })
  }
}

module.exports = SlaColunmSchema
