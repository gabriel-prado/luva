'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const Drive = use('Drive')
const { test, trait, beforeEach } = use('Test/Suite')('Company File - POST /company/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })

  await defaultClient.companies().attach([ defaultCompany.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('client should save file and return status 200', async({ client, assert }) => {
  const response = await _storeFile(client, defaultClient)

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.url, 'file.pdf')
})

test('admin should save file and return status 200', async({ client, assert }) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const response = await _storeFile(client, admin)

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.url, 'file.pdf')
})

test('should return 401 when the company does not belong to the user', async({ client, assert }) => {
  const response = await client.post(`company/34/file`).query({company_id: 1}).loginVia(defaultClient).end()
  assert.equal(response.status, 401)
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.post(`company/${defaultCompany.id}/file`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.post(`company/${defaultCompany.id}/file`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

const _storeFile = async (client, user) => {
  await Drive.put('file.pdf', Buffer.from('Hello world!'))
  let file = await Drive.disk('local').getStream('file.pdf')

  const response =
    await
      client
      .post(`company/${defaultCompany.id}/file`)
      .attach('file', file)
      .query({company_id: 1})
      .loginVia(user)
      .end()

  await Drive.disk('local').delete('file.pdf')
  return response
}
