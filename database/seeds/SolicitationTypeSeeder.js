'use strict'

const Factory = use('Factory')

class SolicitationTypeSeeder {
  async run () {
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Consultas', 50, 24, 25)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Pareceres de baixa complexidade', 300, 48, 300)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Pareceres de média complexidade', 500, 96, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Pareceres de alta complexidade')
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de documentos - textos, comunicados, notificações de baixa complexidade', 300, 24, 300)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de documentos - textos, comunicados, notificações de média complexidade', 500, 72, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de documentos - textos, comunicados, notificações de alta complexidade', null, 96)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Análise de contratos de baixa complexidade', 300, 72, 300)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Análise de contratos de média complexidade', 500, 96, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Análise de contratos de alta complexidade')
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de contratos de baixa complexidade', 500, 48, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de contratos de média complexidade', 1500, 72, 1500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de contratos de alta complexidade')
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de contratos internacionais baixa/média complexidade (até 15 páginas)', 1500, 168, 1500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Elaboração de minuta padrão de contrato (até 15 páginas)', 2000, 168, 2000)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Contrato Social (modelo Junta)', 50, 24, 50)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Alteração contrato social (baixa complexidade)', 500, 96, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Alteração contrato social (média complexidade)', 1000, 144, 1000)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Alteração contrato social (alta complexidade)', null, 192)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Ata de reunião de sócios', 500, 96, 500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Ata de Assembléia Geral Acionistas', 1500, 168, 1500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Acordo de sócios (baixa complexidade)', 2500, 240, 2500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Acordo de sócios (média complexidade)', 5000, 288, 5000)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Registro de marcas (documentação para depósito no INPI)', 1500, 240, 1500)
    )
    await Factory.model('App/Models/SolicitationType').create(
      this.build('Registro de domínio internet', 300, 48, 300)
    )
  }

  build(name, value = null, response_time = null, client_up_value = null) {
    return { name, value, response_time, client_up_value }
  }
}

module.exports = SolicitationTypeSeeder
