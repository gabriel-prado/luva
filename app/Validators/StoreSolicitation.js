'use strict'

class StoreSolicitation {
  get rules () {
    return {
      name: 'required|string',
      demand_value: 'number',
      status: 'required|in:WAITING_FOR_CLASSIFICATION',
      description: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'demand_value',
      'description'
    ])

    body.status = 'WAITING_FOR_CLASSIFICATION'

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreSolicitation
