'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Recover Password - PUT /user/change-password')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({type: 'ADMIN'})
})

test('should update password and return 200', async({ client, assert}) => {
  await client.post('user/generate-code').send({ email: defaultUser.email }).end()
  await defaultUser.reload()

  const response =
    await client
      .put('user/change-password')
      .send({
        code: defaultUser.verification_code,
        newPassword: 'new_password',
        confirmPassword: 'new_password'
      })
      .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body.token)
  const userInDatabase = await UserModel.find(defaultUser.id)
  assert.isNull(userInDatabase.verification_code)
  assert.notEqual(userInDatabase.password, defaultUser.password)
})

test('should try update password with wrong code and return status 404', async({ client, assert}) => {
  const response =
    await client
      .put('user/change-password')
      .send({
        code: "1231232123",
        newPassword: 'new_password',
        confirmPassword: 'new_password'
      })
      .end()

  assert.equal(response.status, 404)
})

test('should try update password with wrong confirmPassword and return status 400', async({ client, assert}) => {
  const response =
    await client
      .put('user/change-password')
      .send({
        code: "1231232123",
        newPassword: 'new_password',
        confirmPassword: 'old_password'
      })
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'confirmPassword')
})
