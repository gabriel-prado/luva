'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('User by Company - GET /company/id/user')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
  const anotherClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  await anotherClient.companies().attach([ defaultCompany.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should list all users by companies for client user and return status 200', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user`).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.isNotNull(response.body.data[0].companies[0].permissions)
})

test('should list all users by companies for admin user and return status 200', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user`).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 2)
  assert.isNotNull(response.body.data[0].companies[0].permissions)
})

test('should not list users for not logged user', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get(`company/${defaultCompany.id}/user`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
