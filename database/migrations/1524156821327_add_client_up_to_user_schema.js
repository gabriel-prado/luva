'use strict'

const Schema = use('Schema')

class AddClientUpToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('client_up').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('client_up')
    })
  }
}

module.exports = AddClientUpToUserSchema
