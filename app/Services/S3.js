'use strict'

const Drive = use('Drive')
const AWS = require('aws-sdk')

class S3Service {

  async send (file, bucket, bucketPath = '') {
    let stream = file.stream
    let fileName = stream.filename

    await Drive
      .disk(bucket)
      .put(`${bucketPath}${fileName}`, stream, { Key: fileName, ContentType: file.headers['content-type']})

  }

  async get (fileName, bucket, bucketPath = '') {
    return await Drive.disk(bucket).getObject(`${bucketPath}${fileName}`)
  }

  forward (fileName, newName, bucket, bucketPathSource = '', bucketPathDestination = '') {
    return Drive
      .disk(bucket)
      .copy(`${bucketPathSource}${fileName}`, `${bucketPathDestination}${newName}`)
  }

}

module.exports = S3Service
