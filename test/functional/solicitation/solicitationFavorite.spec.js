'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const SolicitationModel = use('App/Models/Solicitation')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation Favorite - PUT /solicitation/id/favorite')

let defaultClient = null
let defaultPartner = null
let defaultAdmin = null
let defaultSolicitation = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    client_viewed_in: null,
    partner_viewed_in: null,
    admin_viewed_in: null
  })
})

test('should update solicitation favorite by admin user and return 204', async({ client, assert}) => {
  const response = await client.put(`solicitation/${defaultSolicitation.id}/favorite`).send({ favorite: true }).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 204)
  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)
  assert.equal(solicitationInDatabase.favorite, 1)
})

test('should update solicitation favorite by client user and return 401', async({ client, assert}) => {
  const response = await client.put(`solicitation/${defaultSolicitation.id}/favorite`).send({ favorite: true }).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 401)
})

test('should update solicitation favorite by partner user and return 401', async({ client, assert}) => {
  const response = await client.put(`solicitation/${defaultSolicitation.id}/favorite`).send({ favorite: true }).query({company_id: 1}).loginVia(defaultPartner).end()

  assert.equal(response.status, 401)
})

test('should update solicitation favorite without logged User and return 401', async({ client, assert }) => {
  const response = await client.put(`solicitation/1/favorite`).end()

  assert.equal(response.status, 401)
})
