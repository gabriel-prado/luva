const { hooks } = require('@adonisjs/ignitor')
const { ioc } = require('@adonisjs/fold')
const Helpers = use('Helpers')

hooks.before.providersBooted(() => {
  if (process.env.NODE_ENV === 'testing') {
    ioc.fake('App/Services/S3', () => {
      return {
        send: () => true,
        get: () => ({ContentType: 'text/plain', Body: Buffer.from('Hello world!')}),
        forward: () => true
      }
    })

    ioc.fake('App/Services/SendGrid', () => {
      return {
        send: () => true,
        buildClientClassifyMessage: () => ({}),
        buildPartnerClassifyMessage: () => ({}),
        buildActivatedPartnerMail: () => ({}),
        buildContactMail: () => ({}),
        buildClientAttach: () => ({})
      }
    })
  }
})

hooks.after.providersBooted(() => {
  const SolicitationService = require('./../app/Services/Solicitation')
  const PartnerService = require('./../app/Services/Partner')
  const CronService = require('./../app/Services/Cron')
  const MessageService = require('./../app/Services/Message')
  const SendGridService = require('./../app/Services/SendGrid')
  const UserService = require('./../app/Services/User')
  const CreditService = require('./../app/Services/Credit')
  const S3Service = require('./../app/Services/S3')
  const CompanyService = require('./../app/Services/Company')
  const RecoverPasswordService = require('./../app/Services/RecoverPassword')
  const SolicitationTypeService = require('./../app/Services/SolicitationType')
  const SupportService = require('./../app/Services/Support')
  const TransactionService = require('./../app/Services/Transaction')
  const AnalyticService = require('./../app/Services/Analytic')

  ioc.singleton('App/Services/S3', () => new S3Service())

  ioc.singleton('App/Services/SendGrid', () => new SendGridService())

  ioc.singleton('App/Services/SolicitationType', () => new SolicitationTypeService())

  ioc.singleton('App/Services/Company', () => {
    const sendGrid = use('App/Services/SendGrid')

    return new CompanyService(sendGrid)
  })

  ioc.singleton('App/Services/RecoverPassword', () => {
    const sendGrid = use('App/Services/SendGrid')

    return new RecoverPasswordService(sendGrid)
  })

  ioc.singleton('App/Services/Support', () => {
    const sendGrid = use('App/Services/SendGrid')

    return new SupportService(sendGrid)
  })

  ioc.singleton('App/Services/Credit', () => {
    const sendGrid = use('App/Services/SendGrid')
    const company = use('App/Services/Company')

    return new CreditService(sendGrid, company)
  })

  ioc.singleton('App/Services/Transaction', () => {
    const credit = use('App/Services/Credit')
    
    return new TransactionService(credit)
  })

  ioc.singleton('App/Services/User', () => {
    const solicitation = use('App/Services/Solicitation')
    const sendGrid = use('App/Services/SendGrid')
    const company = use('App/Services/Company')

    return new UserService(sendGrid, solicitation, company)
  })

  ioc.singleton('App/Services/Message', () => {
    const sendGrid = use('App/Services/SendGrid')

    return new MessageService(sendGrid)
  })

  ioc.singleton('App/Services/Solicitation', () => {
    const message = use('App/Services/Message')
    const company = use('App/Services/Company')
    const s3 = use('App/Services/S3')
    const credit = use('App/Services/Credit')
    const sendGrid = use('App/Services/SendGrid')

    return new SolicitationService(message, company, s3, credit, sendGrid)
  })

  ioc.singleton('App/Services/Analytic', () => {
    const solicitation = use('App/Services/Solicitation')

    return new AnalyticService(solicitation)
  })

  ioc.singleton('App/Services/Partner', () => {
    const solicitation = use('App/Services/Solicitation')
    const sendGrid = use('App/Services/SendGrid')
    const message = use('App/Services/Message')

    return new PartnerService(solicitation, sendGrid, message)
  })

  ioc.singleton('App/Services/Cron', () => {
    const solicitation = use('App/Services/Solicitation')
    const partner = use('App/Services/Partner')

    return new CronService(partner, solicitation)
  })

  if (process.env.NODE_ENV !== 'testing' && !Helpers.isAceCommand()) {
    const cron = use('App/Services/Cron')
    cron.start()
  }

})
