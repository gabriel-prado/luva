'use strict'

class AnalyticController {

  static get inject () {
    return ['App/Services/Analytic']
  }

  constructor (AnalyticService) {
    this.analyticService = AnalyticService
  }

  solicitationByCompany ({ request, response }) {
    const { start_date, end_date, companies_id } = request.get()

    if (!start_date || !end_date || !companies_id) {
      return response.status(400).send({})
    }

    return this.analyticService.solicitationByCompany(companies_id, start_date, end_date)
  }

  averageSlaTime ({ request, response }) {
    const { start_date, end_date, company_id } = request.get()

    if (!start_date || !end_date || !company_id) {
      return response.status(400).send({})
    }

    return this.analyticService.averageSlaTime(Number(company_id), start_date, end_date)
  }

  slaCompliance ({ request, response }) {
    const { company_id } = request.get()

    if (!company_id) return response.status(400).send({})

    return this.analyticService.slaCompliance(Number(company_id))
  }

  export ({ request, response }) {
    const { company_id } = request.get()

    if (!company_id) return response.status(400).send({})

    response.header('Content-disposition', 'attachment; filename=relatório.xlsx')
    response.type('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

    return this.analyticService.export(Number(company_id))
  }

}

module.exports = AnalyticController
