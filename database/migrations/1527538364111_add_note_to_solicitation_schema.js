'use strict'

const Schema = use('Schema')

class AddNoteToSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.text('note')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('note')
    })
  }
}

module.exports = AddNoteToSolicitationSchema
