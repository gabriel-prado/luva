'use strict'

const Model = use('Model')

class SolicitationType extends Model {

  solicitations() {
    return this.hasMany('App/Models/Solicitation')
  }

}

module.exports = SolicitationType
