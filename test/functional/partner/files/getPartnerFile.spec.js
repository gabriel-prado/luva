'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Partner File - GET /partner/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultPartner = null

beforeEach(async () => {
  await clearDataBase()
  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })
})

test('should get file and return status 200', async({ client, assert }) => {
  const response =
    await
      client
        .get(`partner/${defaultPartner.id}/file`)
        .query({ file: 'teste.txt', company_id: 1 })
        .loginVia(defaultPartner)
        .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('admin should get file and return status 200', async({ client, assert }) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const response =
    await
      client
        .get(`partner/${defaultPartner.id}/file`)
        .query({ file: 'teste.txt', company_id: 1 })
        .loginVia(admin)
        .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get(`partner/${defaultPartner.id}/file`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get(`partner/${defaultPartner.id}/file`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
