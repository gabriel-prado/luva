'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Recover Password - POST /user/generate-code')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({type: 'ADMIN'})
})

test('should generate code and send email', async({ client, assert}) => {
  const response = await client.post('user/generate-code').send({ email: defaultUser.email }).end()

  assert.equal(response.status, 204)
  const userInDatabase = await UserModel.find(defaultUser.id)
  assert.isNotNull(userInDatabase.verification_code)
})

test('should not generate code to client without company', async({ client, assert}) => {
  const clientUser = await Factory.model('App/Models/User').create({type: 'CLIENT'})
  const response = await client.post('user/generate-code').send({ email: clientUser.email }).end()

  assert.equal(response.status, 204)
  const userInDatabase = await UserModel.find(defaultUser.id)
  assert.isNull(userInDatabase.verification_code)
})

test('should try generate code and return status 400', async({ client, assert}) => {
  const response = await client.post('user/generate-code').end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'email')
})
