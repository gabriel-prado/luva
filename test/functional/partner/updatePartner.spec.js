'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Partner - PUT /partner/:id')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should update Partner for partner user and return 200', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let partner = await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  user = user.toJSON()
  user.partner = partner.toJSON()

  user.name = 'JONAS'
  user.partner.document_number = '1234'
  user.partner.address.zip_code = '1234'
  user.partner.status = (user.partner.status !== 'ACTIVE') ? 'ACTIVE' : 'INACTIVE'
  user.partner.notes = 'new_notes'
  user.email = 'new_email@email.com'

  const response = await client.put(`partner/${user.id}`).query({company_id: 1}).loginVia(user).send(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, user.name)
  assert.equal(response.body.email, user.email)
  assert.equal(response.body.partner.document_number, user.partner.document_number)
  assert.equal(response.body.partner.address.zip_code, user.partner.address.zip_code)
  assert.notEqual(response.body.partner.status, user.partner.status)
  assert.notEqual(response.body.partner.notes, user.partner.notes)
})

test('should update Partner for partner user with same email and return 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  user = user.toJSON()

  const response = await client.put(`partner/${user.id}`).query({company_id: 1}).loginVia(user).send(user).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].message, 'unique validation failed on email')
})

test('should update Partner for admin user and return 200', async({ client, assert}) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let partner = await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  user = user.toJSON()
  user.partner = partner.toJSON()
  delete user.email

  user.name = 'JONAS'
  user.partner.document_number = '1234'
  user.partner.address.zip_code = '1234'
  user.partner.status = (user.partner.status !== 'ACTIVE') ? 'ACTIVE' : 'INACTIVE'
  user.partner.notes = 'new_notes'

  const response = await client.put(`partner/${user.id}`).query({company_id: 1}).loginVia(admin).send(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, user.name)
  assert.equal(response.body.partner.document_number, user.partner.document_number)
  assert.equal(response.body.partner.address.zip_code, user.partner.address.zip_code)
  assert.equal(response.body.partner.status, user.partner.status)
  assert.equal(response.body.partner.notes, user.partner.notes)
})

test('should update Partner and return 401', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let partner = await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  user = user.toJSON()
  user.partner = partner.toJSON()
  delete user.email

  user.name = 'JONAS'
  user.partner.document_number = '1234'
  user.partner.address.zip_code = '1234'

  const response = await client.put(`partner/171`).query({company_id: 1}).loginVia(user).send(user).end()

  assert.equal(response.status, 401)
})

test('should update Partner whithout loggedUser and return 401', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  user = user.toJSON()

  const response = await client.put(`partner/${user.id}`).send(user).end()

  assert.equal(response.status, 401)
})

test('should update Partner CLIENT USER and return 401', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  user = user.toJSON()
  const response = await client.put(`partner/${user.id}`).query({company_id: 1}).loginVia(user).send(user).end()

  assert.equal(response.status, 401)
})
