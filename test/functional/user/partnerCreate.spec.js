'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const PartnerModel = use('App/Models/Partner')
const { test, trait, beforeEach } = use('Test/Suite')('User - POST /partner')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
})

test('should create partner user and return status 204', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let partner = await Factory.model('App/Models/Partner').make({ notes: null })
  user = user.toJSON()
  user.partner = partner.toJSON()
  user.password = 'senha123'

  const response =
    await client
      .post('partner')
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 204)
  const userInDatabase = await UserModel.findBy('email', user.email)
  assert.isNotNull(userInDatabase)
  const partnerInDatabase = await PartnerModel.findBy('document_number', user.partner.document_number)
  assert.isNotNull(partnerInDatabase)
})

test('should try create user without email and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let partner = await Factory.model('App/Models/Partner').make({ notes: null })
  user = user.toJSON()
  user.partner = partner.toJSON()
  delete user.email

  const response = await client.post('partner').send(user).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'email')
})

test('should try create partner user without document_number and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let partner = await Factory.model('App/Models/Partner').make({ notes: null })
  user = user.toJSON()
  user.partner = partner.toJSON()
  delete user.partner.document_number

  const response = await client.post('partner').send(user).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 400)
})
