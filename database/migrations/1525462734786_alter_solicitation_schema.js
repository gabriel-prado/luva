'use strict'

const Schema = use('Schema')

class AlterSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.datetime('end_date')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('end_date')
    })
  }
}

module.exports = AlterSolicitationSchema
