'use strict'

const Factory = use('Factory')
const moment = use('moment')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  return {
    name: _isDefined(data.name, faker.name()),
    email: _isDefined(data.email, faker.email()),
    password: _isDefined(data.password, faker.word()),
    type: _isDefined(data.type, faker.pickone(['ADMIN', 'CLIENT', 'PARTNER'])),
    photo_url: _isDefined(data.photo_url, faker.avatar()),
    verification_code: _isDefined(data.verification_code, null),
    prefix_oab: _isDefined(data.prefix_oab, faker.pickone(['OAB/MG', 'OAB/AC', 'OAB/SP'])),
    oab_number: _isDefined(data.oab_number, faker.cnpj()),
    permissions: _isDefined(data.permissions, ['SOLICITATION','CREDIT','COMPANY','USER','REPORT','ADMIN']),
    active: _isDefined(data.active, true)
  }
})

Factory.blueprint('App/Models/Company', (faker, i, data) => {
  return {
    name: _isDefined(data.name, faker.name()),
    cnpj: _isDefined(data.cnpj, faker.cnpj()),
    status: _isDefined(data.status, 'ACTIVE'),
    society: _isDefined(data.society, faker.company()),
    activity: _isDefined(data.activity, faker.company()),
    client_up: _isDefined(data.client_up, false),
    unlimited_balance: _isDefined(data.unlimited_balance, false),
    plan_value: _isDefined(data.plan_value, null),
    size: _isDefined(data.size, faker.pickone(['Pequena', 'Média', 'Grande'])),
    notes: _isDefined(data.notes, faker.paragraph()),
    area_of_expertise: _isDefined(data.area_of_expertise, faker.pickset([faker.word(), faker.word(), faker.word(), faker.word()])),
    taxation_regime: _isDefined(data.taxation_regime, faker.word()),
    files: _isDefined(data.files, faker.pickset([faker.word(), faker.word(), faker.word(), faker.word()])),
    complementary_data: _isDefined(data.complementary_data, {
      photo_url: faker.word(),
      trading_name: faker.word(),
      state_inscription: faker.word(),
      date_foundation: faker.word(),
      social_capital: faker.floating({min: 0, max: 1000, fixed: 2}),
      employees: faker.word(),
      company_syndicate: faker.word(),
      employees_syndicate: faker.word(),
      employee_recruitment_scheme: faker.word(),
      employee_working_day: faker.word(),
      billing: faker.floating({min: 0, max: 1000, fixed: 2}),
      billing_summary: faker.word()
    }),
    address: _isDefined(data.address, {
      zip_code: faker.zip({plusfour: true}),
      state: faker.state(),
      city: faker.city(),
      neighborhood: faker.province(),
      street: faker.street(),
      number: faker.integer({min: 1, max: 99999}),
      complement: faker.sentence()
    }),
    contact: _isDefined(data.contact, {
      name: faker.name(),
      phone: faker.phone(),
      email: faker.email()
    })
  }
})

Factory.blueprint('App/Models/Partner', async (faker, i, data) => {
  data.address = data.address || {}
  data.contact = data.contact || {}
  data.bank = data.bank || {}
  data.bank.responsible = data.bank.responsible || {}
  data.complementary_data = data.complementary_data || {}

  return {
    id: data.id,
    entity_type: _isDefined(data.entity_type, faker.pickone(['PF', 'PJ'])),
    document_number: _isDefined(data.cnpj, faker.cnpj()),
    prefix_oab: _isDefined(data.prefix_oab, faker.pick(['OAB/MG', 'OAB/AC', 'OAB/SP'])),
    oab_number: _isDefined(data.cnpj, faker.cnpj()),
    status: _isDefined(data.status, 'REQUESTED'),
    rg: _isDefined(data.rg, faker.string()),
    files: _isDefined(data.files, faker.pickset(['file1', 'file2', 'file3'])),
    specialty: _isDefined(data.specialty, faker.pickset(['Specialty1', 'Specialty2', 'Specialty3'])),
    notes: _isDefined(data.notes, faker.paragraph()),
    address: {
      zip_code: _isDefined(data.address.zip_code, faker.zip()),
      state: _isDefined(data.address.state, faker.string({ length: 20 })),
      city: _isDefined(data.address.city, faker.string({ length: 20 })),
      neighborhood: _isDefined(data.address.neighborhood, faker.string({ length: 20 })),
      street: _isDefined(data.address.street, faker.string({ length: 20 })),
      number: _isDefined(data.address.number, faker.integer()),
      complement: _isDefined(data.address.complement, faker.string({ length: 20 }))
    },
    contact: {
      name: _isDefined(data.contact.name, faker.name()),
      phone: _isDefined(data.contact.phone, faker.phone()),
      email: _isDefined(data.contact.email, faker.email())
    },
    bank: {
      name: _isDefined(data.bank.name, faker.name()),
      agency: _isDefined(data.bank.agency, faker.string({length: 10})),
      account: _isDefined(data.bank.account, faker.string({length: 10})),
      responsible: {
        name: _isDefined(data.bank.responsible.name, faker.name()),
        cpf: _isDefined(data.bank.responsible.cpf, faker.cpf())
      }
    },
    complementary_data: {
      gender: _isDefined(data.complementary_data.gender, faker.name()),
      birth_date: _isDefined(data.complementary_data.birth_date, faker.date()),
      language: _isDefined(data.complementary_data.language, faker.pickset(['language1', 'language2', 'language3'])),
      civil_state: _isDefined(data.complementary_data.civil_state, faker.pick(['Casado', 'Solteiro', 'Viúvo'])),
      availability_attendance: _isDefined(data.complementary_data.availability_attendance, faker.pickset(['08:00 - 18:00', '12:00 - 20:00']))
    }
  }
})

Factory.blueprint('App/Models/SolicitationType', (faker, i, data) => {
  return {
    name: _isDefined(data.name, faker.name()),
    value: _isDefined(data.value, faker.floating({min: 0, max: 1000, fixed: 2})),
    response_time: _isDefined(data.response_time, faker.integer({min: 0, max: 100})),
    client_up_value: _isDefined(data.client_up_value, faker.floating({min: 0, max: 100}))
  }
})

Factory.blueprint('App/Models/Solicitation', (faker, i, data) => {
  return {
    name: _isDefined(data.name, faker.name()),
    client_id: _isDefined(data.client_id, null),
    partner_id: _isDefined(data.partner_id, null),
    company_id: _isDefined(data.company_id, null),
    demand_value: _isDefined(data.demand_value, faker.floating({min: 0, max: 1000, fixed: 2})),
    client_files: _isDefined(data.client_files, faker.pickset(['file1', 'file2', 'file3'])),
    partner_files: _isDefined(data.partner_files, faker.pickset(['file1', 'file2', 'file3'])),
    solicitation_type_id: _isDefined(data.solicitation_type_id, null),
    specialty: _isDefined(data.specialty, faker.pickone(['Specialty1', 'Specialty2', 'Specialty3'])),
    status: _isDefined(data.status, faker.pickone([
      'WAITING_FOR_CLASSIFICATION', 'WAITING_FOR_PARTNER_ACCEPT',
      'WAITING_FOR_PARTNER_RESPONSE', 'WAITING_FOR_ADMIN_RESPONSE',
      'WAITING_FOR_CLIENT_RESPONSE', 'FINALIZED', 'CANCELED'
    ])),
    client_viewed_in: _isDefined(data.client_viewed_in, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    partner_viewed_in: _isDefined(data.partner_viewed_in, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    admin_viewed_in: _isDefined(data.admin_viewed_in, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    end_date: _isDefined(data.end_date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    sla_start_date: _isDefined(data.sla_start_date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    sla_end_date: _isDefined(data.sla_end_date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    partner_sla_start_date: _isDefined(data.partner_sla_start_date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    partner_sla_end_date: _isDefined(data.partner_sla_end_date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    value: _isDefined(data.value, faker.floating({min: 0, max: 100})),
    response_time: _isDefined(data.response_time, faker.integer({min: 0, max: 100})),
    note: _isDefined(data.note, faker.paragraph()),
    notice_time: _isDefined(data.notice_time, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    admin_id: _isDefined(data.admin_id, null),
    partner_notice_time: _isDefined(data.partner_notice_time, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    favorite: _isDefined(data.favorite, false),
    tags: _isDefined(data.tags, faker.pickset(['tag1', 'tag2', 'tag3']))
  }
})

Factory.blueprint('App/Models/Message', (faker, i, data) => {
  return {
    text: _isDefined(data.text, faker.paragraph()),
    type: _isDefined(data.type, faker.pickone(['FILE', 'TEXT', 'ACTION'])),
    solicitation_id: _isDefined(data.solicitation_id, null),
    user_id: _isDefined(data.user_id, null),
    chat_type: _isDefined(data.chat_type, faker.pickone(['CLIENT', 'PARTNER']))
  }
})

Factory.blueprint('App/Models/Transaction', (faker, i, data) => {
  return {
    date: _isDefined(data.date, moment(faker.date()).format('YYYY-MM-DD HH:mm:ss')),
    expire: _isDefined(data.expire, false),
    value: _isDefined(data.value, faker.floating({ min: 0, max: 1000, fixed: 2 })),
    operation: _isDefined(data.operation, faker.pickone(['ADD', 'REMOVE'])),
    solicitation_id: _isDefined(data.solicitation_id, null),
    balance_expiring: _isDefined(data.balance_expiring, faker.floating({ min: 0, max: 1000, fixed: 2 })),
    balance_not_expiring: _isDefined(data.balance_not_expiring, faker.floating({ min: 0, max: 1000, fixed: 2 })),
    company_id: _isDefined(data.company_id, null)
  }
})

const _isDefined = (data, fake) => {
  return (data === undefined) ? fake : data
}
