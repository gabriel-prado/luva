'use strict'

const Schema = use('Schema')

class AddFavoriteToSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.boolean('favorite').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('favorite')
    })
  }
}

module.exports = AddFavoriteToSolicitationSchema
