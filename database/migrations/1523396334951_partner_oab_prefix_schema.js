'use strict'

const Schema = use('Schema')

class PartnerOabPrefixSchema extends Schema {
  up () {
    this.table('partners', (table) => {
      table.string('prefix_oab', 6)
    })
  }

  down () {
    this.table('partners', (table) => {
      table.dropColumn('prefix_oab')
    })
  }
}

module.exports = PartnerOabPrefixSchema
