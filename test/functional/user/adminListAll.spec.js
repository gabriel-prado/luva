'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Admin - GET /admin')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
})

test('should list all admins and return status 200', async({ client, assert}) => {
  const anotherAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const response = await client.get('admin').query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].name, anotherAdmin.name)
})

test('should not list admins for not logged user', async ({ client, assert }) => {
  const response = await client.get('admin').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('admin').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get('admin').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
