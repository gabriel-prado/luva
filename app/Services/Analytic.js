'use strict'

const CompanyModel = use('App/Models/Company')
const SolicitationTypeModel = use('App/Models/SolicitationType')
const SolicitationModel = use('App/Models/Solicitation')
const moment = use('moment')
const XLSX = use('xlsx')
const Numeral = use('numeral')

class AnalyticService {

  constructor (SolicitationService) {
    this.solicitationService = SolicitationService
  }

  async solicitationByCompany (ids = [], startDate, endDate) {
    startDate = moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss')
    endDate = moment(endDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')

    return await
      CompanyModel
        .query()
        .select('name as label')
        .withCount('solicitations as value', (builder) => {
          builder.whereBetween('created_at', [startDate, endDate])
        })
        .whereIn('id', ids)
  }

  async averageSlaTime (companyId, startDate, endDate) {
    startDate = moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss')
    endDate = moment(endDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')

    const types = await
      SolicitationTypeModel
        .query()
        .whereHas('solicitations', this._solicitationConditions (companyId, startDate, endDate))
        .with('solicitations', this._solicitationConditions (companyId, startDate, endDate))
        .fetch()

    return types.toJSON().map(s => {
      return {
        label: s.name,
        value: this._calculateSlaAverage(s.solicitations) || 1
      }
    })
  }

  _calculateSlaAverage (solicitations = []) {
    return solicitations.reduce((s1, s2) => {
      return s1 + this.solicitationService.getSla(s2).sla.current
    }, 0) / solicitations.length
  }

  _solicitationConditions (companyId, startDate, endDate) {
    return (builder) => {
      builder.where('company_id', companyId)
      builder.whereBetween('created_at', [startDate, endDate])
      builder.where('status', 'FINALIZED')
      builder.whereNotNull('response_time')
    }
  }

  async slaCompliance (companyId) {
    const solicitations = await
      SolicitationModel
        .query()
        .where('company_id', companyId)
        .where('status', 'FINALIZED')

    let fulfilledPercent = 0

    if (!!solicitations.length) {
      const fulfilled = solicitations.reduce((sum, s) => {
        const { total, current } = this.solicitationService.getSla(s).sla
        return (current <= total) ? (sum + 1) : sum
      }, 0)

      fulfilledPercent = Math.round((fulfilled * 100) / solicitations.length)
    }

    return [{
      label: 'Cumprido',
      value: fulfilledPercent
    },{
      label: 'Estourado',
      value: fulfilledPercent === 0 && !solicitations.length ? 0 : 100 - fulfilledPercent
    }]
  }

  async export (companyId) {
    const solicitations =
      await
        SolicitationModel
          .query()
          .where('company_id', companyId)
          .with('company')
          .with('client')
          .with('partner')
          .with('solicitationType')
          .with('admin')
          .fetch()

    const data = this._createData(solicitations)

    let ws = XLSX.utils.json_to_sheet(data)
    let wb = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(wb, ws, "Solicitações")
    return XLSX.write(wb, {type:'buffer', bookType: "xlsx"})
  }

  _createData (solicitations, isAdmin = false) {
    Numeral.locale('pt-br')
    return solicitations.toJSON().map(s => {
      const sla = this.solicitationService.getSla(s)
      return {
        'ID': s.id,
        'Nome': s.name,
        'Status': this._convertStatus(s.status, s),
        'Cliente': s.client.name,
        'Empresa': s.company.name,
        'Parceiro': (s.partner) ? s.partner.name : '',
        'Administrador': (s.admin) ? s.admin.name : '',
        'SLA total(hrs)': sla.sla.total,
        'SLA atual(hrs)': sla.sla.current,
        'SLA total do parceiro(hrs)': (s.partner) ? sla.sla.total : '',
        'SLA atual do parceiro(hrs)': (s.partner) ? sla.sla.current : '',
        'Valor da Demanda': Numeral(s.demand_value).format('$ 0,0.00'),
        'Tipo': (s.solicitationType) ? s.solicitationType.name : '',
        'Especialidade': s.specialty,
        'Data de abertura': s.created_at,
        'Valor': Numeral(s.value).format('$ 0,0.00'),
        'Tempo de resposta(hrs)': s.response_time,
        'Notas': s.note,
        'Favorita': (s.favorite) ? 'sim' : 'não',
        'Tags': s.tags.toString()
      }
    })
  }

  _convertStatus (status, solicitation) {
    switch (status) {
      case 'WAITING_FOR_CLASSIFICATION' : return 'Aguardando classificação'
      case 'WAITING_FOR_PARTNER_ACCEPT' : return 'Aguardando retorno advogado'
      case 'WAITING_FOR_PARTNER_RESPONSE' : return 'Aguardando retorno advogado'
      case 'WAITING_FOR_ADMIN_RESPONSE' : return !!solicitation.admin_id ? 'Aguardando retorno Luva' : 'Aguardando double-check'
      case 'WAITING_FOR_CLIENT_RESPONSE' : return 'Aguardando retorno cliente'
      case 'WAITING_FOR_SELECT_NEW_PARTNER' : return 'Aguardando retorno advogado'
      case 'FINALIZED' : return 'Finalizada'
      case 'CANCELED' : return 'Cancelada'
      default: ''
    }
  }

}

module.exports = AnalyticService
