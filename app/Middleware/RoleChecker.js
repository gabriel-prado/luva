'use strict'

class RoleChecker {
  async handle ({ response, auth }, next, types) {
    let user = auth.user

    if(user.type === 'PARTNER') {
      let count = await user.partner().where('status', 'ACTIVE').getCount()
      if(!count) return response.status(401).send({})
    } else if(user.type === 'CLIENT') {
      let count = await user.companies().where('status', 'ACTIVE').getCount()
      if(!count) return response.status(401).send({})
    }

    return ( types.includes(user.type) ) ? await next() : response.status(401).send({})
  }
}

module.exports = RoleChecker
