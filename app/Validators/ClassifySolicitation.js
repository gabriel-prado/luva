'use strict'

class ClassifySolicitation {
  get rules () {
    return {
      partner_id: 'integer',
      solicitation_type_id: 'required|integer',
      specialty: 'required|string',
      value: 'required|number',
      response_time: 'required|number',
      demand_value: 'number'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'partner_id',
      'solicitation_type_id',
      'specialty',
      'value',
      'response_time',
      'demand_value'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = ClassifySolicitation
