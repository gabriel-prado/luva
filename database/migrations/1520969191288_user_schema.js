'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('name', 80).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.enum('type', ['ADMIN', 'CLIENT', 'PARTNER']).notNullable()
      table.string('photo_url', 300).notNullable().defaultTo('assets/img/default-icon.png')
      table.string('verification_code', 20)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
