'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - GET /company')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  let activeCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(activeCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  let inactiveCompany = await Factory.model('App/Models/Company').make({ status: 'INACTIVE' })
  await defaultClient.companies().save(inactiveCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should list all companies for client user and return status 200', async({ client, assert}) => {
  const response = await client.get('company').query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.notExists(response.body.data[0].notes)
  assert.notExists(response.body.data[0].status)
})

test('should list all companies for admin user and return status 200', async({ client, assert}) => {
  const response = await client.get('company').query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 2)
  assert.exists(response.body.data[0].notes)
  assert.exists(response.body.data[0].status)
})

test('should list all companies with active status for admin user and return status 200', async({ client, assert}) => {
  const response = await client.get('company').query({company_id: 1, status: 'ACTIVE'}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should not list companies for not logged user', async ({ client, assert }) => {
  const response = await client.get('company').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('company').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
