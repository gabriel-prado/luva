'use strict'

const Schema = use('Schema')

class SolicitationValueResponseTimeSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.float('value')
      table.integer('response_time').unsigned()
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('value')
      table.dropColumn('response_time')
    })
  }
}

module.exports = SolicitationValueResponseTimeSchema
