'use strict'

const Schema = use('Schema')

class AddPartnerNoticeToSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.datetime('partner_notice_time')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('partner_notice_time')
    })
  }
}

module.exports = AddPartnerNoticeToSolicitationSchema
