'use strict'

class StoreMessage {
  get rules () {
    return {
      text: 'required|string',
      type: 'required|in:FILE,TEXT,ACTION',
      chat_type: 'required|in:CLIENT,PARTNER'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'text',
      'type',
      'chat_type'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreMessage
