'use strict'

const Schema = use('Schema')

class AddNoteToCompanySchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.text('notes')
    })
  }

  down () {
    this.table('companies', (table) => {
      table.dropColumn('notes')
    })
  }
}

module.exports = AddNoteToCompanySchema
