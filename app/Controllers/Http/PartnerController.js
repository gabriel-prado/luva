'use strict'

const Database = use('Database')

class PartnerController {

  static get inject () {
    return [
      'App/Services/Partner', 'App/Services/User', 'App/Services/S3', 'App/Services/Company'
    ]
  }

  constructor (PartnerService, UserService, S3Service, CompanyService) {
    this.partnerService = PartnerService
    this.userService = UserService
    this.s3Service = S3Service
    this.companyService = CompanyService
  }

  async update ({ request, auth, response, params }) {
    const id = Number(params.id)

    if (auth.user.type !== 'ADMIN' && auth.user.id !== id) return response.status(401).send({})

    const trx = await Database.beginTransaction()

    const userData = request.body || {}
    userData.id = id

    const partnerData = userData.partner || {}
    partnerData.id = id

    delete userData.partner

    try {
      const user = await this.userService.update(userData.id, userData, trx)
      const partner = await this.partnerService.update(partnerData.id, partnerData, auth.user.type, user, trx)

      trx.commit()

      return await this.userService.findByIdWithPartner(user.id, auth.user.type)
    } catch(e) {
      trx.rollback()
      throw e
    }
  }

  show ({ request, auth }) {
    return this.userService
      .findByIdWithPartner((auth.user.type === 'ADMIN') ? request.params.id : auth.user.id, auth.user.type)
  }

  async uploadFile ({ request, response, auth }) {
    request.multipart.file('file', {}, async (file) => {
      let bucketPath = `${auth.user.id}/`
      await this.s3Service.send(file, 's3_partner', bucketPath)

      return response.status(200).send({ url: file.stream.filename })
    })

    await request.multipart.process()
  }

  async getFile ({ request, response, auth }) {
    let { file } = request.all()
    if(!file) return response.status(404).send({})

    let bucketPath = `${auth.user.id}/`
    let res = await this.s3Service.get(file, 's3_partner', bucketPath)
    if(!res) return response.status(404).send({})

    response.header('content-type', res.ContentType)
    response.header('content-disposition', `filename=${file}`)

    return res.Body
  }

  index ({ request, auth }) {
    let { all, page, perPage, status, specialty } = request.get()

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5
    let query = (status) ? { status } : {}
    if (specialty) query.specialty = specialty

    return this.partnerService.list(page, perPage, query, auth.user.type)
  }

  async fetchCompanies({ request, response, auth }) {
    if (Number(request.params.id) !== auth.user.id) return response.status(403).send({})

    let { all, page, perPage } = request.get()

    page = (all) ? null : Number(page) || 1
    perPage = (all) ? null : Number(perPage) || 5

    return await this.companyService.fetchPartnerCompanies(page, perPage, auth.user)
  }
}

module.exports = PartnerController
