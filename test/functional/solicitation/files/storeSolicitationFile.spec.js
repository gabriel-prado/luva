'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const SolicitationModel = use('App/Models/Solicitation')
const MessageModel = use('App/Models/Message')
const Drive = use('Drive')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation File - POST /solicitation/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultSolicitation = null
let defaultClient = null

beforeEach(async () => {
  await clearDataBase()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({
    status: 'ACTIVE'
  })
  await defaultClient.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })
  const solicitationType = await Factory.model('App/Models/SolicitationType').create()
  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    partner_id: partner.id,
    company_id: company.id,
    solicitation_type_id: solicitationType.id,
    client_files: [],
    partner_files: []
  })
})

test('client should save file and return status 200', async({ client, assert }) => {
  await Drive.put('file1.pdf', Buffer.from('Hello world!'))
  await Drive.put('file2.pdf', Buffer.from('Hello world!'))
  const file1 = await Drive.disk('local').getStream('file1.pdf')
  const file2 = await Drive.disk('local').getStream('file2.pdf')

  const response =
    await
      client
      .post(`solicitation/${defaultSolicitation.id}/file`)
      .attach('files', file2)
      .attach('files', file1)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  await Drive.disk('local').delete('file1.pdf')
  await Drive.disk('local').delete('file2.pdf')

  assert.equal(response.status, 200)
  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)
  assert.isNotNull(solicitationInDatabase)
  assert.lengthOf(JSON.parse(solicitationInDatabase.client_files), 2)
  const messagesInDatabase = await MessageModel.query().where('solicitation_id', defaultSolicitation.id)
  assert.isNotNull(messagesInDatabase)
  assert.lengthOf(messagesInDatabase, 2)
})

test('client should save file into another solicitation and return status 401', async({ client, assert }) => {
  await Drive.put('file.pdf', Buffer.from('Hello world!'))
  const file = await Drive.disk('local').getStream('file.pdf')
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({
    status: 'ACTIVE'
  })
  await user.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
  let solicitation = await Factory.model('App/Models/Solicitation').create({
    client_id: user.id,
    company_id: company.id,
    solicitation_type_id: 1
  })

  const response =
    await
      client
      .post(`solicitation/${solicitation.id}/file`)
      .attach('files', file)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  await Drive.disk('local').delete('file.pdf')

  assert.equal(response.status, 401)
})

test('should return 404 for request that does not exist.', async({ client, assert }) => {
  const response = await client.post(`solicitation/44/file`).query({company_id: 1}).loginVia(defaultClient).end()
  assert.equal(response.status, 404)
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.post(`solicitation/${defaultSolicitation.id}/file`).end()
  assert.equal(response.status, 401)
})

test('PARTNER should save file and return status 200', async ({ client, assert }) => {
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })

  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({
    status: 'ACTIVE'
  })
  await clientUser.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })

  const solicitationType = await Factory.model('App/Models/SolicitationType').create()
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    client_id: clientUser.id,
    company_id: company.id,
    status: 'WAITING_FOR_CLASSIFICATION',
    solicitation_type_id: solicitationType.id,
    client_files: [],
    partner_files: [],
    partner_id: partner.id
  })

  await Drive.put('file1.pdf', Buffer.from('Hello world!'))
  await Drive.put('file2.pdf', Buffer.from('Hello world!'))

  const file1 = await Drive.disk('local').getStream('file1.pdf')
  const file2 = await Drive.disk('local').getStream('file2.pdf')

  const response = await client
    .post(`solicitation/${solicitation.id}/file`)
    .attach('files', file2)
    .attach('files', file1)
    .query({company_id: 1})
    .loginVia(partner)
    .end()

  await Drive.disk('local').delete('file1.pdf')
  await Drive.disk('local').delete('file2.pdf')

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)
  const messagesInDatabase = await MessageModel.query().where({
    solicitation_id: solicitation.id,
    chat_type: 'PARTNER'
  })

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase)
  assert.lengthOf(JSON.parse(solicitationInDatabase.partner_files), 2)
  assert.isNotNull(messagesInDatabase)
  assert.lengthOf(messagesInDatabase, 2)
})

test('ADMIN should save file partner chatType and return status 200', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  await Drive.put('file1.pdf', Buffer.from('Hello world!'))
  await Drive.put('file2.pdf', Buffer.from('Hello world!'))

  const file1 = await Drive.disk('local').getStream('file1.pdf')
  const file2 = await Drive.disk('local').getStream('file2.pdf')

  const response = await client
    .post(`solicitation/${defaultSolicitation.id}/file`)
    .attach('files', file2)
    .attach('files', file1)
    .query({ chat_type: 'PARTNER', company_id: 1 })
    .loginVia(user)
    .end()

  await Drive.disk('local').delete('file1.pdf')
  await Drive.disk('local').delete('file2.pdf')

  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)
  const messagesInDatabase = await MessageModel.query().where({
    solicitation_id: defaultSolicitation.id,
    chat_type: 'PARTNER'
  })

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase)
  assert.lengthOf(JSON.parse(solicitationInDatabase.partner_files), 2)
  assert.isNotNull(messagesInDatabase)
  assert.lengthOf(messagesInDatabase, 2)
})

test('ADMIN should save file client chatType and return status 200', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  await Drive.put('file1.pdf', Buffer.from('Hello world!'))
  await Drive.put('file2.pdf', Buffer.from('Hello world!'))

  const file1 = await Drive.disk('local').getStream('file1.pdf')
  const file2 = await Drive.disk('local').getStream('file2.pdf')

  const response = await client
    .post(`solicitation/${defaultSolicitation.id}/file`)
    .attach('files', file2)
    .attach('files', file1)
    .query({ chat_type: 'CLIENT', company_id: 1 })
    .loginVia(user)
    .end()

  await Drive.disk('local').delete('file1.pdf')
  await Drive.disk('local').delete('file2.pdf')

  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)
  const messagesInDatabase = await MessageModel.query().where({
    solicitation_id: defaultSolicitation.id,
    chat_type: 'CLIENT'
  })

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase)
  assert.lengthOf(JSON.parse(solicitationInDatabase.client_files), 2)
  assert.isNotNull(messagesInDatabase)
  assert.lengthOf(messagesInDatabase, 2)
})
