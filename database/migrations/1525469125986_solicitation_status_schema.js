'use strict'

const Schema = use('Schema')

class SolicitationStatusSchema extends Schema {
  up () {
    this.alter('solicitations', (table) => {
      table.enum('status', [
        'WAITING_FOR_CLASSIFICATION', 'WAITING_FOR_PARTNER_ACCEPT',
        'WAITING_FOR_PARTNER_RESPONSE', 'WAITING_FOR_ADMIN_RESPONSE',
        'WAITING_FOR_CLIENT_RESPONSE', 'FINALIZED', 'CANCELED'
      ]).alter().defaultTo('WAITING_FOR_CLASSIFICATION').notNullable()
    })
  }

  down () {
    this.alter('solicitations', (table) => {
      table.enum('status', ['WAITING_FOR_CLASSIFICATION']).alter().defaultTo('WAITING_FOR_CLASSIFICATION').notNullable()
    })
  }
}

module.exports = SolicitationStatusSchema
