'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('PartnerCompany - GET /partner/:id/company')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should list all companies and return status 200', async({ client, assert}) => {
  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create()
  await clientUser.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })

  const userPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const partner = await Factory.model('App/Models/Partner').create({ id: userPartner.id, status: 'ACTIVE' })

  await Factory.model('App/Models/Solicitation').create({
    client_id: clientUser.id,
    partner_id: userPartner.id,
    company_id: company.id
  })

  await Factory.model('App/Models/Solicitation').create({
    client_id: clientUser.id,
    company_id: company.id
  })

  const response = await client.get(`partner/${userPartner.id}/company`).query({ all: true })
    .loginVia(userPartner).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should not list partners for not logged user', async ({ client, assert }) => {
  const response = await client.get(`partner/2/company`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with ADMIN role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const response = await client.get(`partner/${anotherUser.id}/company`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with CLIENT role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get(`partner/${anotherUser.id}/company`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
