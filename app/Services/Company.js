'use strict'

const CompanyModel = use('App/Models/Company')
const UserModel = use('App/Models/User')
const Database = use('Database')

class CompanyService {

  constructor (SendGridService) {
    this.sgMail = SendGridService
  }

  async list (page = null, perPage = null, condition = {}, userType) {
    let query = CompanyModel.query().where(condition)
    if (userType !== 'ADMIN') query.setHidden(['status', 'notes', 'unlimited_balance'])
    return await query.paginate(page, perPage)
  }

  async store (attributes, userType, trx = null) {
    attributes = this._clearAttributes(attributes, userType)

    return await CompanyModel.create(attributes, trx)
  }

  async update (attributes, userType) {
    attributes = this._clearAttributes(attributes, userType)

    let company = await CompanyModel.findOrFail(attributes.id)
    company.merge(attributes)
    await company.save()

    return company
  }

  show(id, userType, userId) {
    if (userType === 'ADMIN') {
      return CompanyModel.findOrFail(id)
    } else {
      return CompanyModel.query()
        .setHidden(['status', 'notes'])
        .where('id', id)
        .whereHas('users', (builder) => {
          return builder.where('user_id', userId)
        }).firstOrFail()
    }
  }

  fetchPartnerCompanies(page = null, perPage = null, user) {
    return CompanyModel.query()
      .whereHas('solicitations', (builder) => {
        return builder.where('partner_id', user.id)
      }).paginate(page, perPage)
  }

  findByIdAndUser(id, userId) {
    return (
      CompanyModel
        .query()
        .where('id', id)
        .whereHas('users', builder => {
          builder.where('user_id', userId)
        })
        .first()
    )
  }

  _clearAttributes(attributes, userType) {
    if (userType !== 'ADMIN') {
      delete attributes.notes
      delete attributes.status
      delete attributes.client_up
      delete attributes.plan_value
    }

    return attributes
  }

  async listUsersByCompany (id, page, perPage, user) {
    if (!(await user.hasCompany(id))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')

    const company = await CompanyModel.findOrFail(id)
    const query = company.users()
      .with('companies', (b) => b.setVisible(['id', 'name', 'pivot']))

    if (user.type === 'CLIENT') query.whereNot('users.id', user.id)

    return await query.paginate(page, perPage)
  }

  async showUserByCompany (companyId, userId, user) {
    if (!(await user.hasCompany(companyId))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')
    const company = await CompanyModel.findOrFail(companyId)
    return await company.users().with('companies', (b) => b.setVisible(['id', 'name', 'pivot'])).where('user_id', userId).first()
  }

  async updateUserByCompany (attributes, companyId, userId, currentUser) {
    const userTarget = await UserModel.findOrFail(userId)

    if (!(await currentUser.hasCompany(companyId)) || !(await userTarget.hasCompany(companyId))) {
      throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')
    }

    if (attributes.password) attributes.password = await Hash.make(attributes.password)
    else delete attributes.password

    const permissions = attributes.permissions

    if (userTarget.type !== 'ADMIN') {
      attributes.prefix_oab = null
      attributes.oab_number = null
      attributes.active = false
      attributes.permissions = null
    }

    const trx = await Database.beginTransaction()

    try {
      userTarget.merge(attributes)
      await userTarget.save(trx)

      await trx
        .update({ permissions: JSON.stringify(permissions) })
        .into('company_user')
        .where({ company_id: companyId, user_id: userId })

      trx.commit()

      return userTarget
    } catch(e) {
      trx.rollback()
      throw e
    }
  }

  async attachUserToCompany (attributes, companyId, user) {
    if (!(await user.hasCompany(companyId))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')

    const client = await UserModel.findBy('email', attributes.email)
    const company = await CompanyModel.findOrFail(companyId)
    const { permissions } = attributes
    delete attributes.permissions

    const trx = await Database.beginTransaction()

    try {
      const status = await this._attach(client, attributes, company, permissions, user, trx)

      trx.commit()

      return { status }
    } catch(e) {
      trx.rollback()
      throw e
    }
  }

  async _attachNewUser (attributes, company, permissions, user, trx) {
    attributes.type = 'CLIENT'

    const newClient = await UserModel.create(attributes, trx)
    await this._attachToCompany(newClient, company.id, permissions, trx)

    const msg = this.sgMail.buildClientAttach(attributes, company, user, true)
    await this.sgMail.send(msg, company.client_up)
  }

  async _attachExistingUser (client, company, permissions, user, trx) {
    if (client.type !== 'CLIENT') throw new Error('USER_IS_NOT_A_CLIENT')

    await this._attachToCompany(client, company.id, permissions, trx)

    const msg = this.sgMail.buildClientAttach(client, company, user)
    await this.sgMail.send(msg, company.client_up)
  }

  async _attach(client, userAttributes, company, permissions, user, trx) {
    if (client) {
      await this._attachExistingUser(client, company, permissions, user, trx)

      return 'ATTACHED'
    } else {
      await this._attachNewUser(userAttributes, company, permissions, user, trx)

      return 'CREATED'
    }
  }

  async _attachToCompany (client, companyId, permissions = [], trx) {
    await client.companies().attach([companyId], (row) => {
      row.permissions = permissions
    }, trx)
  }

  async detachUserToCompany (companyId, clientId, user) {
    if (!(await user.hasCompany(companyId))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')
    const client = await UserModel.findOrFail(clientId)
    await client.companies().detach([companyId])
  }

  async create(user, companyData) {
    const trx = await Database.beginTransaction()
    const permissions = ['COMPANY', 'SOLICITATION', 'USER', 'CREDIT']

    try {
      const company = await this.store(companyData, user.type, trx)
      await this._attachToCompany(user, company.id, permissions, trx)

      await trx.commit()

      return company
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }
}

module.exports = CompanyService
