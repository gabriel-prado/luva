'use strict'

const Factory = use('Factory')

class CreateDefaultAdminUserSeeder {
  async run () {
    await Factory.model('App/Models/User').create({
      name: 'Luva',
      email: 'luiza@luvaassessoria.com.br',
      password: 'senha123',
      type: 'ADMIN',
      photo_url: 'assets/img/default-icon.png'
    })
  }
}

module.exports = CreateDefaultAdminUserSeeder
