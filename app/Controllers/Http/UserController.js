'use strict'

const Database = use('Database')

class UserController {

  static get inject () {
    return ['App/Services/User', 'App/Services/Partner', 'App/Services/Company']
  }

  constructor (UserService, PartnerService, CompanyService) {
    this.userService = UserService
    this.partnerService = PartnerService
    this.companyService = CompanyService
  }

  async storePartner ({ request }) {
    const trx = await Database.beginTransaction()
    const attributes = request.body
    let partner = attributes.partner
    delete attributes.partner
    try {
      let user = await this.userService.store(attributes, trx)
      partner.id = user.id
      partner = await this.partnerService.store(partner, trx)

      trx.commit()

      await this.userService.sendPartnerCreatedMail(user, partner)
    } catch(e) {
      trx.rollback()
      throw e
    }
  }

  async storeClient ({ request, auth }) {
    const attributes = request.body
    const company = attributes.company
    delete attributes.company

    try {
      await this.userService.storeClient(attributes, company)

      const { token } = await auth.attempt(attributes.email, attributes.password)

      return { token: `Bearer ${token}` }
    } catch(e) {
      throw e
    }
  }

  update ({ request, auth }) {
    return this.userService.update(auth.user.id, request.body)
  }

  notification ({ auth }) {
    return this.userService.notification(auth.user)
  }

  contact ({ request }) {
    return this.userService.contact(request.body)
  }

  listAdmins ({ request, auth }) {
    let { all, page, perPage } = request.get()

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    return this.userService.listAdmins(page, perPage, auth.user)
  }

  createAdmin({ request }) {
    const attributes = request.body
    attributes.type = 'ADMIN'

    return this.userService.store(attributes)
  }

  updateAdmin ({ request, params }) {
    return this.userService.update(params.id, request.body)
  }

  showAdmin ({ params }) {
    return this.userService.showAdmin(params.id)
  }
}

module.exports = UserController
