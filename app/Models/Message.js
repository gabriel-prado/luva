'use strict'

const Model = use('Model')

class Message extends Model {

  solicitation() {
    return this.belongsTo('App/Models/Solicitation')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

}

module.exports = Message
