'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Finalize Solicitation - POST /solicitation/:id/finalize')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultClient = null
let defaultCompany = null
let defaultSolicitation = null

beforeEach(async () => {
  await clearDataBase()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make()
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    end_date: null,
    client_id: defaultClient.id,
    company_id: defaultCompany.id,
    status: 'WAITING_FOR_CLASSIFICATION'
  })
})

test('should finalize solicitation to CLIENT user and return 200', async({ client, assert}) => {
  const response = await client.post(`solicitation/${defaultSolicitation.id}/finalize`).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
})

test('should finalize solicitation to another CLIENT user and return 401', async({ client, assert}) => {
  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make()
  await clientUser.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  const response = await client.post(`solicitation/${defaultSolicitation.id}/finalize`).query({company_id: company.id}).loginVia(clientUser).end()

  assert.equal(response.status, 401)
})

test('should finalize solicitation is already finalized and return 500', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    end_date: null,
    status: 'FINALIZED',
    client_id: defaultClient.id,
    company_id: defaultCompany.id
  })

  const response = await client.post(`solicitation/${solicitation.id}/finalize`).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.message, "SOLICITATION_IS_ALREADY_FINALIZED")
})

test('should finalize solicitation to ADMIN user and return 200', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const response = await client.post(`solicitation/${defaultSolicitation.id}/finalize`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
})

test('should finalize solicitation to PARTNER user and return 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.post(`solicitation/${defaultSolicitation.id}/finalize`).query({company_id: 1}).loginVia(user).end()
  assert.equal(response.status, 401)
})

test('should finalize solicitation whithout logged User and return 401', async({ client, assert}) => {
  const response = await client.post(`solicitation/1/finalize`).end()

  assert.equal(response.status, 401)
})
