'use strict'

class CompanyController {

  static get inject () {
    return ['App/Services/Company', 'App/Services/User', 'App/Services/S3']
  }

  constructor (CompanyService, UserService, S3Service) {
    this.companyService = CompanyService
    this.userService = UserService
    this.s3Service = S3Service
  }

  index ({ request, auth }) {
    let { all, page, perPage, status } = request.get()

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5
    let query = (status) ? { status } : {}

    if(auth.user.type === 'CLIENT') return this.userService.fetchCompanies(auth.user, page, perPage)
    else return this.companyService.list(page, perPage, query, auth.user.type)
  }

  async store ({ request, auth }) {
    try {
      return await this.companyService.create(auth.user, request.body)
    } catch(e) {
      throw e
    }
  }

  async update ({ response, request, auth }) {
    const attributes = request.body
    attributes.id = Number(request.params.id)
    if (auth.user.type === 'CLIENT') {
      let companies = await this.userService.fetchCompanies(auth.user, null, null, [attributes.id])
      companies = companies.toJSON()
      if (!companies.data.length) return response.status(401).send({})
    }

    return this.companyService.update(attributes, auth.user.type)
  }

  async uploadFile ({ request, response, auth }) {
    if (auth.user.type === 'CLIENT') {
      let companies = await this.userService.fetchCompanies(auth.user, null, null, [request.params.id])
      companies = companies.toJSON()
      if (!companies.data.length) return response.status(401).send({})
    }

    request.multipart.file('file', {}, async (file) => {
      let bucketPath = `${auth.user.id}/company/${request.params.id}/`
      await this.s3Service.send(file, 's3_client', bucketPath)

      return response.status(200).send({ url: file.stream.filename })
    })

    await request.multipart.process()
  }

  async getFile ({ request, response, auth }) {
    let { file } = request.all()
    if(!file) return response.status(404).send({})

    if (auth.user.type === 'CLIENT') {
      let companies = await this.userService.fetchCompanies(auth.user, null, null, [request.params.id])
      companies = companies.toJSON()
      if (!companies.data.length) return response.status(401).send({})
    }

    let bucketPath = `${auth.user.id}/company/${request.params.id}/`
    let res = await this.s3Service.get(file, 's3_client', bucketPath)
    if(!res) return response.status(404).send({})

    response.header('content-type', res.ContentType)
    response.header('content-disposition', `filename=${file}`)

    return res.Body
  }

  show ({ request, auth }) {
    return this.companyService.show(request.params.id, auth.user.type, auth.user.id)
  }

  listUsersByCompany ({ request, params, auth }) {
    let { all, page, perPage, status } = request.get()

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    try {
      return this.companyService.listUsersByCompany(params.company_id, page, perPage, auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  showUserByCompany ({ params, auth }) {
    const { company_id, user_id } = params

    try {
      return this.companyService.showUserByCompany(company_id, user_id, auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  updateUserByCompany ({ request, params, auth }) {
    const { company_id, user_id } = params

    try {
      return this.companyService.updateUserByCompany(request.body, company_id, user_id, auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  attachUserToCompany ({ request, params, auth }) {
    try {
      return this.companyService.attachUserToCompany(request.body, params.company_id, auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  detachUserToCompany ({ params, auth }) {
    try {
      return this.companyService.detachUserToCompany(params.company_id, params.user_id, auth.user)
    } catch (e) {
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

}

module.exports = CompanyController
