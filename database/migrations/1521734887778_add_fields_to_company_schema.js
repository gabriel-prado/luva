'use strict'

const Schema = use('Schema')

class AddFieldsToCompanySchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.string('society', 100)
      table.string('activity', 100)
      table.string('size', 100)
      table.specificType('area_of_expertise', 'JSON')
      table.string('taxation_regime', 100)
      table.specificType('files', 'JSON')
      table.specificType('complementary_data', 'JSON')
      table.specificType('address', 'JSON')
      table.specificType('contact', 'JSON')
    })
  }

  down () {
    this.table('companies', (table) => {
      table.dropColumn('society')
      table.dropColumn('activity')
      table.dropColumn('size')
      table.dropColumn('area_of_expertise')
      table.dropColumn('taxation_regime')
      table.dropColumn('files')
      table.dropColumn('complementary_data')
      table.dropColumn('address')
      table.dropColumn('contact')
    })
  }
}

module.exports = AddFieldsToCompanySchema
