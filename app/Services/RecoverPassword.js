'use strict'

const UserModel = use('App/Models/User')
const Env = use('Env')
const Hash = use('Hash')

class RecoverPasswordService {

  constructor (SendGridService) {
    this.sgMail = SendGridService
  }

  async sendCode (email, verificationCode) {
    const user = await UserModel.findByOrFail('email', email)
    if (user.type === 'CLIENT' && !await user.hasAnyActiveCompany()) return
    if (user.type === 'ADMIN' && !user.active) return
    const isClientUp = await user.isClientUp()
    const subjectPrefix = isClientUp ? 'Up Legal' : 'Luva Assessoria'

    const msg = {
      to: email,
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      subject: `${subjectPrefix} - recuperar senha`,
      templateId: Env.get('CODE_CONFIRM_TEMPLATE_ID'),
      substitutions: {
        name: user.name,
        email: user.email,
        code: verificationCode,
        link: `${Env.get((isClientUp) ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL')}/alterar-senha/${verificationCode}`
      }
    }

    user.merge({ verification_code: verificationCode })
    await user.save()
    await this.sgMail.send(msg, isClientUp)
  }

  async changePassword (code, newPassword) {
    const user = await UserModel.findByOrFail('verification_code', code)

    user.merge({
      password: await Hash.make(newPassword),
      verification_code: null
    })

    await user.save()
    return user
  }

}

module.exports = RecoverPasswordService
