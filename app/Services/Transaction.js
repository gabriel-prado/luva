'use strict'

const TransactionModel = use('App/Models/Transaction')

class TransactionService {

  constructor (CreditService) {
    this.creditService = CreditService
  }

  async list(page, perPage, filter, user, company_id, direction, orderBy) {
    if (!(await user.hasCompany(company_id))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')
    await this.creditService.expire(company_id)

    let query = TransactionModel
      .query()
      .where('company_id', company_id)
      .with('solicitation')
      .orderBy(orderBy, direction)

    query = this._applyFilters(query, filter)

    return query.paginate(page, perPage)
  }

  _applyFilters(query, filter) {
    if (!!filter.start_date) query.where('date', '>=', filter.start_date)
    if (!!filter.end_date) query.where('date', '<=', filter.end_date)

    return query
  }

}

module.exports = TransactionService
