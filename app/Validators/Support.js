'use strict'

class Support {
  get rules () {
    return {
      phone: 'required|string'
    }
  }

  get data () {
    this.ctx.request.body = this.ctx.request.only(['phone'])
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = Support
