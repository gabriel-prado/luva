'use strict'

const moment = use('moment')

class RecoverPasswordController {

  static get inject() {
    return ['App/Services/RecoverPassword']
  }

  constructor(RecoverPasswordService) {
    this.recoverPasswordService = RecoverPasswordService
  }

  async generateCode({ request, response }) {
    const { email } = request.all()
    const verificationCode = moment().format('x')

    await this.recoverPasswordService.sendCode(email, verificationCode)
  }

  async changePassword({ request, auth }) {
    const { code, newPassword } = request.all()

    let user = await this.recoverPasswordService.changePassword(code, newPassword)

    let { token } = await auth.attempt(user.email, newPassword)

    return { token: `Bearer ${token}` }
  }

}

module.exports = RecoverPasswordController
