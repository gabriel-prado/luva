'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const MessageModel = use('App/Models/Message')
const SolicitationModel = use('App/Models/Solicitation')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation - POST /company/:id/solicitation')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make()
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should create solicitation and return 200', async({ client, assert}) => {
  let solicitation = await Factory.model('App/Models/Solicitation').make()
  solicitation = solicitation.toJSON()
  solicitation.description = 'description_test'

  const response = await client.post(`company/${defaultCompany.id}/solicitation`).send(solicitation).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, solicitation.name)
  assert.equal(response.body.status, 'WAITING_FOR_CLASSIFICATION')
  assert.equal(response.body.client_id, defaultClient.id)
  assert.equal(response.body.company_id, defaultCompany.id)

  const messagesInDatabase = await MessageModel.query().where('solicitation_id', response.body.id)
  assert.isNotNull(messagesInDatabase)
  assert.lengthOf(messagesInDatabase, 2)
})

test('should create solicitation without files and return 200', async({ client, assert}) => {
  let solicitation = await Factory.model('App/Models/Solicitation').make()
  solicitation = solicitation.toJSON()
  solicitation.description = 'description_test'
  delete solicitation.client_files
  delete solicitation.partner_files

  const response = await client.post(`company/${defaultCompany.id}/solicitation`).send(solicitation).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  let solicitationInDatabase = await SolicitationModel.find(response.body.id)
  assert.equal(solicitationInDatabase.partner_files, '[]')
  assert.equal(solicitationInDatabase.client_files, '[]')
})

test('should try create solicitation without description and return 400', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').make()

  const response = await client.post(`company/${defaultCompany.id}/solicitation`).send(solicitation).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 400)
})

test('should try create solicitation for another company and return 401', async({ client, assert}) => {
  let solicitation = await Factory.model('App/Models/Solicitation').make()
  solicitation = solicitation.toJSON()
  solicitation.description = 'description_test'
  const company = await Factory.model('App/Models/Company').create({ status: 'ACTIVE' })

  const response = await client.post(`company/${company.id}/solicitation`).send(solicitation).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 401)
  assert.equal(response.body.message, 'COMPANY_DOES_NOT_BELONG_TO_USER')
})

test('should try create solicitation with empty fields and return 412', async({ client, assert}) => {
  const company = await Factory.model('App/Models/Company').make({ area_of_expertise: [] })
  await defaultClient.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  let solicitation = await Factory.model('App/Models/Solicitation').make()
  solicitation = solicitation.toJSON()
  solicitation.description = 'description_test'

  const response = await client.post(`company/${company.id}/solicitation`).send(solicitation).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 412)
  assert.equal(response.body.status, 412)
  assert.equal(response.body.message, 'INCOMPLETE_COMPANY_BASIC_DATA')
})

test('should create solicitation whithout logged User and return 401', async({ client, assert}) => {
  const response = await client.post(`company/1/solicitation`).end()

  assert.equal(response.status, 401)
})

test('should create solicitation with PARTNER role and return 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })

  const response = await client.post(`company/1/solicitation`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 401)
})

test('should create solicitation with ADMIN role and return 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const response = await client.post(`company/1/solicitation`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 401)
})
