'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Permissions')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultPartner = null

beforeEach(async () => {
  await clearDataBase()
  const client = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await client.companies().save(userCompany, (row) => {
  row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
})

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER', permissions: [] })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: client.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    client_viewed_in: null,
    partner_viewed_in: null,
    admin_viewed_in: null
  })
})

test('admin should try to access the route with the correct permissions and return 200', async({ client, assert}) => {
  const admin = await Factory.model('App/Models/User').create({ type: 'ADMIN', permissions: ["SOLICITATION"] })
  const response = await client.get(`solicitation/1`).query({company_id: 1}).loginVia(admin).end()
  assert.equal(response.status, 200)
  assert.isNotNull(response.body.id)
})

test('admin should try to access the route with the wrong permissions and return 401', async({ client, assert}) => {
  const admin = await Factory.model('App/Models/User').create({ type: 'ADMIN', permissions: [] })
  const response = await client.get(`solicitation/1`).query({company_id: 1}).loginVia(admin).end()
  assert.equal(response.status, 401)
})

test('partner should try to access the route with the correct permissions and return 200', async({ client, assert}) => {
  const response = await client.get(`solicitation/1`).query({company_id: 1}).loginVia(defaultPartner).end()
  assert.equal(response.status, 200)
  assert.isNotNull(response.body.id)
})
//
test('client should try to access the route with the correct permissions and return 200', async({ client, assert}) => {
  const userClient = await Factory.model('App/Models/User').create({ type: 'CLIENT', permissions: [] })
  await userClient.companies().attach([ 1 ], (row) => {
    row.permissions = ["REPORT","SOLICITATION"]
  })
  const response = await client.get(`solicitation/1`).query({company_id: 1}).loginVia(userClient).end()
  assert.equal(response.status, 200)
  assert.isNotNull(response.body.id)
})

test('client should try to access the route with the wrong permissions and return 401', async({ client, assert}) => {
  const userClient = await Factory.model('App/Models/User').create({ type: 'CLIENT', permissions: [] })
  await userClient.companies().attach([ 1 ], (row) => {
    row.permissions = ["REPORT",]
  })
  const response = await client.get(`solicitation/1`).query({company_id: 1}).loginVia(userClient).end()
  assert.equal(response.status, 401)
})
