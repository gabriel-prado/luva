'use strict'
const { CronJob } = use('cron')

class CronService {

  constructor (PartnerService, SolicitationService) {
    this.timeZone = 'America/Sao_Paulo'
    this.partnerService = PartnerService
    this.solicitationService = SolicitationService
  }

  start () {
    const halfAnHour = this.getHalfAnHourJob()
    const onceADay = this.getOnceADayJob()

    halfAnHour.start()
    onceADay.start()
  }

  getHalfAnHourJob () {
    return new CronJob({
      cronTime: '00 */30 * * * *',
      onTick: async () => {
        const promises = [
          this.partnerService.partnerNotAccept(),
          this.partnerService.partnerDelayed(),
          this.solicitationService.delayed()
        ]

        await Promise.all(promises)
      },
      start: false,
      runOnInit: true,
      timeZone: this.timeZone
    })
  }

  getOnceADayJob () {
    return new CronJob({
      cronTime: '00 00 */24 * * *',
      onTick: async () => {
        await this.solicitationService.autoFinalize()
      },
      start: false,
      runOnInit: true,
      timeZone: this.timeZone
    })
  }

}

module.exports = CronService
