'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const CompanyModel = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('User - POST /client')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
})

test('should create client user and return token', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let company = await Factory.model('App/Models/Company').make()
  user = user.toJSON()
  user.company = company.toJSON()
  user.password = 'senha123'

  const response =
    await client
      .post('client')
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)

  const userInDatabase = await UserModel.findBy('email', user.email)
  userInDatabase.company = (await userInDatabase.companies().fetch()).toJSON()

  assert.isNotNull(userInDatabase)
  assert.sameDeepMembers(userInDatabase.company[0].pivot.permissions, ['COMPANY', 'SOLICITATION', 'USER', 'CREDIT'])

  const companyInDatabase = await CompanyModel.findBy('cnpj', user.company.cnpj)
  assert.isNotNull(companyInDatabase)
  assert.isNotNull(response.body.token)
})

test('should try create user without email and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let company = await Factory.model('App/Models/Company').make()
  user = user.toJSON()
  user.company = company.toJSON()
  delete user.email

  const response = await client.post('client').send(user).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'email')
})

test('should try create client user without cnpj and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  let company = await Factory.model('App/Models/Company').make()
  user = user.toJSON()
  user.company = company.toJSON()
  delete user.company.cnpj

  const response = await client.post('client').send(user).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 400)
})
