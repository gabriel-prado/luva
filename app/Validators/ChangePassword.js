'use strict'

class ChangePassword {
  get rules () {
    return {
      code: 'required|string',
      newPassword: 'required|string',
      confirmPassword: 'required|string|same:newPassword'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'code',
      'newPassword',
      'confirmPassword'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = ChangePassword
