'use strict'

const Schema = use('Schema')

class TransactionSchema extends Schema {
  up () {
    this.create('transactions', (table) => {
      table.increments()
      table.date('date')
      table.boolean('expire').notNullable().defaultTo(false)
      table.float('value').notNullable()
      table.enum('operation', ['ADD', 'REMOVE']).notNullable()
      table.integer('solicitation_id').unsigned()
      table.foreign('solicitation_id').references('solicitations.id')
      table.float('balance_expiring').notNullable()
      table.float('balance_not_expiring').notNullable()
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('transactions')
  }
}

module.exports = TransactionSchema
