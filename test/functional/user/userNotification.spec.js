'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Notification - GET /user/notification')

let clientUser
let userCompany

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should list all notifications for CLIENT user and return status 200', async({ client, assert}) => {
  const adminUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    client_viewed_in: null
  })

  await Factory.model('App/Models/Message').createMany(3, {
    solicitation_id: solicitation.id,
    chat_type: 'CLIENT',
    user_id: adminUser.id
  })

  const response = await client.get('user/notification').query({company_id: 1}).loginVia(clientUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.total, 3)
  assert.lengthOf(response.body.solicitations, 1)
  assert.equal(response.body.solicitations[0].id, solicitation.id)
})

test('should list all notifications for ADMIN user and return status 200', async({ client, assert}) => {
  const adminUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const solicitations = await Factory.model('App/Models/Solicitation').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    admin_viewed_in: null
  })

  for (let i = 0; i < solicitations.length; i++) {
    await Factory.model('App/Models/Message').createMany(3, {
      solicitation_id: solicitations[i].id,
      chat_type: 'CLIENT',
      user_id: clientUser.id
    })
  }

  const response = await client.get('user/notification').query({company_id: 1}).loginVia(adminUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.total, 6)
  assert.lengthOf(response.body.solicitations, 2)
})

test('should list all notifications for PARTNER user and return status 200', async({ client, assert}) => {
  const partnerUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partnerUser.id, status: 'ACTIVE' })
  const adminUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    partner_viewed_in: null,
    partner_id: partnerUser.id
  })

  await Factory.model('App/Models/Message').createMany(3, {
    solicitation_id: solicitation.id,
    chat_type: 'PARTNER',
    user_id: adminUser.id
  })

  await Factory.model('App/Models/Message').createMany(1, {
    solicitation_id: solicitation.id,
    chat_type: 'CLIENT',
    user_id: adminUser.id
  })

  const response = await client.get('user/notification').query({company_id: 1}).loginVia(partnerUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.total, 3)
  assert.lengthOf(response.body.solicitations, 1)
  assert.equal(response.body.solicitations[0].id, solicitation.id)
})

test('should return 401 for not logged user', async ({ client, assert }) => {
  const response = await client.get('user/notification').end()

  assert.equal(response.status, 401)
})
