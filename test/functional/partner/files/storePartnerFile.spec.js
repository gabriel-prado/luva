'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const Drive = use('Drive')
const { test, trait, beforeEach } = use('Test/Suite')('Partner File - POST /partner/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultPartner = null

beforeEach(async () => {
  await clearDataBase()
  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })
})

test('should save file and return status 200', async({ client, assert }) => {
  await Drive.put('file.pdf', Buffer.from('Hello world!'))
  let file = await Drive.disk('local').getStream('file.pdf')

  const response =
    await
      client
        .post(`partner/${defaultPartner.id}/file`)
        .attach('file', file)
        .query({company_id: 1})
        .loginVia(defaultPartner)
        .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.url, 'file.pdf')

  await Drive.disk('local').delete('file.pdf')
})

test('admin should save file and return status 200', async({ client, assert }) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  await Drive.put('file.pdf', Buffer.from('Hello world!'))
  let file = await Drive.disk('local').getStream('file.pdf')

  const response =
    await
      client
        .post(`partner/${defaultPartner.id}/file`)
        .attach('file', file)
        .query({company_id: 1})
        .loginVia(admin)
        .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.url, 'file.pdf')

  await Drive.disk('local').delete('file.pdf')
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.post(`partner/${defaultPartner.id}/file`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.post(`partner/${defaultPartner.id}/file`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
