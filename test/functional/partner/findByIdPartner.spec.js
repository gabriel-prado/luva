'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Partner - GET /partner/:id')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should get Partner for partner user and return 200', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let partner = await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.get(`partner/${user.id}`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, user.name)
  assert.equal(response.body.partner.document_number, partner.document_number)
  assert.notEqual(response.body.partner.notes, partner.notes)
  assert.notEqual(response.body.partner.status, partner.status)
})

test('should get Partner for admin user and return 200', async({ client, assert}) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let partner = await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.get(`partner/${user.id}`).query({company_id: 1}).loginVia(admin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, user.name)
  assert.equal(response.body.partner.document_number, partner.document_number)
  assert.equal(response.body.partner.notes, partner.notes)
  assert.equal(response.body.partner.status, partner.status)
})

test('should get Partner whithout logged User and return 401', async({ client, assert}) => {
  const response = await client.get(`partner/1`).end()

  assert.equal(response.status, 401)
})

test('should get Partner with CLIENT role and return 401', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  user = user.toJSON()
  const response = await client.get(`partner/${user.id}`).query({company_id: 1}).loginVia(user).send(user).end()

  assert.equal(response.status, 401)
})
