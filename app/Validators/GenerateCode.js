'use strict'

class GenerateCode {
  get rules () {
    return {
      email: 'required|email'
    }
  }

  get data () {
    this.ctx.request.body = this.ctx.request.only(['email'])
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = GenerateCode
