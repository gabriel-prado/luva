'use strict'

const Schema = use('Schema')

class RemoveClientUpFromUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.dropColumn('client_up')
    })
  }

  down () {
    this.table('users', (table) => {
      table.boolean('client_up').notNullable().defaultTo(false)
    })
  }
}

module.exports = RemoveClientUpFromUserSchema
