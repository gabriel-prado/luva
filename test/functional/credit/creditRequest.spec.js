'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('CreditRequest - POST /company/:company_id/credit/request')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should request credit for client user and return 204', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create()
  await user.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })

  const response =
    await client
      .post(`company/${company.id}/credit/request`)
      .send({ value: 500 })
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 204)
})

test('Should request credit for a company that it does not belong and return 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create()
  await user.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })

  const company2 = await Factory.model('App/Models/Company').create()

  const response =
    await client
      .post(`company/${company2.id}/credit/request`)
      .send({ value: 500 })
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 404)
})

test('should request credit for ADMIN user and get 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const response = await client.post('company/1/credit/request').send({ value: 100 }).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 401)
})

test('should request credit for PARTNER user and get 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.post('company/1/credit/request').send({ value: 200 }).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 401)
})

test('should request credit for not logged user', async ({ client, assert }) => {
  const response = await client.post('company/1/credit/request').end()

  assert.equal(response.status, 401)
})
