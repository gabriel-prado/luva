'use strict'

const Route = use('Route')

Route
  .resource('company.transaction', 'TransactionController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN,CLIENT', 'access:CREDIT'])
