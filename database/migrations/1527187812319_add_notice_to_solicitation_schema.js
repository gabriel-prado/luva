'use strict'

const Schema = use('Schema')

class AddNoticeToSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.datetime('notice_time')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropColumn('notice_time')
    })
  }
}

module.exports = AddNoticeToSolicitationSchema
