'use strict'

const Route = use('Route')

Route.group('Authentication', () => {
  Route.post('/login', 'AuthController.login')
  Route.get('/me', 'AuthController.me').middleware(['auth', 'acl:ADMIN,CLIENT,PARTNER'])
}).prefix('auth')
