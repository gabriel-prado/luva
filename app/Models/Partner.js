'use strict'

const Model = use('Model')

class Partner extends Model {
  getContact(contact) {
    return (!!contact) ? JSON.parse(contact) : {}
  }

  setContact(contact) {
    return (!!contact) ? JSON.stringify(contact) : '{}'
  }

  getAddress(address) {
    return (!!address) ? JSON.parse(address) : {}
  }

  setAddress(address) {
    return (!!address) ? JSON.stringify(address) : '{}'
  }

  getFiles(files) {
    return (!!files) ? JSON.parse(files) : []
  }

  setFiles(files) {
    return (!!files) ? JSON.stringify(files) : '[]'
  }

  getSpecialty(specialty) {
    return (!!specialty) ? JSON.parse(specialty) : []
  }

  setSpecialty(specialty) {
    return (!!specialty) ? JSON.stringify(specialty) : '[]'
  }

  getBank(bank) {
    return (!!bank) ? JSON.parse(bank) : {}
  }

  setBank(bank) {
    return (!!bank) ? JSON.stringify(bank) : '{}'
  }

  getComplementaryData(complementaryData) {
    return (!!complementaryData) ? JSON.parse(complementaryData) : {}
  }

  setComplementaryData(complementaryData) {
    return (!!complementaryData) ? JSON.stringify(complementaryData) : '{}'
  }

  user() {
    return this.belongsTo('App/Models/User', 'id', 'id')
  }

  solicitations() {
    return this.hasMany('App/Models/Solicitation', 'id', 'partner_id')
  }
}

module.exports = Partner
