'use strict'

class SupportController {

  static get inject () {
    return ['App/Services/Support']
  }

  constructor (SupportService) {
    this.supportService = SupportService
  }

  async support ({ request, auth }) {
    return this.supportService.sendMail(auth.user.name, request.body.phone, await auth.user.isClientUp())
  }

}

module.exports = SupportController
