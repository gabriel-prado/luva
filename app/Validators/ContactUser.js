'use strict'

class ContactUser {
  get rules () {
    return {
      name: 'required|string',
      email: 'required|email',
      description: 'required|string',
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'description'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = ContactUser
