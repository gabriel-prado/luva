'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Transaction - GET /company/:id/transaction')

let clientUser
let userCompany

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should list all transactions for CLIENT user and return status 200', async({ client, assert}) => {
  await _addCredit(200)

  const response = await client.get(`company/${userCompany.id}/transaction`).query({company_id: 1, all: true}).loginVia(clientUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 20)
})

test('should list all transactions for ADMIN user and return status 200', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  await _addCredit(200)

  const response = await client.get(`company/${userCompany.id}/transaction`).query({company_id: 1, all: true}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 20)
})

test('should not list transactions for PARTNER user and return status 401', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.get(`company/${userCompany.id}/transaction`).query({company_id: 1, all: true}).loginVia(user).end()
  assert.equal(response.status, 401)
})

test('Should show the error message because the CLIENT user does not have permission in the company', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await _addCredit(200)

  const response = await client.get(`company/${userCompany.id}/transaction`).query({company_id: 1, all: true}).loginVia(user).end()

  assert.equal(response.status, 401)
})

test('should not list transactions for not logged user', async ({ client, assert }) => {
  const response = await client.get(`company/${userCompany.id}/transaction`).end()

  assert.equal(response.status, 401)
})

const _addCredit = async (value, expire = false) => {
  const attributes = {
    value,
    operation: 'ADD',
    company_id: 1
  }

  if (expire) {
    attributes.balance_expiring = value
    attributes.balance_not_expiring = 0
    attributes.expire = true
  } else {
    attributes.balance_expiring = 0
    attributes.balance_not_expiring = value
  }

  await Factory.model('App/Models/Transaction').createMany(20, attributes)
}
