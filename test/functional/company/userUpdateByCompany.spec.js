'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User by Company - PUT /company/id/user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })

  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should update user and return status 200', async({ client, assert }) => {
  const user = defaultClient.toJSON()
  user.name = 'new_name'
  user.email = 'new_email@email.com'
  user.oab_number = '1234'
  user.prefix_oab = 'OAB/RJ'
  user.permissions = ["SOLICITATION"]

  const response =
    await client
      .put(`company/${defaultCompany.id}/user/${defaultClient.id}`)
      .send(user)
      .query({company_id: defaultCompany.id})
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)

  assert.equal(response.body.name, user.name)
  assert.equal(response.body.email, user.email)

  const userInDatabase = await UserModel.find(user.id)
  assert.equal(userInDatabase.name, user.name)
  assert.equal(userInDatabase.email, user.email)
  assert.notEqual(userInDatabase.oab_number, '1234')
  assert.notEqual(userInDatabase.prefix_oab, 'OAB/RJ')

  const company = (await userInDatabase.companies().fetch()).toJSON()[0]
  assert.deepEqual(company.pivot.permissions, ["SOLICITATION"])
})

test('should update user with same email and return status 400', async({ client, assert }) => {
  const user = defaultClient.toJSON()
  user.name = 'new_name'

  const response =
    await client
      .put(`company/${defaultCompany.id}/user/${defaultClient.id}`)
      .send(user)
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].message, 'unique validation failed on email')
})

test('should not get user for not logged user', async ({ client, assert }) => {
  const response = await client.put(`company/${defaultCompany.id}/user/${defaultClient.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.put(`company/${defaultCompany.id}/user/${defaultClient.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
