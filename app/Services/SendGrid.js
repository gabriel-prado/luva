'use strict'

use('numeral/locales')
const Numeral = use('numeral')
const sgMail = require('@sendgrid/mail')
const Env = use('Env')
sgMail.setApiKey(Env.get('SENDGRID_API_KEY'))

class SendGridService {

  send (msg, clientUp) {
    msg.substitutions = {
      ...this._getDefaultData(clientUp),
      ...msg.substitutions
    }

    return sgMail.send(msg)
  }

  _getDefaultData (clientUp = false) {
    return {
      logo_url: Env.get( (clientUp) ? 'LOGO_UP_URL' : 'LOGO_LUVA_URL' ),
      btn_color: Env.get( (clientUp) ? 'BTN_UP_COLOR' : 'BTN_LUVA_COLOR' ),
      base_url: Env.get( (clientUp) ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL' ),
      alt_img: clientUp ? 'Up Legal' : 'Luva Assessoria'
    }
  }

  buildClientClassifyMessage (client, partner, company, solicitation, solicitationType, slaValue) {
    const clientUp = company.client_up
    const subjectPrefix = clientUp ? 'Up Legal' : 'Luva Assessoria'
    const prefixUrl = Env.get(clientUp ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL')
    Numeral.locale('pt-br')

    return {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: client.email,
      subject: `${subjectPrefix} - Nova solicitação`,
      templateId: Env.get('CLASSIFY_CLIENT_TEMPLATE_ID'),
      substitutions: {
        name: solicitation.name,
        partner: (partner) ? partner.name : undefined,
        partnerDisplay: (partner) ? 'block' : 'none',
        type: solicitationType.name,
        company: company.name,
        sla: slaValue,
        value: Numeral(solicitation.value).format('$ 0,0.00'),
        link: `${prefixUrl}/solicitacao/${solicitation.id}`
      }
    }
  }

  buildPartnerClassifyMessage (partner, company, solicitation, solicitationType, slaValue) {
    return {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: partner.email,
      subject: `Luva Assessoria - Nova solicitação`,
      templateId: Env.get('CLASSIFY_PARTNER_TEMPLATE_ID'),
      substitutions: {
        name: solicitation.name,
        type: solicitationType.name,
        company: company.name,
        sla: slaValue,
        link: `${Env.get('FRONTEND_LUVA_URL')}/solicitacao/${solicitation.id}`,
        specialty: solicitation.specialty
      }
    }
  }

  buildActivatedPartnerMail(user) {
    return {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: user.email,
      subject: `Luva Assessoria - Conta ativada`,
      templateId: Env.get('ACTIVATED_PARTNER_TEMPLATE_ID'),
      substitutions: {
        name: user.name,
        link: Env.get('FRONTEND_LUVA_URL')
      }
    }
  }

  buildContactMail(data) {
    return {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: 'Up Legal - dúvida',
      templateId: Env.get('UP_CONTACT_TEMPLATE_ID'),
      substitutions: {
        name: data.name,
        email: data.email,
        description: data.description
      }
    }
  }

  buildClientAttach(user, company, owner, isNewUser = false) {
    const isClientUp = company.client_up
    const subjectPrefix = isClientUp ? 'Up Legal' : 'Luva Assessoria'

    return {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: user.email,
      subject: `${subjectPrefix} - Cliente Associado`,
      templateId: Env.get(isNewUser ? 'ATTACH_NEW_CLIENT_TEMPLATE_ID' : 'ATTACH_CLIENT_TEMPLATE_ID'),
      substitutions: {
        name: user.name,
        company: company.name,
        email: user.email,
        password: user.password,
        link: Env.get(isClientUp ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL'),
        owner_name: owner.name,
        platform_name: isClientUp ? 'Up Legal' : 'Luva Assessoria'
      }
    }
  }

}

module.exports = SendGridService
