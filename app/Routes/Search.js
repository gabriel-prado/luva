'use strict'

const Route = use('Route')

Route
  .get('search', 'SearchController.search')
  .middleware(['auth', 'acl:CLIENT,PARTNER,ADMIN', 'access:SOLICITATION'])
