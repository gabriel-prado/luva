'use strict'

const Schema = use('Schema')

class SolicitationTypeSchema extends Schema {
  up () {
    this.create('solicitation_types', (table) => {
      table.increments()
      table.text('name').notNullable()
      table.float('value')
      table.integer('response_time').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('solicitation_types')
  }
}

module.exports = SolicitationTypeSchema
