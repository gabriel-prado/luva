'use strict'

const UserModel = use('App/Models/User')

class AuthController {

  static get inject () {
    return ['App/Services/User']
  }

  constructor (UserService) {
    this.userService = UserService
  }

  async login({ request, response, auth }) {
    const { email, password } = request.all()

    const user = await UserModel.findBy('email', email)

    if (!user || (user.type === 'ADMIN' && !user.active)) {
      return response.status(401).send({})
    } else if(user.type === 'PARTNER') {
      const partner = await user.partner().fetch()
      if(partner.status !== 'ACTIVE') return response.status(401).send({})
    } else if (user.type === 'CLIENT') {
      if (!await user.hasAnyCompany()) {
        return response.status(401).send({ message: 'USER_HAS_NO_COMPANY' })
      } else if (!await user.hasAnyActiveCompany()) {
        return response.status(401).send({ message: 'USER_HAS_NO_ACTIVE_COMPANY' })
      }
    }

    const { token } = await auth.attempt(email, password)

    return { token: `Bearer ${token}`  }
  }

  async me({ request, auth }) {
    const user = auth.user
    user.companies = await user.companies().fetch()
    return user
  }

}

module.exports = AuthController
