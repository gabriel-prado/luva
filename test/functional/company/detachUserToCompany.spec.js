'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User by Company - DELETE /company/id/user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null
let userToRemove = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
  userToRemove = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  await userToRemove.companies().attach([ defaultCompany.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should detach user to company by client user and return status 200', async({ client, assert }) => {
  const response =
    await client
      .delete(`company/${defaultCompany.id}/user/${userToRemove.id}`)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 204)
  const userInDatabase = await UserModel.find(userToRemove.id)
  const companies = await userInDatabase.companies().fetch()
  assert.lengthOf(companies.toJSON(), 0)
})

test('should detach user to company by admin user and return status 200', async({ client, assert }) => {
  const response =
    await client
      .delete(`company/${defaultCompany.id}/user/${userToRemove.id}`)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 204)
  const userInDatabase = await UserModel.find(userToRemove.id)
  const companies = await userInDatabase.companies().fetch()
  assert.lengthOf(companies.toJSON(), 0)
})

test('should not detach user for not logged user', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}/user/${userToRemove.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.delete(`company/${defaultCompany.id}/user/${userToRemove.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
