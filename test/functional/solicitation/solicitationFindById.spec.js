'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation - GET /solicitation/id')

let defaultClient = null
let defaultPartner = null
let defaultAdmin = null
let defaultSolicitation = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    client_viewed_in: null,
    partner_viewed_in: null,
    admin_viewed_in: null
  })
})

test('should get solicitation by client user and return 200', async({ client, assert}) => {
  const response = await client.get(`solicitation/${defaultSolicitation.id}`).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.isNotNull(response.body.client_viewed_in)
  assert.isNotNull(response.body.partner)
  assert.isNotNull(response.body.client)
  assert.isNotNull(response.body.company)
  assert.notEqual(response.body.client_viewed_in, defaultSolicitation.client_viewed_in)
})

test('should get solicitation by partner user and return 200', async({ client, assert}) => {
  const response = await client.get(`solicitation/${defaultSolicitation.id}`).query({company_id: 1}).loginVia(defaultPartner).end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.isNotNull(response.body.partner_viewed_in)
  assert.isNotNull(response.body.partner)
  assert.isNotNull(response.body.client)
  assert.isNotNull(response.body.company)
  assert.notExists(response.body.note)
  assert.notExists(response.body.favorite)
  assert.notExists(response.body.tags)
  assert.notEqual(response.body.partner_viewed_in, defaultSolicitation.partner_viewed_in)
})

test('should get solicitation by admin user and return 200', async({ client, assert}) => {
  const response = await client.get(`solicitation/${defaultSolicitation.id}`).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.isNotNull(response.body.admin_viewed_in)
  assert.isNotNull(response.body.partner)
  assert.isNotNull(response.body.client)
  assert.isNotNull(response.body.company)
  assert.notEqual(response.body.admin_viewed_in, defaultSolicitation.admin_viewed_in)
})

test('should try get solicitation by another user and return 401', async({ client, assert}) => {
  let partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })
  const response = await client.get(`solicitation/${defaultSolicitation.id}`).query({company_id: 1}).loginVia(partner).end()

  assert.equal(response.status, 401)
})

test('should try get solicitation that does not exist and return 404', async({ client, assert}) => {
  const response = await client.get(`solicitation/3`).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 404)
})

test('should get solicitation without logged User and return 401', async({ client, assert }) => {
  const response = await client.get(`solicitation/1`).end()

  assert.equal(response.status, 401)
})
