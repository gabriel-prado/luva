'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const moment = use('moment')
const { test, trait, beforeEach } = use('Test/Suite')('Analytic - GET /analytics/sla-compliance')

let defaultAdmin = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({ status: 'ACTIVE' })
  await clientUser.companies().attach([company.id], (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: 1,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: moment().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss'),
    response_time: 5
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: 1,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: moment().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss'),
    response_time: 2
  })
})

test('should return the percentage of sla completed and return status 200', async({ client, assert}) => {
  const response = await
    client
      .get('/analytics/sla-compliance')
      .query({
        company_id: 1
      })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body[0].label, 'Cumprido')
  assert.equal(response.body[1].label, 'Estourado')
  assert.equal(response.body[0].value, 75)
  assert.equal(response.body[1].value, 25)
})

test('should not return the percentage of sla completed and return status 401', async ({ client, assert }) => {
  const response = await client.get('/analytics/sla-compliance').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('/analytics/sla-compliance').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get('/analytics/sla-compliance').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
