'use strict'

const Route = use('Route')

Route
  .resource('solicitation-type', 'SolicitationTypeController')
  .only(['index'])
  .middleware(['auth', 'access:SOLICITATION'])
