'use strict'

const UserModel = use('App/Models/User')
const Env = use('Env')
const Hash = use('Hash')
const Database = use('Database')

class UserService {

  constructor (SendGridService, SolicitationService, CompanyService) {
    this.sgMail = SendGridService
    this.solicitationService = SolicitationService
    this.companyService = CompanyService
  }

  async store (attributes, trx = null) {
    return await UserModel.create(attributes, trx)
  }

  async sendClientCreatedMail (user, company) {
    const subjectPrefix = company.client_up ? 'Up Legal' : 'Luva Assessoria'

    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `${subjectPrefix} - novo cliente cadastrado`,
      templateId: Env.get('CLIENT_CREATED_TEMPLATE_ID'),
      substitutions: {
        name: user.name,
        email: user.email,
        cnpj: company.cnpj,
        company_name: company.name
      }
    }

    await this.sgMail.send(msg, company.client_up)
  }

  async sendPartnerCreatedMail (user, partner) {
    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `Luva Assessoria - novo parceiro cadastrado`,
      templateId: Env.get('PARTNER_CREATED_TEMPLATE_ID'),
      substitutions: {
        name: user.name,
        email: user.email,
        entity_type: partner.entity_type === 'PF' ? 'Pessoa física' : 'Pesoa jurídica',
        document_number: partner.document_number,
        oab_number: `${partner.prefix_oab} ${partner.oab_number}`,
        document_type: partner.entity_type === 'PF' ? 'CPF' : 'CNPJ'
      }
    }

    await this.sgMail.send(msg)
  }

  async attachCompanies (user, ids, permissions = [], trx = null) {
    if (!Array.isArray(ids)) ids = [ids]

    await user.companies().attach(ids, (row) => {
      row.permissions = permissions
    }, trx)
  }

  async fetchCompanies (user, page = null, perPage = null, ids = []) {
    let query = user.companies().where('status', 'ACTIVE')
    if(ids.length) query.whereIn('companies.id', ids)
    if (user.type !== 'ADMIN') query.setHidden(['status', 'notes'])

    return await query.paginate(page, perPage)
  }

  async update(id, attributes, trx = null) {
    if (attributes.password) attributes.password = await Hash.make(attributes.password)
    else delete attributes.password

    const user = await UserModel.findOrFail(id)

    if (user.type !== 'ADMIN') {
      attributes.prefix_oab = null
      attributes.oab_number = null
      attributes.active = false
      attributes.permissions = null
    }

    user.merge(attributes)
    await user.save(trx)

    return user
  }

  async findByIdWithPartner(id, type) {
    return await UserModel
      .query()
      .where('id', id)
      .with('partner', (builder) => {
        if (type !== 'ADMIN') builder.setHidden(['status', 'notes'])
      })
      .first()
  }

  async notification(user) {
    const solicitationsList = (await this.solicitationService.list(null, null, {}, user)).data.filter((s) => !!s.__meta__.unread_messages)
    let total = 0

    const solicitations = solicitationsList.map((solicitation) => {
      total += solicitation.__meta__.unread_messages

      return { id: solicitation.id, name: solicitation.name, __meta__: solicitation.__meta__ }
    })

    return { total, solicitations }
  }

  async contact(data) {
    const email = this.sgMail.buildContactMail(data)
    await this.sgMail.send(email, true)
  }

  async listAdmins (page = null, perPage = null, user) {
    return await UserModel.query()
      .where('type', 'ADMIN')
      .whereNot('id', user.id)
      .paginate(page, perPage)
  }

  async showAdmin (id) {
    return await UserModel.findByOrFail({
      id: id,
      type: 'ADMIN'
    })
  }

  async show (id) {
    return await UserModel.findOrFail(id)
  }

  async storeClient(userAttrs, companyAttrs) {
    const trx = await Database.beginTransaction()
    const permissions = ['COMPANY', 'SOLICITATION', 'USER', 'CREDIT']

    try {
      const user = await this.store(userAttrs, trx)
      const company = await this.companyService.store(companyAttrs, user.type, trx)

      await this.attachCompanies(user, [company.id], permissions, trx)

      await this.sendClientCreatedMail(user, company)

      trx.commit()
    } catch(e) {
      trx.rollback()
      throw e
    }
  }
}

module.exports = UserService
