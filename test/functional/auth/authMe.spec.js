'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - GET /auth/me')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should retrieve authenticated user', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const response = await client.get('auth/me').query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.email, user.email)
})
