'use strict'

const Schema = use('Schema')

class AlterSolicitationFiles extends Schema {
  up () {
    this.alter('solicitations', (table) => {
      table.specificType('client_files', 'JSON').notNullable().alter()
      table.specificType('partner_files', 'JSON').notNullable().alter()
    })
  }

  down () {
    this.alter('solicitations', (table) => {
      table.specificType('client_files', 'JSON').alter()
      table.specificType('partner_files', 'JSON').alter()
    })
  }
}

module.exports = AlterSolicitationFiles
