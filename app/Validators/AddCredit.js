'use strict'

class AddCredit {
  get rules () {
    return {
      value: 'required|number',
      expire: 'required|boolean'
    }
  }

  get data () {
    this.ctx.request.body = this.ctx.request.only(['value', 'expire'])
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = AddCredit
