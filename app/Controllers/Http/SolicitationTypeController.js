'use strict'

class SolicitationTypeController {
  static get inject () {
    return ['App/Services/SolicitationType']
  }

  constructor (SolicitationTypeService) {
    this.solicitationTypeService = SolicitationTypeService
  }

  async index ({ request, auth }) {
    let { all, page, perPage } = request.get()

    page = (all) ? null : Number(page) || 1
    perPage = (all) ? null : Number(perPage) || 5

    return await this.solicitationTypeService.list(page, perPage)
  }
}

module.exports = SolicitationTypeController
