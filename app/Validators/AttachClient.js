'use strict'

class AttachClient {
  get rules () {
    return {
      name: 'required|string',
      email: 'required|email',
      password: 'required',
      type: 'required|in:CLIENT',
      photo_url: 'string',
      permissions: 'required|array'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'password',
      'photo_url',
      'permissions'
    ])

    body.type = 'CLIENT'

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = AttachClient
