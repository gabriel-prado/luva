'use strict'

const Schema = use('Schema')

class SolicitationMessagesRename extends Schema {
  up () {
    this.rename('solicitation_messages', 'messages')
  }

  down () {
    this.rename('messages', 'solicitation_messages')
  }
}

module.exports = SolicitationMessagesRename
