'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('User by Company - GET /company/id/user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  const anotherClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  await anotherClient.companies().attach([ defaultCompany.id ], (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should get user by company for client user and return status 200', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user/${defaultClient.id}`).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.id, defaultClient.id)
  assert.isNotNull(response.body.companies[0].pivot.permissions)
})

test('should get user by company for admin user and return status 200', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user/${defaultClient.id}`).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.id, defaultClient.id)
  assert.isNotNull(response.body.companies[0].pivot.permissions)
})

test('should not get user for not logged user', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/user/${defaultClient.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get(`company/${defaultCompany.id}/user/${defaultClient.id}`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
