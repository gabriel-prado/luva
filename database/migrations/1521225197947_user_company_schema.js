'use strict'

const Schema = use('Schema')

class UserCompanySchema extends Schema {
  up () {
    this.create('company_user', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable()
      table.foreign('user_id').references('users.id')
      table.integer('company_id').unsigned().notNullable()
      table.foreign('company_id').references('companies.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('company_user')
  }
}

module.exports = UserCompanySchema
