'use strict'

const Schema = use('Schema')

class AdminSolicitationSchema extends Schema {
  up () {
    this.table('solicitations', (table) => {
      table.integer('admin_id').unsigned()
      table.foreign('admin_id').references('users.id')
    })
  }

  down () {
    this.table('solicitations', (table) => {
      table.dropForeign('admin_id')
      table.dropColumn('admin_id')
    })
  }
}

module.exports = AdminSolicitationSchema
