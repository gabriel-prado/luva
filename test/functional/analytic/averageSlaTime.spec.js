'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const moment = use('moment')
const { test, trait, beforeEach } = use('Test/Suite')('Analytic - GET /analytics/average-sla-time')

let defaultAdmin = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({ status: 'ACTIVE' })
  await clientUser.companies().attach([company.id], (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  const type1 = await Factory.model('App/Models/SolicitationType').create({ name: 'type1' })
  const type2 = await Factory.model('App/Models/SolicitationType').create({ name: 'type2' })
  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: 1,
    solicitation_type_id: type1.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: moment().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss'),
    response_time: 12
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: 1,
    solicitation_type_id: type2.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: moment().add(3, 'hours').format('YYYY-MM-DD HH:mm:ss'),
    response_time: 12
  })
})

test('should return the amount of solicitation per company and return status 200', async({ client, assert}) => {
  const start_date = moment().subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss')
  const end_date = moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss')

  const response = await
    client
      .get('/analytics/average-sla-time')
      .query({
        company_id: 1,
        start_date,
        end_date
      })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 2)
  assert.equal(response.body[0].label, 'type1')
  assert.equal(response.body[1].label, 'type2')
  assert.equal(response.body[0].value, 3)
  assert.equal(response.body[1].value, 3)
})

test('should not return the amount of solicitation per company and return status 401', async ({ client, assert }) => {
  const response = await client.get('/analytics/average-sla-time').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('/analytics/average-sla-time').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get('/analytics/average-sla-time').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
