'use strict'

class UpdateCompany {
  get rules () {
    return {
      name: 'string',
      cnpj: 'string',
      status: 'string|in:ACTIVE,INACTIVE',
      society: 'string',
      notes: 'string',
      client_up: 'boolean',
      activity: 'string',
      size: 'string',
      area_of_expertise: 'array',
      taxation_regime: 'string',
      files: 'array',
      unlimited_balance: 'boolean',
      plan_value: 'number',
      address: 'object',
      'address.zip_code': 'string',
      'address.state': 'string',
      'address.city': 'string',
      'address.neighborhood': 'string',
      'address.street': 'string',
      'address.number': 'number',
      'address.complement': 'string',
      complementary_data: 'object',
      'complementary_data.photo_url': 'string',
      'complementary_data.trading_name': 'string',
      'complementary_data.state_inscription': 'string',
      'complementary_data.date_foundation': 'string',
      'complementary_data.social_capital': 'number',
      'complementary_data.employees': 'string',
      'complementary_data.company_syndicate': 'string',
      'complementary_data.employees_syndicate': 'string',
      'complementary_data.employee_recruitment_scheme': 'string',
      'complementary_data.employee_working_day': 'string',
      'complementary_data.billing': 'number',
      'complementary_data.billing_summary': 'string',
      contact: 'object',
      'contact.name': 'string',
      'contact.phone': 'string',
      'contact.email': 'string'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'cnpj',
      'society',
      'activity',
      'status',
      'notes',
      'size',
      'client_up',
      'taxation_regime',
      'complementary_data',
      'address',
      'contact',
      'unlimited_balance',
      'plan_value'
    ])

    body.area_of_expertise = this.ctx.request.input('area_of_expertise', [])
    body.files = this.ctx.request.input('files', [])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateCompany
