'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Search - GET /search')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultSolicitation = null
let defaultClient = null
let defaultPartner = null
let defaultAdmin = null
let userCompany = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    name: 'solicitation_name',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    note: 'solicitation_note'
  })
  await Factory.model('App/Models/Message').createMany(8, {
    solicitation_id: defaultSolicitation.id,
    user_id: defaultClient.id,
    chat_type: 'CLIENT'
  })
  await Factory.model('App/Models/Message').createMany(4, {
    solicitation_id: defaultSolicitation.id,
    user_id: defaultPartner.id,
    chat_type: 'PARTNER'
  })
})

test('should search for admin user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get('search')
      .query({ query: defaultSolicitation.note, company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
})

test('should search for client user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get('search')
      .query({ query: defaultSolicitation.name, company_id: 1 })
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
})

test('should search for partner user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get('search')
      .query({ query: defaultSolicitation.name, company_id: 1 })
      .loginVia(defaultPartner)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
})

test('should search for admin user by tag and return status 200', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    name: 'SOLICITATION_TAG',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    tags: ["TAG_UNICA"]
  })

  const response =
    await client
      .get('search')
      .query({ query: "TAG_UNICA", company_id: userCompany.id })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
  assert.equal(response.body[0].data.id, solicitation.id)
})

test('should not search for not logged user', async ({ client, assert }) => {
  const response =
    await client
      .get(`/search`)
      .end()

  assert.equal(response.status, 401)
})
