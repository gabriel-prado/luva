'use strict'

const Schema = use('Schema')

class ClientUpValueSolicitationTypeSchema extends Schema {
  up () {
    this.table('solicitation_types', (table) => {
      table.float('client_up_value')
    })
  }

  down () {
    this.table('solicitation_types', (table) => {
      table.dropColumn('client_up_value')
    })
  }
}

module.exports = ClientUpValueSolicitationTypeSchema
