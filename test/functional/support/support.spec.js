'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Support - POST /support')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should send with client role email', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make()
  await user.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response =
    await client
      .post('support')
      .send({ phone: '322222222' })
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 204)
})

test('should send with partner role email', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response =
    await client
      .post('support')
      .send({ phone: '322222222' })
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 204)
})

test('should try send mail without phone and return status 400', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })
  const response = await client.post('support').query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 400)
})

test('should not send mail for not logged user', async ({ client, assert }) => {
  const response = await client.post('support').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with admin role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const response = await client.post('support').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
