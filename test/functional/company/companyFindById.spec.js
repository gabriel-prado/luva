'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - GET /company/:id')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should get Company and return 200', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let company = await Factory.model('App/Models/Company').make()
  await user.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response = await client.get(`company/${user.id}`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.id, company.id)
  assert.equal(response.body.name, company.name)
  assert.exists(response.body.notes)
  assert.exists(response.body.status)
})

test('should get Company whithout logged User and return 401', async({ client, assert}) => {
  const response = await client.get(`company/1`).end()

  assert.equal(response.status, 401)
})

test('should get Company with PARTNER role and return 401', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  user = user.toJSON()
  const response = await client.get(`company/${user.id}`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 401)
})

test('should get Company with CLIENT role and return 200', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make()
  await user.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response = await client.get(`company/${company.id}`).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.id, company.id)
})
