'use strict'

use('numeral/locales')
const Numeral = use('numeral')
const SolicitationModel = use('App/Models/Solicitation')
const MessageModel = use('App/Models/Message')
const Database = use('Database')
const moment = use('moment')
const Env = use('Env')

class MessageService {

  constructor (SendGridService) {
    this.sgMail = SendGridService
  }

  async list (solicitationId, user, chatType, page = null, perPage = null, direction, orderBy) {
    const order = orderBy === 'id' ?  `${orderBy} ${direction}` : `${orderBy} ${direction}, id ${direction}`

    if (!await user.hasSolicitation(solicitationId)) {
      throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
    }

    return await
      MessageModel
        .query()
        .where({
          solicitation_id: solicitationId,
          chat_type: chatType
        })
        .orderByRaw(order)
        .with('user')
        .paginate(page, perPage)
  }

  store(attributes, trx = null) {
    return MessageModel.create(attributes, trx)
  }

  async create (attributes, solicitationId, user) {
    const solicitation = await SolicitationModel.findOrFail(solicitationId)
    if (solicitation.status === 'FINALIZED' || solicitation.status === 'CANCELED') return
    const trx = await Database.beginTransaction()

    try {
      const message = await this.store(attributes, trx)

      solicitation.merge(this.getNewStatusAndSla(message, solicitation, user))
      await solicitation.save(trx)

      await this.sendMails(user, solicitation, message)

      await trx.commit()

      return message
    } catch (e) {
      await trx.rollback()

      throw e
    }
  }

  async sendMails (user, solicitation, message) {
    const clientUp = await user.isClientUp()
    const subjectPrefix = clientUp ? 'Up Legal' : 'Luva Assessoria'
    const prefixUrl = Env.get(clientUp ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL')
    let query = ''
    let userName = ''
    let userType = ''
    let email = ''

    if (user.type === 'ADMIN') {
      userType = 'administrador'
      if (message.chat_type === 'PARTNER') {
        if (solicitation.partner_id) {
          const partner = await solicitation.partner().fetch()
          email = partner.email
        } else {
          return
        }
      } else {
        const client = await solicitation.client().fetch()
        if (!await client.hasCompany(solicitation.company_id)) return
        email = client.email
      }
    } else {
      email = Env.get('DEFAULT_ADMIN_MAIL')
      userName = user.name
      userType = (user.type === 'CLIENT') ? 'cliente' : 'parceiro'
      query = `?chatType=${user.type}`
    }

    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: email,
      subject: `${subjectPrefix} - Nova mensagem na solicitação #${solicitation.id}`,
      templateId: Env.get('NEW_MESSAGE_TEMPLATE_ID'),
      substitutions: {
        userName,
        userType,
        message: message.text,
        link: `${prefixUrl}/solicitacao/${solicitation.id}${query}`
      }
    }

    await this.sgMail.send(msg, clientUp)
  }

  getNewStatusAndSla(message, solicitation, user) {
    const currentStatus = solicitation.$originalAttributes.status
    const status = this.getNewStatus(message, solicitation, user)
    let partner_sla_end_date
    let partner_sla_start_date
    let sla_end_date
    let sla_start_date

    if ( (currentStatus === 'WAITING_FOR_PARTNER_RESPONSE' && status === 'WAITING_FOR_ADMIN_RESPONSE') ) {
      partner_sla_end_date = moment().format('YYYY-MM-DD HH:mm:ss')
    }

    if (currentStatus === 'WAITING_FOR_PARTNER_RESPONSE' && status === 'WAITING_FOR_CLIENT_RESPONSE') {
      partner_sla_end_date = moment().format('YYYY-MM-DD HH:mm:ss')
      sla_end_date = moment().format('YYYY-MM-DD HH:mm:ss')
    }

    if ( (currentStatus === 'WAITING_FOR_ADMIN_RESPONSE' && status === 'WAITING_FOR_PARTNER_RESPONSE') ) {
      partner_sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
      partner_sla_end_date = null
    }

    if ( (currentStatus === 'WAITING_FOR_CLIENT_RESPONSE' && status === 'WAITING_FOR_PARTNER_RESPONSE') ) {
      partner_sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
      partner_sla_end_date = null
      sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
      sla_end_date = null
    }

    if ( (currentStatus === 'WAITING_FOR_ADMIN_RESPONSE' && status === 'WAITING_FOR_CLIENT_RESPONSE') ) {
      sla_end_date = moment().format('YYYY-MM-DD HH:mm:ss')
    }

    if ( (currentStatus === 'WAITING_FOR_CLIENT_RESPONSE' && status === 'WAITING_FOR_ADMIN_RESPONSE') ) {
      sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
      sla_end_date = null
    }

    return { status, partner_sla_end_date, partner_sla_start_date, sla_end_date, sla_start_date }
  }

  getNewStatus(message, solicitation, user) {
    if (solicitation.status === 'WAITING_FOR_PARTNER_RESPONSE' && user.type === 'PARTNER') {
      return 'WAITING_FOR_ADMIN_RESPONSE'
    }

    if ( (solicitation.status === 'WAITING_FOR_ADMIN_RESPONSE' || solicitation.status === 'WAITING_FOR_CLASSIFICATION')
      && user.type === 'ADMIN' && message.chat_type === 'CLIENT') {
      return 'WAITING_FOR_CLIENT_RESPONSE'
    }

    if (solicitation.status === 'WAITING_FOR_ADMIN_RESPONSE' && user.type === 'ADMIN' && message.chat_type === 'PARTNER') {
      return 'WAITING_FOR_PARTNER_RESPONSE'
    }

    if (solicitation.status === 'WAITING_FOR_CLIENT_RESPONSE' && user.type === 'CLIENT') {
      return 'WAITING_FOR_ADMIN_RESPONSE'
    }

    if (solicitation.status === 'WAITING_FOR_PARTNER_RESPONSE' && user.type === 'ADMIN' && message.chat_type === 'CLIENT') {
      return 'WAITING_FOR_CLIENT_RESPONSE'
    }

    if (solicitation.status === 'WAITING_FOR_CLIENT_RESPONSE' && user.type === 'ADMIN' && message.chat_type === 'PARTNER') {
      return 'WAITING_FOR_PARTNER_RESPONSE'
    }
  }

  buildCancelMessage(chatType, userId, solicitationId) {
    return {
      solicitation_id: solicitationId,
      user_id: userId,
      chat_type: chatType,
      type: 'ACTION',
      text: 'Solicitação cancelada'
    }
  }

  async buildPartnerAcceptMessage(chatType, user, solicitationId) {
    const partner = (await user.partner())[0]

    return {
      solicitation_id: solicitationId,
      user_id: user.id,
      chat_type: chatType,
      type: 'ACTION',
      text: `Solicitação aceita pelo parceiro "${user.name}", ${partner.prefix_oab} ${partner.oab_number}`
    }
  }

  async buildPartnerRefusedMessage(chatType, user, solicitationId) {
    const partner = (await user.partner())[0]

    return {
      solicitation_id: solicitationId,
      user_id: user.id,
      chat_type: chatType,
      type: 'ACTION',
      text: `Solicitação recusada pelo parceiro "${user.name}". Aguardando novo parceiro`
    }
  }

  buildClientClassifyMessage (chatType, partner, solicitationId) {
    return {
      solicitation_id: solicitationId,
      user_id: partner.id,
      chat_type: chatType,
      type: 'ACTION',
      text: `Aguardando confirmação do parceiro.`
    }
  }

  buildPartnerClassifyMessage (chatType, partner, solicitationId) {
    return {
      solicitation_id: solicitationId,
      user_id: partner.id,
      chat_type: chatType,
      type: 'ACTION',
      text: `Parceiro selecionado "${partner.name}", aguardando confirmação.`
    }
  }

  async buildSolicitationClassifyMessage (chatType, solicitation, unlimitedBalance) {
    const solicitationType = await solicitation.solicitationType().fetch()
    let text = `Solicitação classificada como "${solicitationType.name}", SLA máximo definido para ${solicitation.response_time}hrs`

    if (!unlimitedBalance) {
      Numeral.locale('pt-br')
      const value = Numeral(solicitation.value).format('$ 0,0.00')
      text = `${text}, Valor: ${value}`
    }

    return {
      solicitation_id: solicitation.id,
      user_id: solicitation.client_id,
      chat_type: chatType,
      type: 'ACTION',
      text
    }
  }

  buildFinalizedMessage (chatType, userId, solicitationId) {
    return {
      solicitation_id: solicitationId,
      user_id: userId,
      chat_type: chatType,
      type: 'ACTION',
      text: `Solicitação finalizada.`
    }
  }

  buildMessageFromAdminIsResponsible(chatType, user, solicitationId) {
    const msg = {
      solicitation_id: solicitationId,
      user_id: user.id,
      chat_type: chatType,
      type: 'ACTION',
      text: `Parceiro selecionado "${user.name}"`
    }

    if (user.prefix_oab && user.oab_number) {
      msg.text = `${msg.text}, ${user.prefix_oab} ${user.oab_number}`
    }

    return msg
  }

  buildPartnerRemovedMessage (chatType, userId, userName, solicitationId) {
    return {
      solicitation_id: solicitationId,
      user_id: userId,
      chat_type: chatType,
      type: 'ACTION',
      text: `O parceiro "${userName}" foi removido da solicitação. Aguardando novo parceiro`
    }
  }
}

module.exports = MessageService
