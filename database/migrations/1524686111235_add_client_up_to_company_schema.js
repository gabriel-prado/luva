'use strict'

const Schema = use('Schema')

class AddClientUpToCompanySchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.boolean('client_up').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('companies', (table) => {
      table.dropColumn('client_up')
    })
  }
}

module.exports = AddClientUpToCompanySchema
