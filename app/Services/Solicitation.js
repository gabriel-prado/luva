'use strict'

use('numeral/locales')
const Numeral = use('numeral')
const SolicitationModel = use('App/Models/Solicitation')
const moment = use('moment')
const Database = use('Database')
const Env = use('Env')

class SolicitationService {

  constructor (MessageService, CompanyService, S3Service, CreditService, SendGridService) {
    this.messageService = MessageService
    this.companyService = CompanyService
    this.s3Service = S3Service
    this.creditService = CreditService
    this.sgMail = SendGridService

    this.columnsFile = {
      CLIENT: 'client_files',
      PARTNER: 'partner_files'
    }
    this.inverseChatType = {
      CLIENT: 'PARTNER',
      PARTNER: 'CLIENT'
    }
  }

  async list (page, perPage, filters, user) {
    let query =
      SolicitationModel
        .query()
        .with('client')
        .with('company', (builder) => {
          builder.setHidden(['status', 'notes', 'plan_value'])
        })
        .with('solicitationType')
        .withCount('messages as unread_messages', (builder) => {
          builder.where('messages.user_id', '!=', user.id)
          builder.where('solicitations.status', '!=', 'CANCELED')
          builder.where('solicitations.status', '!=', 'FINALIZED')
          builder.where(function() {
            const viewedInColumn = `solicitations.${user.type.toLowerCase()}_viewed_in`

            this
              .whereRaw(`messages.created_at > ${viewedInColumn}`)
              .orWhere(viewedInColumn, null)
          })

          if (user.type !== 'ADMIN') builder.where('chat_type', user.type)
        })
        .orderByRaw("FIELD(status, 'CANCELED', 'FINALIZED') ASC")

    query = this._applyFilters(query, filters, user)

    const resp = await query.paginate(page, perPage)

    resp.rows = resp.rows.map((_solicitation) => {
      _solicitation.$attributes = { ...this.getSla(_solicitation), ..._solicitation.$attributes }

      if (user.type !== 'ADMIN') {
        delete _solicitation.$attributes.note
        delete _solicitation.$attributes.favorite
        delete _solicitation.$attributes.tags
        if (user.type === 'CLIENT') delete _solicitation.$attributes.sla_partner
        if (user.type === 'PARTNER') _solicitation.$attributes.sla = _solicitation.$attributes.sla_partner
      }

      return _solicitation
    })

    return this._orderSolicitations(resp.toJSON())
  }

  _orderSolicitations (solicitations) {
    solicitations.data.sort((a, b) => {
      if (a.status === 'CANCELED' || a.status === 'FINALIZED') return 1
      if (b.status === 'CANCELED' || b.status === 'FINALIZED') return -1

      if (a.status !== b.status) {
        if (a.status === 'WAITING_FOR_CLASSIFICATION') return -1
        if (b.status === 'WAITING_FOR_CLASSIFICATION') return 1
      }

      const restA = a.sla.total - a.sla.current
      const restB = b.sla.total - b.sla.current

      if (restA > restB) return 1
      if (restA < restB) return -1

      const unreadA = a.__meta__.unread_messages
      const unreadB = b.__meta__.unread_messages

      if (unreadA > unreadB) return -1
      if (unreadA < unreadB) return 1

      return 0
    })

    return solicitations
  }

  _applyFilters (query, filters, user) {
    if (user.type === 'PARTNER') query.where('partner_id', user.id)

    if (user.type === 'CLIENT') {
      query.whereHas('company', (builder) => {
        builder.whereHas('users', (builder) => {
          builder.where('user_id', user.id)
          builder.whereRaw("JSON_CONTAINS(`company_user`.`permissions`, ?)", '["SOLICITATION"]')
        })
      })
    }

    if (user.type === 'ADMIN') {
      if (filters.favorite === 'true') filters.favorite = true
      if (filters.favorite === 'false') filters.favorite = false

      if (filters.favorite !== null && filters.favorite !== undefined) query.where('favorite', filters.favorite)
    }


    if (!!filters.start_date) query.where('created_at', '>=', filters.start_date)
    if (!!filters.end_date) query.where('created_at', '<=', filters.end_date)
    if (!!filters.companies_id) query.whereIn('company_id', filters.companies_id)
    if (!!filters.types_id) query.whereIn('solicitation_type_id', filters.types_id)
    if (!!filters.specialties) query.whereIn('specialty', filters.specialties)
    if (!!filters.status) query.whereIn('status', filters.status)

    return query
  }

  async create (companyId, user, attributes, description) {
    const company = await this.companyService.findByIdAndUser(companyId, user.id)

    if (!(await user.hasCompany(companyId))) throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')

    if(company.canCreateSolicitation()) {
      return this.store(attributes, description, user, company)
    } else {
      throw new Error('INCOMPLETE_COMPANY_BASIC_DATA')
    }
  }

  async _sendCreateMail (solicitation, description, company) {
    const clientUp = company.client_up
    const prefixUrl = Env.get(clientUp ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL')
    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `Luva Assessoria - Nova solicitação`,
      templateId: Env.get('SOLICITATION_CREATED_TEMPLATE_ID'),
      substitutions: {
        name: solicitation.name,
        company: company.name,
        link: `${prefixUrl}/solicitacao/${solicitation.id}`,
        description
      }
    }

    await this.sgMail.send(msg, clientUp)
  }

  async store (attributes, description, user, company = null) {
    const trx = await Database.beginTransaction()
    attributes.client_files = attributes.client_files || []
    attributes.partner_files = attributes.partner_files || []

    try {
      const solicitation = await SolicitationModel.create(attributes, trx)
      await this.createInitialMessages(solicitation.id, user, description, trx)
      await trx.commit()

      const resp = { ...this.getSla(solicitation), ...solicitation.toJSON() }

      if (user.type === 'CLIENT') {
        delete resp.sla_partner
        await this._sendCreateMail(solicitation, description, company)
      }
      if (user.type === 'PARTNER') resp.sla = resp.sla_partner

      return resp
    } catch (e) {
      await trx.rollback()
      throw e
    }
  }

  async createInitialMessages (solicitationId, user, description, trx) {
    const attributes = {
      solicitation_id: solicitationId,
      user_id: user.id,
      chat_type: 'CLIENT'
    }

    const action = {
      ...attributes,
      type: 'ACTION',
      text: 'Solicitação aberta'
    }

    const message = {
      ...attributes,
      type: 'TEXT',
      text: description
    }

    await this.messageService.store(action, trx)
    await this.messageService.store(message, trx)
  }

  addFile (solicitationId, type, fileName) {
    const query = {}
    const column = this.columnsFile[type]
    query[column] = Database.raw(`JSON_ARRAY_APPEND(${column}, "$", ?)`, [fileName])

    return Database
        .table('solicitations')
        .where('id', solicitationId)
        .update(query)
  }

  async uploadFile (file, solicitation, user, chatType) {
    chatType = user.type === 'ADMIN' ? chatType : user.type
    if (await user.hasSolicitation(solicitation.id)) {
      await this.s3Service.send(file, 's3_solicitation', solicitation.getBucketUrl(chatType))
      await this.addFile(solicitation.id, chatType, file.stream.filename)
      await this.createFileMessage(solicitation.id, user, file.stream.filename, chatType)
    } else {
      throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
    }
  }

  async forwardFile (id, fileName, chatType, user) {
    const solicitation = await SolicitationModel.findOrFail(id)
    const newName = this._fixDuplicateFileName(solicitation[this.columnsFile[chatType]], fileName)
    const source = solicitation.getBucketUrl(this.inverseChatType[chatType])
    const destination = solicitation.getBucketUrl(chatType)

    await this.s3Service.forward(fileName, newName, 's3_solicitation', source, destination)
    await this.addFile(solicitation.id, chatType, newName)
    return await this.createFileMessage(solicitation.id, user, newName, chatType)
  }

  _fixDuplicateFileName (arr, name) {
    while (arr.includes(name)) {
      name = `_${name}`
    }

    return name
  }

  async getFile (fileName, solicitation, user, chatType) {
    chatType = user.type === 'ADMIN' ? chatType : user.type
    const solicitationJSON = solicitation.toJSON()

    if (!solicitationJSON[this.columnsFile[chatType]].includes(fileName)) {
      throw new Error('THIS_FILE_DOES_NOT_EXISTS')
    } else if (await user.hasSolicitation(solicitation.id)) {
      return this.s3Service.get(fileName, 's3_solicitation', solicitation.getBucketUrl(chatType))
    } else {
      throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
    }
  }

  async createFileMessage (solicitationId, user, fileName, chatType) {
    const attributes = {
      solicitation_id: solicitationId,
      user_id: user.id,
      chat_type: chatType,
      type: 'FILE',
      text: fileName
    }

    return await this.messageService.create(attributes, solicitationId, user)
  }

  async show (id, user) {
    if (await user.hasSolicitation(id)) {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')

      const solicitation =
        await SolicitationModel
          .query()
          .where('id', id)
          .with('company')
          .with('partner')
          .with('client')
          .with('admin')
          .firstOrFail()

      const mergePayload = {
        CLIENT: { client_viewed_in: date },
        ADMIN: { admin_viewed_in: date },
        PARTNER: { partner_viewed_in: date }
      }

      solicitation.merge(mergePayload[user.type])
      await solicitation.save()
      const resp = { ...this.getSla(solicitation), ...solicitation.toJSON() }

      if (user.type !== 'ADMIN') {
        delete resp.note
        delete resp.favorite
        delete resp.tags
        if (user.type === 'CLIENT') delete resp.sla_partner
        if (user.type === 'PARTNER') resp.sla = resp.sla_partner
      }

      return resp
    } else {
      throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
    }
  }

  getSla(solicitation) {
    const responseTime = solicitation.response_time * 60 * 60 * 1000
    let slaEndDate = moment()
    let partnerSlaEndDate = moment()

    if (solicitation.status === 'WAITING_FOR_CLIENT_RESPONSE' && !!solicitation.sla_end_date) {
      slaEndDate = moment(solicitation.sla_end_date)
    } else if (solicitation.status === 'FINALIZED' || solicitation.status === 'CANCELED') {
      slaEndDate = moment(solicitation.sla_end_date)
      partnerSlaEndDate = moment(solicitation.partner_sla_end_date)
    } else if (solicitation.status === 'WAITING_FOR_ADMIN_RESPONSE') {
      partnerSlaEndDate = moment(solicitation.partner_sla_end_date)
    } else if (solicitation.status === 'WAITING_FOR_CLASSIFICATION' && solicitation.sla_end_date) {
      slaEndDate = moment(solicitation.sla_end_date)
    }

    const resp = {
      sla: {
        current: this._convertTimeMillisToHour( slaEndDate.subtract( moment(solicitation.sla_start_date).valueOf() ).valueOf() ),
        total: this._convertTimeMillisToHour(responseTime)
      },
      sla_partner: {
        current: this._convertTimeMillisToHour( partnerSlaEndDate.subtract( moment(solicitation.partner_sla_start_date).valueOf() ).valueOf() ),
        total: this._convertTimeMillisToHour(responseTime * 20 / 100)
      }
    }

    if (!solicitation.partner_sla_start_date) delete resp.sla_partner

    return resp
  }

  _convertTimeMillisToHour(time) {
    return Math.round(time / 60 / 60 / 1000)
  }

  async cancel(solicitationId, user) {
    if (!(await user.hasSolicitation(solicitationId))) {
      throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
    }

    const solicitation = await SolicitationModel.findOrFail(solicitationId)

    if (user.type === 'CLIENT' && solicitation.status !== 'WAITING_FOR_CLASSIFICATION') {
      throw new Error('SOLICITATION_CANNOT_BE_CANCELED_BY_CLIENT')
    }

    if (solicitation.status === 'CANCELED' || solicitation.status === 'FINALIZED') return solicitation

    solicitation.merge({
      status: 'CANCELED',
      end_date: moment().format('YYYY-MM-DD HH:mm:ss'),
      partner_sla_end_date: moment().format('YYYY-MM-DD HH:mm:ss'),
      sla_end_date: moment().format('YYYY-MM-DD HH:mm:ss')
    })

    const trx = await Database.beginTransaction()
    const clientMessage = this.messageService.buildCancelMessage('CLIENT', user.id, solicitation.id)
    const partnerMessage = this.messageService.buildCancelMessage('PARTNER', user.id, solicitation.id)

    try {
      await solicitation.save(trx)

      await this.messageService.store(clientMessage, trx)
      await this.messageService.store(partnerMessage, trx)

      await trx.commit()

      return solicitation
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }

  async accept(solicitationId, user) {
    if (!(await user.hasSolicitation(solicitationId))) throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')

    const solicitation = await SolicitationModel.findOrFail(solicitationId)

    if (solicitation.status !== 'WAITING_FOR_PARTNER_ACCEPT') throw new Error('SOLICITATION_CANNOT_BE_ACCEPT')

    solicitation.merge({
      status: 'WAITING_FOR_PARTNER_RESPONSE',
      partner_sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss')
    })

    const trx = await Database.beginTransaction()

    const clientMessage = await this.messageService.buildPartnerAcceptMessage('CLIENT', user, solicitation.id)
    const partnerMessage = await this.messageService.buildPartnerAcceptMessage('PARTNER', user, solicitation.id)

    try {
      await solicitation.save(trx)
      await this.messageService.store(clientMessage, trx)
      await this.messageService.store(partnerMessage, trx)

      await trx.commit()

      return solicitation
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }

  async refuse(solicitationId, user) {
    if (!(await user.hasSolicitation(solicitationId))) throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')

    const solicitation = await SolicitationModel.findOrFail(solicitationId)

    if (solicitation.status !== 'WAITING_FOR_PARTNER_ACCEPT') throw new Error('SOLICITATION_CANNOT_BE_REFUSED')

    solicitation.merge({
      status: 'WAITING_FOR_SELECT_NEW_PARTNER',
      partner_sla_start_date: null,
      partner_id: null
    })

    const trx = await Database.beginTransaction()

    const clientMessage = await this.messageService.buildPartnerRefusedMessage('CLIENT', user, solicitation.id)
    const partnerMessage = await this.messageService.buildPartnerRefusedMessage('PARTNER', user, solicitation.id)

    try {
      await solicitation.save(trx)

      await this.messageService.store(clientMessage, trx)
      await this.messageService.store(partnerMessage, trx)

      await trx.commit()

      return solicitation
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }

  async classify (id, attributes, user) {
    const solicitation = await SolicitationModel.findOrFail(id)
    const company = await solicitation.company().fetch()
    const isClassified = !!solicitation.solicitation_type_id
    const trx = await Database.beginTransaction()
    await this.creditService.expire(company.id)

    try {
      if (!isClassified) {
        await this.creditService.remove(company, attributes.value, id, trx)
      }
      const newAttributes = this._buildAttributes(attributes, solicitation, isClassified, user)
      const adminIsResponsible = !attributes.partner_id && !solicitation.admin_id

      solicitation.merge(newAttributes)
      await solicitation.save(trx)

      await this._createClassifyMessages(solicitation, company.unlimited_balance, trx, isClassified, adminIsResponsible, user)

      await this._sendClassifyMail(solicitation, company)

      await trx.commit()

      return solicitation
    } catch (e) {
      trx.rollback()
      if (e.message === 'INSUFFICIENT_COMPANY_BALANCE') {
        solicitation.merge({ sla_end_date: moment().format('YYYY-MM-DD HH:mm:ss') })
        await solicitation.save()
      }
      throw e
    }
  }

  async _sendClassifyMail (solicitation, company) {
    const slaValues = this.getSla(solicitation)
    const mails = new Array()
    let partner = solicitation.partner().fetch()
    let client = solicitation.client().fetch()
    let type = solicitation.solicitationType().fetch()

    await Promise.all([partner, client, type])
      .then(result => {
        partner = result[0]
        client = result[1]
        type = result[2]
      })

    if (await client.hasCompany(solicitation.company_id)) {
      const clientMessage = this.sgMail.buildClientClassifyMessage(client, partner, company, solicitation, type, slaValues.sla.total)
      mails.push(this.sgMail.send(clientMessage, company.client_up))
    }

    if (solicitation.partner_id) {
      const partnerMessage = this.sgMail.buildPartnerClassifyMessage(partner, company, solicitation, type, slaValues.sla_partner.total)

      mails.push(this.sgMail.send(partnerMessage))
    }

    await Promise.all(mails)
  }

  _buildAttributes (attributes, solicitation, isClassified, user) {
    const newPartnerId = attributes.partner_id
    const oldPartnerId = solicitation.partner_id
    let newAttributes = {
      specialty: attributes.specialty,
      demand_value: attributes.demand_value
    }

    if (!newPartnerId) {
      newAttributes.partner_id = null
      newAttributes.partner_sla_start_date = null
      newAttributes.status = 'WAITING_FOR_ADMIN_RESPONSE'

      if (!solicitation.admin_id) newAttributes.admin_id = user.id
    } else if (!oldPartnerId || oldPartnerId !== newPartnerId) {
      newAttributes.partner_sla_start_date = moment().format('YYYY-MM-DD HH:mm:ss')
      newAttributes.status = 'WAITING_FOR_PARTNER_ACCEPT'
      newAttributes.partner_id = newPartnerId
      newAttributes.admin_id = null
    }

    if (!isClassified) {
      newAttributes = {
        ...newAttributes,
        ...attributes
      }
    }

    return newAttributes
  }

  async _createClassifyMessages (solicitation, unlimitedBalance, trx, isClassified, adminIsResponsible, user) {
    if (!isClassified) {
      const solicitationMessage = await this.messageService.buildSolicitationClassifyMessage('CLIENT', solicitation, unlimitedBalance)
      await this.messageService.store(solicitationMessage, trx)
    }

    if (!!solicitation.partner_id) {
      const partner = await solicitation.partner().fetch()

      const partnerMessage = this.messageService.buildPartnerClassifyMessage('PARTNER', partner, solicitation.id)
      await this.messageService.store(partnerMessage, trx)
    }

    if (adminIsResponsible) {
      const adminIsResponsibleMessage = this.messageService.buildMessageFromAdminIsResponsible('CLIENT', user, solicitation.id)
      await this.messageService.store(adminIsResponsibleMessage, trx)
    } else {
      const clientMessage = this.messageService.buildClientClassifyMessage('CLIENT', user, solicitation.id)
      await this.messageService.store(clientMessage, trx)
    }
  }

  async finalize (solicitationId, user) {
    if (!(await user.hasSolicitation(solicitationId))) throw new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')

    const solicitation = await SolicitationModel.findOrFail(solicitationId)

    if (solicitation.status === 'FINALIZED') throw new Error('SOLICITATION_IS_ALREADY_FINALIZED')

    const now = moment().format('YYYY-MM-DD HH:mm:ss')

    solicitation.merge({
      status: 'FINALIZED',
      partner_sla_end_date: (!solicitation.partner_sla_end_date) ? now : undefined,
      sla_end_date: (!solicitation.sla_end_date) ? now : undefined,
      end_date: now
    })

    const trx = await Database.beginTransaction()

    const clientMessage = await this.messageService.buildFinalizedMessage('CLIENT', user.id, solicitation.id)
    const partnerMessage = await this.messageService.buildFinalizedMessage('PARTNER', user.id, solicitation.id)

    try {
      await solicitation.save(trx)

      await this.messageService.store(clientMessage, trx)
      await this.messageService.store(partnerMessage, trx)

      await trx.commit()

      return solicitation
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }

  async updateMany (solicitations, attributes, trx = null) {
    const ids = new Array()

    solicitations.forEach(s => {
      ids.push(s.id)
    })

    await SolicitationModel
      .query()
      .whereIn('id', ids)
      .transacting(trx)
      .update(attributes)
  }

  async autoFinalize () {
    const period = moment().subtract(15, 'days').format('YYYY-MM-DD HH:mm:ss')
    const trx = await Database.beginTransaction()
    let promises = new Array()

    const solicitations =
      await SolicitationModel
        .query()
        .where('status', 'WAITING_FOR_CLIENT_RESPONSE')
        .where('sla_start_date', '<', period)

    try {
      solicitations.forEach(s => {
        promises = promises.concat(this._finalizeWithPromises(s, s.client_id, trx))
      })

      await Promise.all(promises)
      await this._sendFinalizeMail(solicitations)
      await trx.commit()
    } catch(e) {
      await trx.rollback()
      throw e
    }
  }

  _finalizeWithPromises (solicitation, userId, trx = null) {
    const now = moment().format('YYYY-MM-DD HH:mm:ss')
    const clientMessage = this.messageService.buildFinalizedMessage('CLIENT', userId, solicitation.id)
    const partnerMessage = this.messageService.buildFinalizedMessage('PARTNER', userId, solicitation.id)
    const update =
      SolicitationModel
        .query()
        .where('id', solicitation.id)
        .transacting(trx)
        .update({
          status: 'FINALIZED',
          partner_sla_end_date: (!solicitation.partner_sla_end_date) ? now : undefined,
          sla_end_date: (!solicitation.sla_end_date) ? now : undefined,
          end_date: now
        })

    return [
      update,
      this.messageService.store(clientMessage, trx),
      this.messageService.store(partnerMessage, trx)
    ]
  }

  _sendFinalizeMail (solicitations) {
    if (!solicitations.length) return

    const message = solicitations.reduce(( prevVal, elem ) => {
      const link = `${Env.get('FRONTEND_LUVA_URL')}/solicitacao/${elem.id}`
      const newLine = `<a href="${link}">#${elem.id} - ${elem.name}</a><br/>`
      return prevVal + newLine
    }, ``)

    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `Luva Assessoria - Solicitações Finalizadas`,
      templateId: Env.get('SOLICITATION_FINALIZED_TEMPLATE_ID'),
      substitutions: {
        solicitations: message
      }
    }

    this.sgMail.send(msg)
  }

  async delayed () {
    const yesterday = moment().subtract(24, 'hours').format('YYYY-MM-DD HH:mm:ss')

    let solicitations =
      await SolicitationModel
        .query()
        .with('company')
        .with('client')
        .whereNotIn('status', ['FINALIZED', 'CANCELED'])
        .fetch()

    solicitations = solicitations.toJSON()
    const solicitationsDelayeds = solicitations.filter(s => {
      const sla = this.getSla(s)
      const noticeTime = moment(s.notice_time)

      return (sla.sla.current >= sla.sla.total) &&
      (s.notice_time === null || noticeTime.isBefore(yesterday)) && (!!s.response_time)
    })

    await this.delayedSendMails(solicitationsDelayeds)

    await this.updateMany(solicitationsDelayeds, {
      notice_time: moment().format('YYYY-MM-DD HH:mm:ss')
    })
  }

  delayedSendMails (solicitations) {
    const promises = new Array()

    solicitations.forEach(s => {
      const clientUp = s.company.client_up
      const subjectPrefix = clientUp ? 'Up Legal' : 'Luva Assessoria'
      const prefixUrl = Env.get(clientUp ? 'FRONTEND_UP_URL' : 'FRONTEND_LUVA_URL')
      const msg = {
        from: Env.get('DEFAULT_CONTACT_MAIL'),
        subject: `${subjectPrefix} - Solicitação Atrasada`,
        templateId: Env.get('SOLICITATION_DELAYED_TEMPLATE_ID'),
        substitutions: {
          id: s.id,
          name: s.name,
          link: `${prefixUrl}/solicitacao/${s.id}`
        }
      }

      const adminMsg = {
        ...msg,
        to: Env.get('DEFAULT_ADMIN_MAIL')
      }

      const clientMsg = {
        ...msg,
        to: s.client.email
      }

      promises.push(this.sgMail.send(clientMsg))
      promises.push(this.sgMail.send(adminMsg))
    })

    return Promise.all(promises)
  }

  async update (attributes, id) {
    const solicitation = await SolicitationModel.findOrFail(id)
    solicitation.merge(attributes)
    await solicitation.save()
  }

}

module.exports = SolicitationService
