'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Partner - GET /partner')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let partnerActive = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partnerActive.id, status: 'ACTIVE', specialty: 'direito' })
  let partnerRequested = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partnerRequested.id, status: 'REQUESTED' })
})

test('should list all partners and return status 200', async({ client, assert}) => {
  const response = await client.get('partner').query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 2)
})

test('should list all partners with specialty and return status 200', async({ client, assert}) => {
  const response = await client.get('partner').query({company_id: 1, specialty: 'direito'}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should list all partners active and return status 200', async({ client, assert}) => {
  const response = await client.get('partner').query({company_id: 1, status: 'ACTIVE'}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should list all partners requested and return status 200', async({ client, assert}) => {
  const response = await client.get('partner').query({company_id: 1, status: 'REQUESTED'}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should not list partners for not logged user', async ({ client, assert }) => {
  const response = await client.get('partner').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('partner').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get('partner').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
