'use strict'

const Model = use('Model')

class Transaction extends Model {
  company() {
    return this.belongsTo('App/Models/Company')
  }

  solicitation() {
    return this.belongsTo('App/Models/Solicitation')
  }
}

module.exports = Transaction
