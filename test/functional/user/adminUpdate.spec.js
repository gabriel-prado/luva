'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Admin - PUT /admin/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN', active: true })
})

test('should update admin and return status 200', async({ client, assert}) => {
  const response = await client.put('admin/1').send({ name: 'new_name', active: false }).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, 'new_name')
  assert.equal(response.body.active, false)
})

test('should not update admin for not logged user', async ({ client, assert }) => {
  const response = await client.put('admin/1').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.put('admin/1').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.put('admin/1').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
