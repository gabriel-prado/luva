'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - PUT /company')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE', client_up: false })
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should update company for client user and return status 200', async({ client, assert}) => {
  let company = defaultCompany.toJSON()
  company.name = 'new_name'
  company.complementary_data.billing = 123.23
  company.complementary_data.billing_summary = 'test_test'
  company.status = (company.status !== 'ACTIVE') ? 'ACTIVE' : 'INACTIVE'
  company.notes = 'new_notes'
  company.client_up = true

  const response = await client.put(`company/${defaultCompany.id}`).send(company).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, company.name)
  assert.equal(response.body.client_up, false)
  assert.equal(response.body.complementary_data.billing, company.complementary_data.billing)
  assert.equal(response.body.complementary_data.billing_summary, company.complementary_data.billing_summary)
  assert.notEqual(response.body.notes, company.notes)
  assert.notEqual(response.body.status, company.status)
})

test('should update company for admin user and return status 200', async({ client, assert}) => {
  let company = defaultCompany.toJSON()
  company.name = 'new_name'
  company.status = (company.status !== 'ACTIVE') ? 'ACTIVE' : 'INACTIVE'
  company.notes = 'new_notes'
  company.client_up = true
  company.unlimited_balance = true
  company.plan_value = 10

  const response = await client.put(`company/${defaultCompany.id}`).send(company).query({company_id: 1}).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.client_up, true)
  assert.equal(response.body.name, company.name)
  assert.equal(response.body.status, company.status)
  assert.equal(response.body.notes, company.notes)
  assert.equal(response.body.unlimited_balance, company.unlimited_balance)
  assert.equal(response.body.plan_value, company.plan_value)
})

test('should update company for another client user and return status 401', async({ client, assert}) => {
  let company = await Factory.model('App/Models/Company').create({ status: 'ACTIVE' })
  company = company.toJSON()
  company.name = 'new_name'
  const response = await client.put(`company/${company.id}`).send(company).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 401)
})

test('should not list companies for not logged user', async ({ client, assert }) => {
  const response = await client.put('company/1').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.put('company/1').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
