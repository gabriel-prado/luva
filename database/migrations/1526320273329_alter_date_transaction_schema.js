'use strict'

const Schema = use('Schema')

class AlterDateTransactionSchema extends Schema {
  up () {
    this.alter('transactions', (table) => {
      table.datetime('date').alter()
    })
  }

  down () {
    this.alter('transactions', (table) => {
      table.date('date').alter()
    })
  }
}

module.exports = AlterDateTransactionSchema
