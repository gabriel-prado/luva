'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User by Company - POST /company/id/user')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should create and attach user to company by client user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').make({ type: 'CLIENT' })
  user = user.toJSON()
  user.permissions = ["SOLICITATION"]
  user.password = 'password123'

  const response =
    await client
      .post(`company/${defaultCompany.id}/user`)
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.status, "CREATED")
  const userInDatabase = await UserModel.findBy('email', user.email)
  assert.equal(userInDatabase.name, user.name)
})

test('should attach user to company by client user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  user = user.toJSON()
  user.permissions = ["SOLICITATION"]
  user.password = 'password123'

  const response =
    await client
      .post(`company/${defaultCompany.id}/user`)
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.status, "ATTACHED")
  const userInDatabase = await UserModel.find(user.id)
  const company = await userInDatabase.companies()
  assert.equal(company[0].name, defaultCompany.name)
})

test('should create and attach user to company by admin user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').make({ type: 'CLIENT' })
  user = user.toJSON()
  user.permissions = ["SOLICITATION"]
  user.password = 'password123'

  const response =
    await client
      .post(`company/${defaultCompany.id}/user`)
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.status, "CREATED")
  const userInDatabase = await UserModel.findBy('email', user.email)
  assert.equal(userInDatabase.name, user.name)
})

test('should attach user to company by admin user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  user = user.toJSON()
  user.permissions = ["SOLICITATION"]
  user.password = 'password123'

  const response =
    await client
      .post(`company/${defaultCompany.id}/user`)
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.status, "ATTACHED")
  const userInDatabase = await UserModel.find(user.id)
  const company = await userInDatabase.companies()
  assert.equal(company[0].name, defaultCompany.name)
})

test('should attach admin to company by admin user and return status 500', async({ client, assert }) => {
  const user = defaultAdmin.toJSON()
  user.permissions = ["SOLICITATION"]
  user.password = 'password123'

  const response =
    await client
      .post(`company/${defaultCompany.id}/user`)
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.message, "USER_IS_NOT_A_CLIENT")
})

test('should not attach user for not logged user', async ({ client, assert }) => {
  const response = await client.post(`company/${defaultCompany.id}/user`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.post(`company/${defaultCompany.id}/user`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
