'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const PartnerModel = use('App/Models/Partner')
const { test, trait, beforeEach } = use('Test/Suite')('Admin - POST /admin')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
})

test('should create admin user and return status 200', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: 'ADMIN' })
  user = user.toJSON()
  user.password = '123456'

  const response =
    await client
      .post('admin')
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  const userInDatabase = await UserModel.findBy('email', user.email)
  assert.isNotNull(userInDatabase)
})

test('should try create admin user with same email and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  user = user.toJSON()
  user.email = defaultAdmin.email

  const response =
    await client
      .post('admin')
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'email')
})

test('should try create admin user without name and return status 400', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').make({ type: null })
  user = user.toJSON()
  delete user.email

  const response =
    await client
      .post('admin')
      .send(user)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'email')
})

test('should not create admins for not logged user', async ({ client, assert }) => {
  const response = await client.post('admin').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.post('admin').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.post('admin').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
