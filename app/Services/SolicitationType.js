'use strict'

const SolicitationTypeModel = use('App/Models/SolicitationType')

class SolicitationTypeService {

  list (page = null, perPage = null) {
    return SolicitationTypeModel.query().paginate(page, perPage)
  }
  
}

module.exports = SolicitationTypeService
