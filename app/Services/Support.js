'use strict'

const Env = use('Env')

class SupportService {

  constructor (SendGridService) {
    this.sgMail = SendGridService
  }

  async sendMail(name, phone, clientUp) {
    const subjectPrefix = clientUp ? 'Up Legal' : 'Luva Assessoria'

    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `${subjectPrefix} - solicitação de suporte`,
      templateId: Env.get('SUPPORT_TEMPLATE_ID'),
      substitutions: {
        name,
        phone
      }
    }

    await this.sgMail.send(msg, clientUp)
  }

}

module.exports = SupportService
