'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const moment = use('moment')
const TransactionModel = use('App/Models/Transaction')
const { test, trait, beforeEach } = use('Test/Suite')('Credit - expire credit')

let defaultAdmin = null
let defaultCompany = null
let defaultClient = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').create({ plan_value: 600 })
  await defaultClient.companies().attach([defaultCompany.id], (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  await Factory.model('App/Models/Transaction').create({
    value: 300,
    operation: 'ADD',
    company_id: defaultCompany.id,
    expire: true,
    balance_expiring: 300,
    balance_not_expiring: 0,
    date: moment().subtract(40, 'days').format('YYYY-MM-DD HH:mm:ss')
  })
})

test('should classify solicitation and expire credit', async({ client, assert}) => {
  const solicitationType = await Factory.model('App/Models/SolicitationType').create()
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    client_id: defaultClient.id,
    company_id: defaultCompany.id,
    partner_sla_start_date: null,
    sla_start_date: null
  })

  const response =
    await
      client
        .post(`solicitation/${solicitation.id}/classify`)
        .send({
          solicitation_type_id: solicitationType.id,
          specialty: 'counts',value: 45,
          response_time: 12
        })
        .loginVia(defaultAdmin)
        .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.message, 'INSUFFICIENT_COMPANY_BALANCE')

  await _verifyCredit(assert)
})

test('should list all transactions and expire credit', async({ client, assert}) => {
  await client
    .get(`company/${defaultCompany.id}/transaction`)
    .query({all: true})
    .loginVia(defaultAdmin)
    .end()

  await _verifyCredit(assert)
})

test('should return balance and expire credit', async ({ client, assert}) => {
  await client
    .get(`company/${defaultCompany.id}/credit`)
    .loginVia(defaultAdmin)
    .end()

  await _verifyCredit(assert)
})

const _verifyCredit = async (assert) => {
  const transactionInDatabase =
    await
      TransactionModel
        .query()
        .where('company_id', defaultCompany.id)
        .orderBy('id', 'desc')
        .first()

  assert.equal(transactionInDatabase.operation, 'REMOVE')
  assert.equal(transactionInDatabase.value, 300)
  assert.equal(transactionInDatabase.balance_expiring, 0)
}
