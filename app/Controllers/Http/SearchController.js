'use strict'

class SearchController {

  static get inject () {
    return ['App/Services/Search']
  }

  constructor (SearchService) {
    this.searchService = SearchService
  }

  search({ request, auth }) {
    const { query } = request.get()
    return this.searchService.search(query, auth.user)
  }

}

module.exports = SearchController
