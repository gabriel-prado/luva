'use strict'

const Route = use('Route')

Route.group('Credit', () => {
  Route.post('/request', 'CreditController.request').middleware(['auth', 'acl:CLIENT', 'access:CREDIT']).validator('RequestCredit')
  Route.post('/add', 'CreditController.add').middleware(['auth', 'acl:ADMIN', 'access:CREDIT']).validator('AddCredit')
  Route.get('', 'CreditController.balance').middleware(['auth', 'acl:ADMIN,CLIENT', 'access:CREDIT'])
})
.prefix('company/:company_id/credit')
