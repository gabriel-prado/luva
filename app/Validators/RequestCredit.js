'use strict'

class RequestCredit {
  get rules () {
    return {
      value: 'required|number'
    }
  }

  get data () {
    const body = this.ctx.request.only(['value'])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = RequestCredit
