'use strict'

const Schema = use('Schema')

class OabAdminSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('prefix_oab', 6)
      table.string('oab_number', 30)
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('prefix_oab')
      table.dropColumn('oab_number')
    })
  }
}

module.exports = OabAdminSchema
