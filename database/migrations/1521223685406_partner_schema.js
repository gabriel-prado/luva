'use strict'

const Schema = use('Schema')

class PartnerSchema extends Schema {
  up () {
    this.create('partners', (table) => {
      table.integer('id').unsigned().notNullable().unique()
      table.foreign('id').references('users.id')
      table.enum('entity_type', ['PF','PJ']).notNullable()
      table.string('document_number', 20).notNullable()
      table.string('oab_number', 30).notNullable()
      table.enum('status', ['ACTIVE', 'INACTIVE', 'REQUESTED']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('partners')
  }
}

module.exports = PartnerSchema
