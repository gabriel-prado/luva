'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const CompanyModel = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - POST /company')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultClient = null

beforeEach(async () => {
  await clearDataBase()
  let company = await Factory.model('App/Models/Company').create()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })

  await defaultClient.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('should create company user and return 200 - OK', async({ client, assert}) => {
  let company = await Factory.model('App/Models/Company').make({ notes: null, client_up: true })
  company = company.toJSON()

  const response =
    await client.post('company').send(company).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 200)

  const companyInDatabase = await CompanyModel.find(response.body.id)
  assert.notEqual(companyInDatabase.client_up, true)
  assert.isNotNull(companyInDatabase)

  const user = (await companyInDatabase.users().fetch()).toJSON()[0]
  assert.sameDeepMembers(user.pivot.permissions, ['COMPANY', 'SOLICITATION', 'USER', 'CREDIT'])
})

test('should try create company without cnpj and return 400 - Bad request', async({ client, assert}) => {
  let company = (await Factory.model('App/Models/Company').make()).toJSON()
  delete company.cnpj

  const response =
    await client.post('company').send(company).query({company_id: 1}).loginVia(defaultClient).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'cnpj')
})

test('should try create company as ADMIN and get 401 - Unauthorized', async({ client, assert}) => {

  let adminUser = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let company = await Factory.model('App/Models/Company').make()

  const response =
    await client.post('company').send(company.toJSON()).query({company_id: 1}).loginVia(adminUser).end()

  assert.equal(response.status, 401)
})

test('should try create company as PARTNER and get 401 - Unauthorized', async({ client, assert}) => {

  let partnerUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  let company = await Factory.model('App/Models/Company').make()

  const response =
    await client.post('company').send(company.toJSON()).query({company_id: 1}).loginVia(partnerUser).end()

  assert.equal(response.status, 401)
})
