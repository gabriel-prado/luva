'use strict'

const Route = use('Route')

Route.group('Recover Password', () => {
  Route.post('/generate-code', 'RecoverPasswordController.generateCode').validator('GenerateCode')
  Route.put('/change-password', 'RecoverPasswordController.changePassword').validator('ChangePassword')
}).prefix('user')
