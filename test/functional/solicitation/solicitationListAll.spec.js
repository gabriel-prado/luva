'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const moment = use('moment')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation - GET /solicitation')

let clientUser
let userCompany

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should list all solicitations for CLIENT user and return status 200', async({ client, assert}) => {
  const clientUser2 = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser2.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  let response = await client.get('solicitation').query({company_id: userCompany.id, all: true}).loginVia(clientUser).end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
  assert.notExists(response.body.data[0].note)
  assert.notExists(response.body.data[0].favorite)
  assert.notExists(response.body.data[0].tags)

  response = await client.get('solicitation').query({company_id: userCompany2.id, all: true}).loginVia(clientUser2).end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 0)
})

test('should list all solicitations for ADMIN user and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const clientUser2 = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser2.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser2.id,
    company_id: userCompany2.id
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(userAdmin).end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 6)
})

test('should list all solicitations with order and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const clientUser2 = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser2.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_ADMIN_RESPONSE',
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: null,
    partner_sla_start_date: null,
    partner_sla_end_date: null,
    response_time: 30,
    client_id: clientUser2.id,
    company_id: userCompany2.id
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_ADMIN_RESPONSE',
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: null,
    partner_sla_start_date: null,
    partner_sla_end_date: null,
    response_time: 10,
    client_id: clientUser2.id,
    company_id: userCompany2.id
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    sla_start_date: null,
    sla_end_date: null,
    partner_sla_start_date: null,
    partner_sla_end_date: null,
    response_time: null,
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(userAdmin).end()
  assert.equal(response.status, 200)
  assert.equal(response.body.data[0].id, 3)
  assert.equal(response.body.data[1].id, 2)
  assert.equal(response.body.data[2].id, 1)
})

test('should list all solicitations for PARTNER user and return status 200', async({ client, assert}) => {
  const clientUser2 = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser2.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const userPartner1 = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: userPartner1.id, status: 'ACTIVE' })

  const userPartner2 = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: userPartner2.id, status: 'ACTIVE' })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    partner_id: userPartner1.id
  })

  await Factory.model('App/Models/Solicitation').createMany(1, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser2.id,
    company_id: userCompany2.id,
    partner_id: userPartner2.id
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(userPartner1).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

test('should list all solicitations with unread_messages for CLIENT user and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    client_viewed_in: null
  })

  const solicitationCanceled = await Factory.model('App/Models/Solicitation').create({
    status: 'CANCELED',
    client_id: clientUser.id,
    company_id: userCompany.id,
    client_viewed_in: null
  })

  const solicitationFinalized = await Factory.model('App/Models/Solicitation').create({
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: userCompany.id,
    client_viewed_in: null
  })

  await Factory.model('App/Models/Message').create({
    solicitation_id: solicitationCanceled.id,
    user_id: userAdmin.id,
    chat_type: 'CLIENT'
  })

  await Factory.model('App/Models/Message').create({
    solicitation_id: solicitationFinalized.id,
    user_id: userAdmin.id,
    chat_type: 'CLIENT'
  })

  await Factory.model('App/Models/Message').createMany(3, {
    solicitation_id: solicitation.id,
    user_id: userAdmin.id,
    chat_type: 'CLIENT'
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(clientUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
  assert.equal(response.body.data[0].__meta__.unread_messages, 3)
  assert.equal(response.body.data[1].__meta__.unread_messages, 0)
  assert.equal(response.body.data[2].__meta__.unread_messages, 0)
})

test('should list all solicitations with unread_messages for PARTNER user and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const userPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: userPartner.id, status: 'ACTIVE' })

  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    partner_id: userPartner.id,
    partner_viewed_in: null
  })

  await Factory.model('App/Models/Message').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    solicitation_id: solicitation.id,
    user_id: userAdmin.id,
    chat_type: 'PARTNER'
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(userPartner).end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].__meta__.unread_messages, 2)
})

test('should list all solicitations with unread_messages for ADMIN user and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const userPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: userPartner.id, status: 'ACTIVE' })

  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    partner_id: userPartner.id,
    admin_viewed_in: null
  })

  await Factory.model('App/Models/Message').createMany(2, {
    solicitation_id: solicitation.id,
    user_id: clientUser.id,
    chat_type: 'CLIENT'
  })

  await Factory.model('App/Models/Message').createMany(2, {
    solicitation_id: solicitation.id,
    user_id: userPartner.id,
    chat_type: 'PARTNER'
  })

  const response = await client.get('solicitation').query({company_id: 1, all: true}).loginVia(userAdmin).end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].__meta__.unread_messages, 4)
})

test('should list solicitations filter by one company_id for CLIENT user and return status 200', async({ client, assert}) => {
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  await Factory.model('App/Models/Solicitation').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany2.id
  })

  const response = await client.get('solicitation')
    .query({ companies_id: [userCompany.id], all: true, company_id: 1 })
    .loginVia(clientUser)
    .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

test('should list solicitations filter by two company_id for CLIENT user and return status 200', async({ client, assert}) => {
  const userCompany2 = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(userCompany2, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  await Factory.model('App/Models/Solicitation').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany2.id
  })

  const response = await client.get('solicitation')
    .query({ companies_id: [userCompany.id, userCompany2.id], all: true, company_id: 1 })
    .loginVia(clientUser)
    .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should list solicitations filter by types_id for CLIENT user and return status 200', async({ client, assert}) => {
  const solicitationsType = await Factory.model('App/Models/SolicitationType').create()

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    solicitation_type_id: solicitationsType.id
  })

  await Factory.model('App/Models/Solicitation').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  const response = await client.get('solicitation')
    .query({ types_id: [solicitationsType.id], all: true, company_id: 1 })
    .loginVia(clientUser)
    .end()
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

test('Should list the requests with the cancelled and finalized last', async({ client, assert}) => {
  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  const solicitationCanceled = await Factory.model('App/Models/Solicitation').create({
    status: 'CANCELED',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  const solicitationFinalized = await Factory.model('App/Models/Solicitation').create({
    status: 'FINALIZED',
    client_id: clientUser.id,
    company_id: userCompany.id
  })

  const response = await client.get('solicitation')
    .query({ all: true, company_id: 1 })
    .loginVia(clientUser)
    .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
  assert.equal(response.body.data[3].id, solicitationFinalized.id)
  assert.equal(response.body.data[4].id, solicitationCanceled.id)
})

test('should list all solicitations with favorite and return status 200', async({ client, assert}) => {
  const userAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    favorite: true
  })

  await Factory.model('App/Models/Solicitation').createMany(2, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: userCompany.id,
    favorite: false
  })

  const response = await client.get('solicitation')
    .query({ all: true, company_id: 1, favorite: true })
    .loginVia(userAdmin)
    .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

test('should not list companies for not logged user', async ({ client, assert }) => {
  const response = await client.get('company').end()

  assert.equal(response.status, 401)
})
