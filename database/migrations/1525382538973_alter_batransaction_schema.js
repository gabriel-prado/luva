'use strict'

const Schema = use('Schema')

class AlterTransactionSchema extends Schema {
  up () {
    this.alter('transactions', (table) => {
      table.float('balance_expiring').defaultTo(0).notNullable().alter()
      table.float('balance_not_expiring').defaultTo(0).notNullable().alter()
    })
  }

  down () {
    this.alter('transactions', (table) => {
      table.float('balance_expiring').notNullable().alter()
      table.float('balance_not_expiring').notNullable().alter()
    })
  }
}

module.exports = AlterTransactionSchema
