'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Solicitation File - GET /solicitation/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultSolicitation = null
let defaultAdmin = null
let defaultClient = null
let defaultPartner = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').create({
    status: 'ACTIVE'
  })
  await defaultClient.companies().attach([ company.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  const solicitationType = await Factory.model('App/Models/SolicitationType').create()
  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    company_id: company.id,
    partner_id: defaultPartner.id,
    solicitation_type_id: solicitationType.id,
    client_files: [ 'image_client.jpg' ],
    partner_files: [ 'image_partner.jpg' ]
  })
})

test('client should get file and return status 200', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'CLIENT', file: 'image_client.jpg', company_id: 1 })
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('partner should get file and return status 200', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'PARTNER', file: 'image_partner.jpg', company_id: 1 })
      .loginVia(defaultPartner)
      .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('admin should get file and return status 200', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'CLIENT', file: 'image_client.jpg', company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('should return 404 for solicitation that does not exist.', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/33/file`)
      .query({ chat_type: 'CLIENT', file: 'image_client.jpg', company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 404)
})

test('should return 404 for file that does not exist.', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'CLIENT', file: 'file.jpg', company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 404)
  assert.equal(response.body.message, 'THIS_FILE_DOES_NOT_EXISTS')
})

test('should return 404 for request without file name', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'CLIENT', company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 404)
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'CLIENT', file: 'image_client.jpg', company_id: 1 })
      .end()

  assert.equal(response.status, 401)
})

test('should return 401 when the solicitation does not belong to the user', async({ client, assert }) => {
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })

  const response =
    await
      client
      .get(`solicitation/${defaultSolicitation.id}/file`)
      .query({ chat_type: 'PARTNER', file: 'image_partner.jpg', company_id: 1 })
      .loginVia(partner)
      .end()

  assert.equal(response.status, 401)
  assert.equal(response.body.message, 'SOLICITATION_DOES_NOT_BELONG_TO_USER')
})
