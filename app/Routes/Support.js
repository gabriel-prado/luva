'use strict'

const Route = use('Route')

Route
  .post('support', 'SupportController.support')
  .middleware(['auth', 'acl:CLIENT,PARTNER'])
  .validator('Support')
