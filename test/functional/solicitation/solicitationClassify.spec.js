'use strict'

const SolicitationModel = use('App/Models/Solicitation')
const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Classify Solicitation - POST /solicitation/:id/classify')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultPartner = null
let defaultAdmin = null
let defaultSolicitation = null
let defaultSolicitationType = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })
  const client = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make({ plan_value: 600 })
  await client.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  defaultSolicitationType = await Factory.model('App/Models/SolicitationType').create()

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    client_id: client.id,
    company_id: company.id,
    partner_sla_start_date: null,
    sla_start_date: null
  })
})

test('should classify solicitation and return 200', async({ client, assert}) => {
  await _addCredit(200)

  const response =
    await
      client
        .post(`solicitation/${defaultSolicitation.id}/classify`)
        .send({
          partner_id: defaultPartner.id,
          solicitation_type_id: defaultSolicitationType.id,
          specialty: 'counts',value: 45,
          response_time: 12
        })
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase.partner_sla_start_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_PARTNER_ACCEPT')
  assert.equal(solicitationInDatabase.specialty, 'counts')
})

test('should classify solicitation without partner_id and return 200', async({ client, assert}) => {
  await _addCredit(200)

  const response =
    await
      client
        .post(`solicitation/${defaultSolicitation.id}/classify`)
        .send({
          solicitation_type_id: defaultSolicitationType.id,
          specialty: 'counts',
          value: 45,
          response_time: 12
        })
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase.sla_start_date)
  assert.isNull(solicitationInDatabase.partner_sla_start_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_ADMIN_RESPONSE')
  assert.equal(solicitationInDatabase.specialty, 'counts')
  assert.equal(solicitationInDatabase.admin_id, defaultAdmin.id)
})

test('should classify solicitation without credit and return 500', async({ client, assert}) => {
  const response =
    await
      client
        .post(`solicitation/${defaultSolicitation.id}/classify`)
        .send({
          partner_id: defaultPartner.id,
          solicitation_type_id: defaultSolicitationType.id,
          specialty: 'counts',
          value: 45,
          response_time: 12
        })
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  const solicitationInDatabase = await SolicitationModel.find(defaultSolicitation.id)

  assert.equal(response.status, 500)
  assert.equal(response.body.message, 'INSUFFICIENT_COMPANY_BALANCE')
})

test('should try classify solicitation to PARTNER user and return 401', async({ client, assert}) => {
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })

  const response = await client.post(`solicitation/${defaultSolicitation.id}/classify`).query({company_id: 1}).loginVia(partner).end()
  assert.equal(response.status, 401)
})

test('should try classify solicitation to CLIENT user and return 401', async({ client, assert}) => {
  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make({ plan_value: 600 })
  await clientUser.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response = await client.post(`solicitation/${defaultSolicitation.id}/classify`).query({company_id: 1}).loginVia(clientUser).end()
  assert.equal(response.status, 401)
})


test('should classify solicitation whithout logged User and return 401', async({ client, assert}) => {
  const response = await client.post(`solicitation/${defaultSolicitation.id}/classify`).end()

  assert.equal(response.status, 401)
})

const _addCredit = async (value, expire = false) => {
  const attributes = {
    value,
    operation: 'ADD',
    company_id: 1
  }

  if (expire) {
    attributes.balance_expiring = value
    attributes.balance_not_expiring = 0
    attributes.expire = true
  } else {
    attributes.balance_expiring = 0
    attributes.balance_not_expiring = value
  }

  await Factory.model('App/Models/Transaction').create(attributes)
}
