const Database = use('Database')
const Env = use('Env')
const Promise = use('bluebird')

module.exports = async () => {
  const schemaInfoQuery = 'SELECT table_name FROM information_schema.tables where table_schema=?;'
  const [ schemaInfo ] = await Database.raw(schemaInfoQuery, [Env.get('DB_DATABASE')])
  let tableNames = schemaInfo.filter(r => r.table_name !== 'adonis_schema')

  await Database.transaction(async (trx) => {
    await trx.raw('SET FOREIGN_KEY_CHECKS=0;')

    for(let i = 0; i < tableNames.length; i++) {
      await trx.truncate(tableNames[i].table_name)
    }

    await trx.raw('SET FOREIGN_KEY_CHECKS=1;')
  })
}
