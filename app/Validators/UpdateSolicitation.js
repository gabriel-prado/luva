'use strict'

class UpdateSolicitation {
  get rules () {
    return {
      note: 'string',
      tags: 'array'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'note',
      'tags'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateSolicitation
