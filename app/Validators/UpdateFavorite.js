'use strict'

class UpdateFavorite {
  get rules () {
    return {
      favorite: 'required|boolean'
    }
  }

  get data () {
    this.ctx.request.body = this.ctx.request.only(['favorite'])
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateFavorite
