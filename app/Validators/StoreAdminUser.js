'use strict'

class StoreAdminUser {
  get rules () {
    return {
      name: 'required|string',
      email: 'required|email|unique:users,email',
      password: 'required|string',
      photo_url: 'string',
      oab_number: 'string',
      prefix_oab: 'string',
      permissions: 'array'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'password',
      'photo_url',
      'oab_number',
      'prefix_oab',
      'permissions'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreAdminUser
