'use strict'

const Schema = use('Schema')

class AddNotesToPartnerSchema extends Schema {
  up () {
    this.table('partners', (table) => {
      table.text('notes')
    })
  }

  down () {
    this.table('partners', (table) => {
      table.dropColumn('notes')
    })
  }
}

module.exports = AddNotesToPartnerSchema
