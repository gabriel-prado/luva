'use strict'

class UpdateUser {
  get rules () {
    return {
      name: 'string',
      password: 'string',
      email: 'email|unique:users,email',
      photo_url: 'string',
      prefix_oab: 'string',
      oab_number: 'string',
      active: 'boolean',
      permissions: 'array'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'password',
      'email',
      'photo_url',
      'prefix_oab',
      'oab_number',
      'active',
      'permissions'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateUser
