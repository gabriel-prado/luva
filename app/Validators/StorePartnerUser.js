'use strict'

class StorePartnerUser {
  get rules () {
    return {
      name: 'required|string',
      email: 'required|email|unique:users,email',
      password: 'required',
      type: 'required|in:PARTNER',
      photo_url: 'string',
      'partner.entity_type': 'required|in:PF,PJ',
      'partner.document_number': 'required|string',
      'partner.oab_number': 'required|string',
      'partner.prefix_oab': 'required|string',
      'partner.status': 'required|in:ACTIVE,INACTIVE,REQUESTED'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'password',
      'photo_url'
    ])
		body.type = 'PARTNER'

    body.partner = {
			status: 'REQUESTED',
			document_number: this.ctx.request.body.partner.document_number,
			entity_type: this.ctx.request.body.partner.entity_type,
			oab_number: this.ctx.request.body.partner.oab_number,
			prefix_oab: this.ctx.request.body.partner.prefix_oab
		}

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StorePartnerUser
