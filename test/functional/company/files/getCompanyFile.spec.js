'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company File - GET /company/id/file')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultClient = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })

  await defaultClient.companies().attach([ defaultCompany.id ], (row) => {
    row.permissions = ["REPORT",'SOLICITATION','CREDIT','COMPANY','USER']
  })
})

test('client should get file and return status 200', async({ client, assert }) => {
  let response = await _getFile(client, defaultClient)

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('admin should get file and return status 200', async({ client, assert }) => {
  let admin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  let response = await _getFile(client, admin)

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')
})

test('should return 401 when the company does not belong to the user', async({ client, assert }) => {
  const response = await client.get(`company/34/file`).query({company_id: 1, file: 'teste.txt'}).loginVia(defaultClient).end()
  assert.equal(response.status, 401)
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}/file`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get(`company/${defaultCompany.id}/file`).query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

const _getFile = async (client, user) => {
  return await
      client
        .get(`company/${defaultCompany.id}/file`)
        .query({ file: 'teste.txt', company_id: defaultCompany.id })
        .loginVia(user)
        .end()
}
