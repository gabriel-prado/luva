'use strict'

const Route = use('Route')

Route
  .resource('company', 'CompanyController')
  .only(['index', 'show', 'store', 'update'])
  .middleware(new Map([
    [['company.index'], ['auth', 'acl:ADMIN,CLIENT', 'access:COMPANY,SOLICITATION,REPORT,CREDIT,USER']],
    [['company.store'], ['auth', 'acl:CLIENT', 'access:COMPANY']],
    [['company.update'], ['auth', 'acl:ADMIN,CLIENT', 'access:COMPANY']],
    [['company.show'], ['auth', 'acl:ADMIN,CLIENT', 'access:COMPANY,SOLICITATION']]
  ]))
  .validator(new Map([
    [['company.store'], ['StoreCompany']],
    [['company.update'], ['UpdateCompany']]
  ]))

Route.group('Company File', () => {
  Route.post('', 'CompanyController.uploadFile')
  Route.get('', 'CompanyController.getFile')
}).prefix('company/:id/file').middleware(['auth', 'acl:ADMIN,CLIENT', 'access:COMPANY'])

Route.group('User by Company', () => {
  Route.get('', 'CompanyController.listUsersByCompany')
  Route.get('/:user_id', 'CompanyController.showUserByCompany')
  Route.put('/:user_id', 'CompanyController.updateUserByCompany').validator('UpdateUser')
  Route.post('', 'CompanyController.attachUserToCompany').validator('AttachClient')
  Route.delete('/:user_id', 'CompanyController.detachUserToCompany')
}).prefix('company/:company_id/user').middleware(['auth', 'acl:ADMIN,CLIENT', 'access:COMPANY,USER'])
