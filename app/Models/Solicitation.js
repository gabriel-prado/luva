'use strict'

const Model = use('Model')
const moment = use('moment')

class Solicitation extends Model {

  static boot () {
    super.boot()
    this.addHook('beforeCreate', 'Solicitation.setSlaStart')
  }

  solicitationType() {
    return this.belongsTo('App/Models/SolicitationType')
  }

  partner() {
    return this.belongsTo('App/Models/User', 'partner_id', 'id')
  }

  client() {
    return this.belongsTo('App/Models/User', 'client_id', 'id')
  }

  admin() {
    return this.belongsTo('App/Models/User', 'admin_id', 'id')
  }

  company() {
    return this.belongsTo('App/Models/Company', 'company_id', 'id')
  }

  messages() {
    return this.hasMany('App/Models/Message')
  }

  getPartnerFiles(partner_files) {
    return (!!partner_files) ? JSON.parse(partner_files) : []
  }

  setPartnerFiles(partner_files) {
    return (!!partner_files) ? JSON.stringify(partner_files) : '[]'
  }

  getClientFiles(client_files) {
    return (!!client_files) ? JSON.parse(client_files) : []
  }

  setClientFiles(client_files) {
    return (!!client_files) ? JSON.stringify(client_files) : '[]'
  }

  getBucketUrl(userType) {
    return `${this.id}/files/${userType.toLowerCase()}/`
  }

  getClientViewedIn(date) {
    return (date) ? moment(date).format('YYYY-MM-DD HH:mm:ss') : null
  }

  getPartnerViewedIn(date) {
    return (date) ? moment(date).format('YYYY-MM-DD HH:mm:ss') : null
  }

  getAdminViewedIn(date) {
    return (date) ? moment(date).format('YYYY-MM-DD HH:mm:ss') : null
  }

  transactions() {
    return this.hasMany('App/Models/Transaction', 'id', 'solicitation_id')
  }

  getTags(tags) {
    return (!!tags) ? JSON.parse(tags) : []
  }

  setTags(tags) {
    return (!!tags) ? JSON.stringify(tags) : '[]'
  }

}

module.exports = Solicitation
