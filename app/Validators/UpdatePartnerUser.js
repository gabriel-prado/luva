'use strict'

class UpdatePartnerUser {
  get rules () {
    return {
      name: 'string',
      password: 'string',
      type: 'in:CLIENT,PARTNER',
      email: 'email|unique:users,email',
      photo_url: 'string',
      'partner.entity_type': 'in:PF,PJ',
      'partner.document_number': 'string',
      'partner.prefix_oab': 'string',
      'partner.oab_number': 'string',
      'partner.status': 'in:ACTIVE,INACTIVE,REQUESTED',
      'partner.notes': 'string',
      'partner.rg': 'string',
      'partner.files': 'array',
      'partner.files.*': 'string',
      'partner.specialty': 'array',
      'partner.specialty.*': 'string',
      'partner.address.zip_code': 'string',
      'partner.address.state': 'string',
      'partner.address.city': 'string',
      'partner.address.neighborhood': 'string',
      'partner.address.street': 'string',
      'partner.address.number': 'integer',
      'partner.address.complement': 'string',
      'partner.contact.name': 'string',
      'partner.contact.phone': 'string',
      'partner.contact.email': 'string',
      'partner.bank.name': 'string',
      'partner.bank.agency': 'string',
      'partner.bank.account': 'string',
      'partner.bank.responsible.name': 'string',
      'partner.bank.responsible.cpf': 'string',
      'partner.complementary_data.gender': 'string',
      'partner.complementary_data.bith_date': 'string',
      'partner.complementary_data.language': 'array',
      'partner.complementary_data.language.*': 'string',
      'partner.complementary_data.civil_state': 'string',
      'partner.complementary_data.availability_attendance': 'array',
      'partner.complementary_data.availability_attendance.*': 'string'
    }
  }

  get data () {
    const body = this.ctx.request.only([
      'name',
      'email',
      'password',
      'email',
      'type',
      'photo_url',
      'partner'
    ])

    this.ctx.request.body = body
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdatePartnerUser
