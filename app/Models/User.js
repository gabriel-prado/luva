'use strict'

const Model = use('Model')

class User extends Model {

  static boot () {
    super.boot()
    this.addHook('beforeCreate', 'User.hashPassword')
  }

  static get hidden () {
    return ['password']
  }

  partner () {
    return this.hasOne('App/Models/Partner', 'id', 'id')
  }

  companies() {
    return this
      .belongsToMany('App/Models/Company')
      .withTimestamps()
      .pivotTable('company_user')
      .withPivot(['permissions'])
      .pivotModel('App/Models/CompanyUser')
  }

  solicitations() {
    const types = {
      CLIENT: 'client_id',
      PARTNER: 'partner_id'
    }
    return this.hasMany('App/Models/Solicitation', 'id', types[this.type] )
  }

  messages() {
    return this.hasMany('App/Models/Message')
  }

  async hasSolicitation(solicitationId) {
    if (this.type === 'ADMIN') {
      return true
    } else if (this.type === 'PARTNER') {
      return !!(await this.solicitations().where('id', solicitationId).getCount())
    } else if (this.type === 'CLIENT') {
      return !!(
        await this
          .companies()
          .whereHas('solicitations', (builder) => {
            builder.where('id', solicitationId)
          })
          .getCount()
      )
    }
  }

  async hasCompany(companyId) {
    if (this.type === 'ADMIN') {
      return true
    } else if (this.type === 'PARTNER') {
      return false
    } else if (this.type === 'CLIENT') {
      return !!(await this.companies().where('company_id', companyId).getCount())
    }
  }

  async isClientUp() {
    if (this.type !== 'CLIENT') return false

    const company = await this.companies().first()
    return (company) ? !!company.client_up : false
  }

  getPermissions(permissions) {
    return (!!permissions) ? JSON.parse(permissions) : []
  }

  setPermissions(permissions) {
    return (!!permissions) ? JSON.stringify(permissions) : '[]'
  }

  async hasAnyCompany() {
    if (this.type === 'ADMIN') {
      return true
    } else if (this.type === 'PARTNER') {
      return false
    } else if (this.type === 'CLIENT') {
      return !!(await this.companies().getCount())
    }
  }

  async hasAnyActiveCompany() {
    if (this.type === 'ADMIN') {
      return true
    } else if (this.type === 'PARTNER') {
      return false
    } else if (this.type === 'CLIENT') {
      return !!(await this.companies().where('status', 'ACTIVE').getCount())
    }
  }

}

module.exports = User
