'use strict'

const ace = require('@adonisjs/ace')
const clearDataBase = use('Test/utils/ClearDataBase')
const { ioc } = use('@adonisjs/fold')

module.exports = (cli, runner) => {
  runner.timeout(15000)

  runner.before(async () => {
    use('Adonis/Src/Server').listen(process.env.HOST, process.env.PORT)
    //Mocks into -> start/hooks.js
    await ace.call('migration:run', null, { silent: true })
  })

  runner.after(async () => {
    use('Adonis/Src/Server').getInstance().close()

    ioc.restore('App/Services/S3')
    ioc.restore('App/Services/SendGrid')

    await clearDataBase()
    await ace.call('migration:reset', null, { silent: true })
  })
}
