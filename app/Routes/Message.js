'use strict'

const Route = use('Route')

Route
  .resource('solicitation.message', 'MessageController')
  .only(['index', 'store'])
  .middleware(['auth', 'acl:ADMIN,CLIENT,PARTNER', 'access:SOLICITATION'])
  .validator(new Map([
    [['solicitation.message.store'], ['StoreMessage']]
  ]))
