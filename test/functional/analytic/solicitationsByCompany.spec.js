'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const moment = use('moment')
const { test, trait, beforeEach } = use('Test/Suite')('Analytic - GET /analytics/solicitation-x-company')

let defaultAdmin = null
let defaultCompanies = null
let companiesIds = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompanies = await Factory.model('App/Models/Company').createMany(3, { status: 'ACTIVE' })
  companiesIds = defaultCompanies.map((c) => c.id)

  await clientUser.companies().attach(companiesIds, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  await Factory.model('App/Models/Solicitation').createMany(3, {
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: companiesIds[0]
  })

  await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: clientUser.id,
    company_id: companiesIds[1]
  })

})

test('should return the amount of solicitation per company and return status 200', async({ client, assert}) => {
  const start_date = moment().subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss')
  const end_date = moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss')

  const response = await
    client
      .get('/analytics/solicitation-x-company')
      .query({
        companies_id: companiesIds,
        start_date,
        end_date
      })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 3)
  assert.equal(response.body[0].label, defaultCompanies[0].name)
  assert.equal(response.body[1].label, defaultCompanies[1].name)
  assert.equal(response.body[2].label, defaultCompanies[2].name)
  assert.equal(response.body[0].value, 3)
  assert.equal(response.body[1].value, 1)
  assert.equal(response.body[2].value, 0)
})

test('should not return the amount of solicitation per company and return status 401', async ({ client, assert }) => {
  const response = await client.get('/analytics/solicitation-x-company').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.get('/analytics/solicitation-x-company').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with client role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const response = await client.get('/analytics/solicitation-x-company').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
