'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - POST /auth/login')

trait('Test/ApiClient')

beforeEach(async () => {
  await clearDataBase()
})

test('should authenticate admin', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123',
    type: 'ADMIN'
  })

  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})

test('should authenticate client', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123',
    type: 'CLIENT'
  })
  let company = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await user.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})

test('should authenticate partner', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123',
    type: 'PARTNER',
    status: 'ACTIVE'
  })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'ACTIVE' })

  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})

test('should not authenticate non-existent user', async({ client, assert}) => {
  const response = await client.post('auth/login').send({
    email: 'user@email.com',
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 401)
})

test('should not authenticate non-active partner', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123',
    type: 'PARTNER'
  })
  await Factory.model('App/Models/Partner').create({ id: user.id, status: 'REQUESTED' })

  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 401)
})

test('should not authenticate non-active client', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123',
    type: 'CLIENT'
  })
  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 401)
})
