'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Message - GET /solicitation/id/message')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultSolicitation = null
let defaultClient = null
let defaultPartner = null
let defaultAdmin = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id
  })
  await Factory.model('App/Models/Message').createMany(8, {
    solicitation_id: defaultSolicitation.id,
    user_id: defaultClient.id,
    chat_type: 'CLIENT'
  })
  await Factory.model('App/Models/Message').createMany(4, {
    solicitation_id: defaultSolicitation.id,
    user_id: defaultPartner.id,
    chat_type: 'PARTNER'
  })
})

test('should list all messages for admin user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ all: true, chat_type: 'CLIENT', company_id: 1 })
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 8)
  assert.isBelow(response.body.data[0].id, response.body.data[1].id)
})

test('should list paginate messages for admin user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ chat_type: 'CLIENT', direction: 'desc', order_by: 'id', company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
  assert.isAbove(response.body.data[0].id, response.body.data[1].id)
})

test('should list all messages for client user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ all: true, company_id: 1 })
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 8)
})

test('should list all messages for partner user and return status 200', async({ client, assert}) => {
  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ all: true, company_id: 1 })
      .loginVia(defaultPartner)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 4)
})

test('should try list messages from another client user and return status 401', async({ client, assert}) => {
  const userClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await userClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ all: true, company_id: userCompany.id })
      .loginVia(userClient)
      .end()

  assert.equal(response.status, 401)
})

test('should try list messages from another partner user and return status 401', async({ client, assert}) => {
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })

  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .query({ all: true, company_id: 1 })
      .loginVia(partner)
      .end()

  assert.equal(response.status, 401)
})

test('should not list messages for not logged user', async ({ client, assert }) => {
  const response =
    await client
      .get(`solicitation/${defaultSolicitation.id}/message`)
      .end()

  assert.equal(response.status, 401)
})
