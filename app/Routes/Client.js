'use strict'

const Route = use('Route')

Route
  .post('client', 'UserController.storeClient')
  .validator('StoreClientUser')
