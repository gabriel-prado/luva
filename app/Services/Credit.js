'use strict'

use('numeral/locales')
const Env = use('Env')
const Numeral = use('numeral')
const moment = use('moment')
const TransactionModel = use('App/Models/Transaction')
const CompanyModel = use('App/Models/Company')

class CreditService {

  constructor (SendGridService, CompanyService) {
    this.sgMail = SendGridService
    this.companyService = CompanyService
  }

  async request(value, companyId, user) {
    const company = await this.companyService.show(companyId, user.type, user.id)
    const subjectPrefix = company.client_up ? 'Up Legal' : 'Luva Assessoria'

    Numeral.locale('pt-br')

    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: Env.get('DEFAULT_ADMIN_MAIL'),
      subject: `${subjectPrefix} - Nova solicitação de crédito`,
      templateId: Env.get('REQUEST_CREDIT_TEMPLATE_ID'),
      substitutions: {
        client_name: user.name,
        company_name: company.name,
        value: Numeral(value).format('$ 0,0.00')
      }
    }

    await this.sgMail.send(msg, company.client_up)
  }

  async add (company, value, expire) {
    if (expire && await company.hasActivePlan()) {
      throw new Error('THERE_IS_ALREADY_AN_ACTIVE_MONTHLY_CREDIT')
    }

    const lastTransaction = await company.lastTransaction()

    const attributes = {
      expire,
      value,
      operation: 'ADD',
      balance_expiring: (lastTransaction) ? lastTransaction.balance_expiring : 0.00,
      balance_not_expiring: (lastTransaction) ? lastTransaction.balance_not_expiring : 0.00,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      company_id: company.id
    }

    attributes[((expire) ? 'balance_expiring' : 'balance_not_expiring')] += value

    return await TransactionModel.create(attributes)
  }

  async remove (company, value, solicitationId, trx = null) {
    const lastTransaction = await company.lastTransaction()
    const hasPlan = await company.hasActivePlan()

    if (!company.unlimited_balance && (!lastTransaction || await company.balance() < value)) {
      throw new Error('INSUFFICIENT_COMPANY_BALANCE')
    }

    const attributes = {
      value,
      operation: 'REMOVE',
      solicitation_id: solicitationId,
      balance_expiring: (lastTransaction) ? lastTransaction.balance_expiring : 0,
      balance_not_expiring: (lastTransaction) ? lastTransaction.balance_not_expiring : 0,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      company_id: company.id
    }

    if (!company.unlimited_balance) {
      if (hasPlan) {
        if (attributes.balance_expiring < value) {
          const rest = value - attributes.balance_expiring
          attributes.balance_expiring = 0
          attributes.balance_not_expiring -= rest
        } else {
          attributes.balance_expiring -= value
        }
      } else {
        attributes.balance_not_expiring -= value
      }
    }

    return await TransactionModel.create(attributes, trx)
  }

  async balance (companyId, user) {
    const company = await CompanyModel.findOrFail(companyId)
    await this.expire(companyId)
    if (!(await user.hasCompany(companyId))) {
      throw new Error('COMPANY_DOES_NOT_BELONG_TO_USER')
    }

    return { credit: await company.balance() }
  }

  async expire (companyId) {
    const thirtyDaysAgo = moment().subtract(30, 'days')
    const company = await CompanyModel.findOrFail(companyId)
    const lastTransaction = await company.lastTransaction()

    if (lastTransaction && lastTransaction.balance_expiring > 0) {
      const lastExpireTransaction = await
        TransactionModel
          .query()
          .where({
            operation: 'ADD',
            expire: true,
            company_id: companyId
          })
          .orderBy('created_at', 'desc')
          .orderBy('id', 'desc')
          .first()

      if (moment(lastExpireTransaction.date).isBefore(thirtyDaysAgo)) {
        await TransactionModel.create({
          value: lastTransaction.balance_expiring,
          operation: 'REMOVE',
          balance_expiring: 0,
          balance_not_expiring: lastTransaction.balance_not_expiring,
          date: moment().format('YYYY-MM-DD HH:mm:ss'),
          company_id: companyId
        })
      }

    }
  }

}

module.exports = CreditService
