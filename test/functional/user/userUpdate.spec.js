'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const UserModel = use('App/Models/User')
const PartnerModel = use('App/Models/Partner')
const { test, trait, beforeEach } = use('Test/Suite')('User - PUT /me')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should update admin user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  user = user.toJSON()
  user.name = 'new_name'
  user.email = 'new_email@email.com'
  user.oab_number = '1234'
  user.prefix_oab = 'OAB/RJ'

  const response =
    await client
      .put(`me`)
      .send(user)
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, user.name)
  assert.equal(response.body.email, user.email)
  const userInDatabase = await UserModel.find(user.id)
  assert.equal(userInDatabase.name, user.name)
  assert.equal(userInDatabase.email, user.email)
  assert.equal(userInDatabase.oab_number, '1234')
  assert.equal(userInDatabase.prefix_oab, 'OAB/RJ')
})

test('should update admin user with same email and return status 400', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  user = user.toJSON()
  user.name = 'new_name'

  const response =
    await client
      .put(`me`)
      .send(user)
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].message, 'unique validation failed on email')
})

test('should update client user and return status 200', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  let company = await Factory.model('App/Models/Company').make()
  await user.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  user = user.toJSON()
  user.name = 'new_name'
  user.oab_number = '1234'
  user.prefix_oab = 'OAB/RJ'
  delete user.email

  const response =
    await client
      .put(`me`)
      .send(user)
      .query({company_id: 1})
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.name, 'new_name')
  const userInDatabase = await UserModel.find(user.id)
  assert.equal(userInDatabase.name, 'new_name')
  assert.isNull(userInDatabase.oab_number)
  assert.isNull(userInDatabase.prefix_oab)
})

test('should not update for not logged user', async ({ client, assert }) => {
  const response = await client.put('me').end()

  assert.equal(response.status, 401)
})

test('should try to access the route with partner role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const response = await client.put('me').query({company_id: 1}).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})
