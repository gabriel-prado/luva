'use strict'

const Schema = use('Schema')

class AddPermissionsToCompanyUserSchema extends Schema {
  up () {
    this.table('company_user', (table) => {
      table.specificType('permissions', 'JSON')
    })
  }

  down () {
    this.table('company_user', (table) => {
      table.dropColumn('permissions')
    })
  }
}

module.exports = AddPermissionsToCompanyUserSchema
