'use strict'

const PartnerModel = use('App/Models/Partner')
const UserModel = use('App/Models/User')
const SolicitationModel = use('App/Models/Solicitation')
const moment = use('moment')
const Database = use('Database')
const Env = use('Env')

class PartnerService {

  constructor (SolicitationService, SendGridService, MessageService) {
    this.solicitationService = SolicitationService
    this.sgMail = SendGridService
    this.messageService = MessageService
  }

  async store (attributes, trx = null) {
    return await PartnerModel.create(attributes, trx)
  }

  async update (id, attributes, type, user, trx = null) {
    if (type !== 'ADMIN') {
      delete attributes.notes
      delete attributes.status
    }

    const partner = await PartnerModel.findOrFail(id)
    const partnerHasBeenActivated = partner.status !== attributes.status && attributes.status === 'ACTIVE'

    partner.merge(attributes)
    await partner.save(trx)

    if (partnerHasBeenActivated) {
      const mail = this.sgMail.buildActivatedPartnerMail(user)
      await this.sgMail.send(mail)
    }

    return partner
  }

  async list (page = null, perPage = null, condition = {}, type) {
    return await UserModel
        .query()
        .where('type', 'PARTNER')
        .with('partner', (builder) => {
          if (type !== 'ADMIN') builder.setHidden(['status', 'notes'])
        })
        .whereHas('partner', (builder) => {
          if (!!condition.specialty) {
            builder.whereRaw(`JSON_SEARCH(specialty, 'one', '${condition.specialty}', NULL, '$') IS NOT NULL`)
            delete condition.specialty
          }

          builder.where(condition)
        })
        .paginate(page, perPage)
  }

  async partnerDelayed () {
    const yesterday = moment().subtract(24, 'hours').format('YYYY-MM-DD HH:mm:ss')

    let solicitations =
      await SolicitationModel
        .query()
        .with('partner')
        .where('status', 'WAITING_FOR_PARTNER_RESPONSE')
        .whereNotNull('partner_id')
        .fetch()

    solicitations = solicitations.toJSON()
    const solicitationsDelayeds = solicitations.filter(s => {
      const sla = this.solicitationService.getSla(s)
      const noticeTime = moment(s.partner_notice_time).format('YYYY-MM-DD HH:mm:ss')

      return (sla.sla_partner.current >= sla.sla_partner.total) &&
      (s.partner_notice_time === null || moment(noticeTime).isBefore(yesterday))
    })

    await this.partnerDelayedSendMails(solicitationsDelayeds)

    await this.solicitationService.updateMany(solicitationsDelayeds, {
      partner_notice_time: moment().format('YYYY-MM-DD HH:mm:ss')
    })
  }

  partnerDelayedSendMails (solicitationsDelayeds) {
    const promises = new Array()

    solicitationsDelayeds.forEach(s => {
      const msg = {
        from: Env.get('DEFAULT_CONTACT_MAIL'),
        subject: `Luva Assessoria - Parceiro Atrasado`,
        templateId: Env.get('PARTNER_DELAYED_TEMPLATE_ID'),
        substitutions: {
          name: s.partner.name,
          id: s.id,
          link: `${Env.get('FRONTEND_LUVA_URL')}/solicitacao/${s.id}`
        }
      }

      const adminMsg = {
        ...msg,
        to: Env.get('DEFAULT_ADMIN_MAIL')
      }

      const partnerMsg = {
        ...msg,
        to: s.partner.email
      }

      promises.push(this.sgMail.send(partnerMsg))
      promises.push(this.sgMail.send(adminMsg))
    })

    return Promise.all(promises)
  }

  async partnerNotAccept () {
    let solicitations =
      await SolicitationModel
        .query()
        .with('partner')
        .with('client')
        .where('status', 'WAITING_FOR_PARTNER_ACCEPT')
        .whereNotNull('partner_id')
        .fetch()

    solicitations = solicitations.toJSON()
    const solicitationsDelayeds = solicitations.filter(s => {
      const sla = this.solicitationService.getSla(s)
      return sla.sla_partner.current >= sla.sla_partner.total
    })
    const trx = await Database.beginTransaction()
    const attributes = {
      status: 'WAITING_FOR_SELECT_NEW_PARTNER',
      partner_sla_start_date: null,
      partner_id: null
    }

    try {
      await this.solicitationService.updateMany(solicitationsDelayeds, attributes, trx)
      await this.partnerNotAcceptCreateMessages(solicitationsDelayeds,trx)
      await trx.commit()
    } catch(e) {
      await trx.rollback()
      throw e
    }

    await this.partnerNotAcceptSendMails(solicitationsDelayeds)
  }

  partnerNotAcceptCreateMessages (solicitations, trx) {
     const promises = new Array()
     solicitations.forEach(s => {
       const clientMessage = this.messageService.buildPartnerRemovedMessage('CLIENT', s.client.id, s.partner.name, s.id)
       const partnerMessage = this.messageService.buildPartnerRemovedMessage('PARTNER', s.partner.id, s.partner.name, s.id)

       promises.push(this.messageService.store(clientMessage, trx))
       promises.push(this.messageService.store(partnerMessage, trx))
     })

     return Promise.all(promises)
   }

  partnerNotAcceptSendMails (solicitations) {
    const promises = new Array()

    solicitations.forEach(s => {
      const msg = {
        from: Env.get('DEFAULT_CONTACT_MAIL'),
        to: Env.get('DEFAULT_ADMIN_MAIL'),
        subject: `Luva Assessoria - Parceiro Removido`,
        templateId: Env.get('PARTNER_REMOVED_TEMPLATE_ID'),
        substitutions: {
          name: s.partner.name,
          id: s.id,
          link: `${Env.get('FRONTEND_LUVA_URL')}/solicitacao/${s.id}`
        }
      }

      promises.push(this.sgMail.send(msg))
    })

    return Promise.all(promises)
  }

}

module.exports = PartnerService
