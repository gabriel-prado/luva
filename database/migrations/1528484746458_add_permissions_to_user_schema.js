'use strict'

const Schema = use('Schema')

class AddPermissionsToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('active').notNullable().defaultTo(true)
      table.specificType('permissions', 'JSON')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('permissions')
      table.dropColumn('active')
    })
  }
}

module.exports = AddPermissionsToUserSchema
