'use strict'

const Model = use('Model')
const moment = use('moment')

class Company extends Model {

  users() {
    return this.belongsToMany('App/Models/User')
      .withTimestamps()
      .pivotTable('company_user')
      .withPivot(['permissions'])
      .pivotModel('App/Models/CompanyUser')
  }

  getContact(contact) {
    return (!!contact) ? JSON.parse(contact) : {}
  }

  setContact(contact) {
    return (!!contact) ? JSON.stringify(contact) : '{}'
  }

  getAddress(address) {
    return (!!address) ? JSON.parse(address) : {}
  }

  setAddress(address) {
    return (!!address) ? JSON.stringify(address) : '{}'
  }

  getComplementaryData(complementary_data) {
    return (!!complementary_data) ? JSON.parse(complementary_data) : {}
  }

  setComplementaryData(complementary_data ) {
    return (!!complementary_data) ? JSON.stringify(complementary_data) : '{}'
  }

  getFiles(files) {
    return (!!files) ? JSON.parse(files) : []
  }

  setFiles(files) {
    return (!!files) ? JSON.stringify(files) : '[]'
  }

  getAreaOfExpertise(area_of_expertise) {
    return (!!area_of_expertise) ? JSON.parse(area_of_expertise) : []
  }

  setAreaOfExpertise(area_of_expertise) {
    return (!!area_of_expertise) ? JSON.stringify(area_of_expertise) : '[]'
  }

  solicitations() {
    return this.hasMany('App/Models/Solicitation', 'id', 'company_id')
  }

  transactions() {
    return this.hasMany('App/Models/Transaction', 'id', 'company_id')
  }

  canCreateSolicitation() {
    return !!(
      this.status === 'ACTIVE' &&
      this.society &&
      this.activity &&
      this.size &&
      this.taxation_regime &&
      this.area_of_expertise &&
      this.getAreaOfExpertise(this.area_of_expertise).length
    )
  }

  lastTransaction() {
    return this.transactions().orderBy('id', 'desc').first()
  }

  async hasActivePlan() {
    const expireDate = moment().subtract(30, 'days').format('YYYY-MM-DD')
    return !!(await this.transactions().where('expire', true).where('date', '>=', expireDate).where('operation', 'ADD').getCount())
  }

  async balance() {
    const lastTransaction = await this.lastTransaction()
    return (lastTransaction) ? lastTransaction.balance_not_expiring + lastTransaction.balance_expiring : 0
  }

}

module.exports = Company
