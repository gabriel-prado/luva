'use strict'

class MessageController {
  static get inject () {
    return ['App/Services/Message']
  }

  constructor (MessageService) {
    this.messageService = MessageService
  }

  async index ({ request, auth, params }) {
    let { page, perPage, all, chat_type, order_by, direction } = request.get()
    const userType = auth.user.type
    const solicitationId = Number(params.solicitation_id)

    page = (all) ? null : Number(page) || 1
    perPage = (all) ? null : Number(perPage) || 5
    chat_type = (userType === 'ADMIN') ? chat_type : userType
    direction = (direction) ? direction.toLowerCase() : 'asc'
    order_by = (order_by) ? order_by.toLowerCase() : 'id'

    try {
      return await this.messageService.list(solicitationId, auth.user, chat_type, page, perPage, direction, order_by)
    } catch (e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  async store ({ request, auth, params }) {
    const solicitationId = Number(params.solicitation_id)
    const attributes = request.body

    if (auth.user.type !== 'ADMIN') attributes.chat_type = auth.user.type

    attributes.user_id = auth.user.id
    attributes.solicitation_id = solicitationId

    if (!!(await auth.user.hasSolicitation(solicitationId))) {
      return this.messageService.create(attributes, solicitationId, auth.user)
    } else {
      const e = new Error('SOLICITATION_DOES_NOT_BELONG_TO_USER')
      e.status = 401
      throw e
    }
  }
}

module.exports = MessageController
