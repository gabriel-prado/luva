'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Credit - POST /company/id/credit/add')

let defaultAdmin = null
let defaultCompany = null

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  const client = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  defaultCompany = await Factory.model('App/Models/Company').make()
  await client.companies().save(defaultCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
})

test('should add credit and get 200', async ({ client, assert}) => {
  const credit = { value: 123, expire: true }
  const response =
    await
      client
        .post(`company/${defaultCompany.id}/credit/add`)
        .send(credit)
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  assert.equal(response.status, 200)
  assert.equal(response.body.value, credit.value)
  assert.equal(response.body.balance_expiring, credit.value)
  assert.equal(response.body.balance_not_expiring, 0.00)
})

test('should add credit with another monthly transaction and get 500', async({ client, assert}) => {
  const credit = { value: 123, expire: true }
  await
    client
      .post(`company/${defaultCompany.id}/credit/add`)
      .send(credit)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  const response =
    await
      client
        .post(`company/${defaultCompany.id}/credit/add`)
        .send(credit)
        .query({company_id: 1})
        .loginVia(defaultAdmin)
        .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.message, 'THERE_IS_ALREADY_AN_ACTIVE_MONTHLY_CREDIT')
})

test('should try add credit as CLIENT and get 401 - Unauthorized', async({ client, assert}) => {
  const clientUser = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const company = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await clientUser.companies().save(company, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  const credit = { value: 123, expire: true }
  const response =
    await
      client
        .post(`company/${company.id}/credit/add`)
        .send(credit)
        .query({company_id: 1})
        .loginVia(clientUser)
        .end()

  assert.equal(response.status, 401)
})

test('should try add credit as PARTNER and get 401 - Unauthorized', async({ client, assert}) => {
  const partnerUser = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  const credit = { value: 123, expire: true }
  const response =
    await
      client
        .post(`company/${defaultCompany.id}/credit/add`)
        .send(credit)
        .query({company_id: 1})
        .loginVia(partnerUser)
        .end()

  assert.equal(response.status, 401)
})

test('should return 401 when have no logged user', async({ client, assert }) => {
  const response =
    await
      client
        .post(`company/${defaultCompany.id}/credit/add`)
        .end()

  assert.equal(response.status, 401)
})
