'use strict'

const SolicitationModel = use('App/Models/Solicitation')

class SolicitationController {

  static get inject () {
    return ['App/Services/Solicitation']
  }

  constructor (SolicitationService) {
    this.solicitationService = SolicitationService
  }

  async index({ request, auth }) {
    const filters = request.get()

    const page = (filters.all) ? null : Number(filters.page) || 1
    const perPage = (filters.all) ? null : Number(filters.perPage) || 5

    return await this.solicitationService.list(page, perPage, filters, auth.user)
  }

  async store ({ request, response, auth, params }) {
    const { body } = request
    const id = Number(params.id)
    const { user } = auth
    const { description } = body
    delete body.description
    body.company_id = id
    body.client_id = user.id

    try {
      return await this.solicitationService.create(id, user, body, description)
    } catch (e) {
      if (e.message === 'INCOMPLETE_COMPANY_BASIC_DATA') e.status = 412
      if (e.message === 'COMPANY_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  async uploadFile ({ request, response, auth, params }) {
    try {
      let { chat_type } = request.get()
      if (auth.user.type !== 'ADMIN') chat_type = auth.user.type
      const { id } = params
      const solicitation = await SolicitationModel.findOrFail(id)
      const solicitationJSON = solicitation.toJSON()
      const fileNames = solicitationJSON[this.solicitationService.columnsFile[chat_type]]
      const resp = new Array()

      request.multipart.file('files', {}, async (file) => {
        const fileName = this.solicitationService._fixDuplicateFileName(fileNames, file.stream.filename)
        fileNames.push(fileName)
        file.stream.filename = fileName
        resp.push(fileName)
        await this.solicitationService.uploadFile(file, solicitation, auth.user, chat_type)
      })

      await request.multipart.process()

      return resp
    } catch (e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  forwardFile ({ request, params, auth }) {
    const { chat_type, text } = request.all()
    const { id } = params

    return this.solicitationService.forwardFile(id, text, chat_type, auth.user)
  }

  async getFile ({ params, auth, request, response }) {
    const { id } = params
    const { file, chat_type } = request.get()
    if(!file) return response.status(404).send({})
    const solicitation = await SolicitationModel.findOrFail(id)

    try {
      let res = await this.solicitationService.getFile(file, solicitation, auth.user, chat_type)
      if(!res) return response.status(404).send({})

      response.header('content-type', res.ContentType)
      response.header('content-disposition', `filename=${file}`)

      return res.Body
    } catch (e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      if (e.message === 'THIS_FILE_DOES_NOT_EXISTS') e.status = 404
      throw e
    }
  }

  async show ({ auth, params }) {
    try {
      return await this.solicitationService.show(Number(params.id), auth.user)
    } catch (e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  async cancel({ params, auth }) {
    return await this.solicitationService.cancel(Number(params.solicitation_id), auth.user)
  }

  async accept({ params, auth }) {
    try {
      return await this.solicitationService.accept(Number(params.solicitation_id), auth.user)
    } catch(e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  async refuse({ params, auth }) {
    try {
      return await this.solicitationService.refuse(Number(params.solicitation_id), auth.user)
    } catch(e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  classify ({ request, params, auth }) {
    return this.solicitationService.classify(Number(params.id), request.body, auth.user)
  }

  async finalize ({ params, auth }) {
    try {
      return await this.solicitationService.finalize(Number(params.solicitation_id), auth.user)
    } catch(e) {
      if (e.message === 'SOLICITATION_DOES_NOT_BELONG_TO_USER') e.status = 401
      throw e
    }
  }

  update ({ request, params }) {
    return this.solicitationService.update(request.body, Number(params.id))
  }

  favorite ({ request, params }) {
    const { favorite } = request.all()

    return this.solicitationService.update({ favorite }, Number(params.solicitation_id))
  }

}

module.exports = SolicitationController
