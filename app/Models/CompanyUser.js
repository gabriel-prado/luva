'use strict'

const Model = use('Model')

class CompanyUser extends Model {

  static get table () {
    return 'company_user'
  }

  getPermissions(permissions) {
    return (!!permissions) ? JSON.parse(permissions) : []
  }

  setPermissions(permissions) {
    return (!!permissions) ? JSON.stringify(permissions) : '[]'
  }

}

module.exports = CompanyUser
