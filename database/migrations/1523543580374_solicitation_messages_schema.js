'use strict'

const Schema = use('Schema')

class SolicitationMessagesSchema extends Schema {
  up () {
    this.create('solicitation_messages', (table) => {
      table.increments()
      table.text('text')
      table.string('type', 20).notNullable()
      table.integer('solicitation_id').notNullable().unsigned()
      table.foreign('solicitation_id').references('solicitations.id')
      table.integer('user_id').notNullable().unsigned()
      table.foreign('user_id').references('users.id')
      table.string('chat_type', 20).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('solicitation_messages')
  }
}

module.exports = SolicitationMessagesSchema
