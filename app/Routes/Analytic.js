'use strict'

const Route = use('Route')

Route.group('Analytics', () => {
  Route.get('solicitation-x-company', 'AnalyticController.solicitationByCompany')
  Route.get('average-sla-time', 'AnalyticController.averageSlaTime')
  Route.get('sla-compliance', 'AnalyticController.slaCompliance')
  Route.get('export', 'AnalyticController.export')
}).prefix('analytics/').middleware(['auth', 'acl:ADMIN', 'access:REPORT'])
