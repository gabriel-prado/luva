'use strict'

const Route = use('Route')

Route
  .post('company/:id/solicitation', 'SolicitationController.store')
  .middleware(['auth', 'acl:CLIENT'])
  .validator('StoreSolicitation')

Route.group('Solicitation', () => {
  Route.post('cancel', 'SolicitationController.cancel').middleware(['auth', 'acl:CLIENT,ADMIN', 'access:SOLICITATION'])
  Route.post('accept', 'SolicitationController.accept').middleware(['auth', 'acl:PARTNER', 'access:SOLICITATION'])
  Route.post('refuse', 'SolicitationController.refuse').middleware(['auth', 'acl:PARTNER', 'access:SOLICITATION'])
  Route.post('finalize', 'SolicitationController.finalize').middleware(['auth', 'acl:CLIENT,ADMIN', 'access:SOLICITATION'])
  Route.put('favorite', 'SolicitationController.favorite').middleware(['auth', 'acl:ADMIN', 'access:SOLICITATION']).validator('UpdateFavorite')
}).prefix('solicitation/:solicitation_id')

Route.group('Solicitation File', () => {
  Route.post('', 'SolicitationController.uploadFile')
  Route.get('', 'SolicitationController.getFile')
}).prefix('solicitation/:id/file').middleware(['auth', 'acl:CLIENT,ADMIN,PARTNER', 'access:SOLICITATION'])

Route
  .post('solicitation/:id/message/forward-file', 'SolicitationController.forwardFile')
  .middleware(['auth', 'acl:ADMIN', 'access:SOLICITATION'])

Route
  .resource('solicitation', 'SolicitationController')
  .only(['index', 'show', 'update'])
  .middleware(new Map([
    [['solicitation.index'], ['auth', 'acl:ADMIN,CLIENT,PARTNER', 'access:SOLICITATION']],
    [['solicitation.show'], ['auth', 'acl:ADMIN,CLIENT,PARTNER', 'access:SOLICITATION']],
    [['solicitation.update'], ['auth', 'acl:ADMIN', 'access:SOLICITATION']]
  ]))
  .validator(new Map([
    [['solicitation.update'], ['UpdateSolicitation']]
  ]))

Route
  .post('solicitation/:id/classify', 'SolicitationController.classify')
  .middleware(['auth', 'acl:ADMIN', 'access:SOLICITATION'])
  .validator('ClassifySolicitation')
