'use strict'

const SolicitationModel = use('App/Models/Solicitation')
const moment = use('moment')
const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Message - POST /solicitation/id/message')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultSolicitation = null
let defaultClient = null
let defaultPartner = null
let defaultAdmin = null
let userCompany = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdmin = await Factory.model('App/Models/User').create({ type: 'ADMIN' })

  defaultClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await defaultClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })

  defaultPartner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: defaultPartner.id, status: 'ACTIVE' })

  defaultSolicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id
  })
})

test('should create message for admin user and return status 200', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultAdmin.id)

  const response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 200)
  delete response.body.id
  delete response.body.created_at
  delete response.body.updated_at
  assert.deepEqual(response.body, message)
})

test('Should test send a message as ADMIN in the client chat and check if the status changed to WAITING_FOR_CLIENT_RESPONSE', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultAdmin.id)
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLASSIFICATION',
    client_id: defaultClient.id,
    company_id: userCompany.id
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)

  assert.equal(response.status, 200)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_CLIENT_RESPONSE')
})

test('should create message for solicitation FINALIZED and return status 204', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultAdmin.id)
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'FINALIZED',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 204)
})

test('should create message for solicitation CANCELED and return status 204', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultAdmin.id)
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'CANCELED',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 204)
})

test('should create message for client user and return status 200', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultClient.id)

  const response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  assert.equal(response.status, 200)
  delete response.body.id
  delete response.body.created_at
  delete response.body.updated_at
  assert.deepEqual(response.body, message)
})

test('should create message for partner user and return status 200', async({ client, assert}) => {
  const message = await _makeMessage('PARTNER', defaultPartner.id)

  const response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultPartner)
      .end()

  assert.equal(response.status, 200)
  delete response.body.id
  delete response.body.created_at
  delete response.body.updated_at
  assert.deepEqual(response.body, message)
})

test('should try create message from another client user and return status 401', async({ client, assert}) => {
  const userClient = await Factory.model('App/Models/User').create({ type: 'CLIENT' })
  const userCompany = await Factory.model('App/Models/Company').make({ status: 'ACTIVE' })
  await userClient.companies().save(userCompany, (row) => {
    row.permissions = ["REPORT","SOLICITATION","CREDIT","COMPANY","USER"]
  })
  const message = await _makeMessage('CLIENT', userClient.id)
  let response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .send(message)
      .query({company_id: userCompany.id})
      .loginVia(userClient)
      .end()

  assert.equal(response.status, 401)
})

test('should try create message from another partner user and return status 401', async({ client, assert}) => {
  const partner = await Factory.model('App/Models/User').create({ type: 'PARTNER' })
  await Factory.model('App/Models/Partner').create({ id: partner.id, status: 'ACTIVE' })
  const message = await _makeMessage('PARTNER', partner.id)
  let response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(partner)
      .end()

  assert.equal(response.status, 401)
})

test('should try get solicitation that does not exist and return 404', async({ client, assert}) => {
  const message = await _makeMessage('CLIENT', defaultAdmin.id)
  const response =
    await client
      .post(`solicitation/33/message`)
      .send(message)
      .query({company_id: 1})
      .loginVia(defaultAdmin)
      .end()

  assert.equal(response.status, 404)
})

test('should not create message for not logged user', async ({ client, assert }) => {
  const response =
    await client
      .post(`solicitation/${defaultSolicitation.id}/message`)
      .end()

  assert.equal(response.status, 401)
})

test('should send message for CLIENT user and change solicitation status from WAITING_FOR_ADMIN_RESPONSE', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_CLIENT_RESPONSE',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    sla_end_date: moment().add(1, 'hour').format('YYYY-MM-DD HH:mm:ss')
  })
  const message = await Factory.model('App/Models/Message').make({
    solicitation_id: solicitation.id,
    user_id: defaultClient.id,
    chat_type: 'CLIENT',
    type: 'TEXT'
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message.toJSON())
      .query({company_id: 1})
      .loginVia(defaultClient)
      .end()

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)

  assert.equal(response.status, 200)
  assert.isNull(solicitationInDatabase.sla_end_date)
  assert.isNotNull(solicitationInDatabase.sla_start_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_ADMIN_RESPONSE')
})

test('should send message for PARTNER user and change solicitation status from WAITING_FOR_ADMIN_RESPONSE', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_PARTNER_RESPONSE',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss')
  })
  const message = await Factory.model('App/Models/Message').make({
    solicitation_id: solicitation.id,
    user_id: defaultPartner.id,
    chat_type: 'PARTNER',
    type: 'TEXT'
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message.toJSON())
      .query({company_id: 1})
      .loginVia(defaultPartner)
      .end()

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase.partner_sla_end_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_ADMIN_RESPONSE')
})

test('should send message for ADMIN user and change solicitation status from WAITING_FOR_PARTNER_RESPONSE', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_ADMIN_RESPONSE',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss'),
    partner_sla_end_date: moment().format('YYYY-MM-DD HH:mm:ss')
  })
  const message = await Factory.model('App/Models/Message').make({
    solicitation_id: solicitation.id,
    user_id: defaultAdmin.id,
    chat_type: 'PARTNER',
    type: 'TEXT'
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message.toJSON())
      .query({company_id: 1, chat_type: 'PARTNER'})
      .loginVia(defaultAdmin)
      .end()

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase.partner_sla_start_date)
  assert.isNull(solicitationInDatabase.partner_sla_end_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_PARTNER_RESPONSE')
})

test('should send message for ADMIN user and change solicitation status from WAITING_FOR_CLIENT_RESPONSE', async({ client, assert}) => {
  const solicitation = await Factory.model('App/Models/Solicitation').create({
    status: 'WAITING_FOR_ADMIN_RESPONSE',
    client_id: defaultClient.id,
    company_id: userCompany.id,
    partner_id: defaultPartner.id,
    sla_start_date: moment().format('YYYY-MM-DD HH:mm:ss')
  })
  const message = await Factory.model('App/Models/Message').make({
    solicitation_id: solicitation.id,
    user_id: defaultAdmin.id,
    chat_type: 'CLIENT',
    type: 'TEXT'
  })

  const response =
    await client
      .post(`solicitation/${solicitation.id}/message`)
      .send(message.toJSON())
      .query({company_id: 1, chat_type: 'CLIENT'})
      .loginVia(defaultAdmin)
      .end()

  const solicitationInDatabase = await SolicitationModel.find(solicitation.id)

  assert.equal(response.status, 200)
  assert.isNotNull(solicitationInDatabase.sla_end_date)
  assert.equal(solicitationInDatabase.status, 'WAITING_FOR_CLIENT_RESPONSE')
})

const _makeMessage = async (chatType, userId) => {
  const message = await Factory.model('App/Models/Message').make({
    solicitation_id: defaultSolicitation.id,
    user_id: userId,
    chat_type: chatType
  })
  return message.toJSON()
}
