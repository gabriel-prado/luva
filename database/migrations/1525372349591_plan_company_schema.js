'use strict'

const Schema = use('Schema')

class PlanCompanySchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.boolean('unlimited_balance').notNullable().defaultTo(false)
      table.float('plan_value')
    })
  }

  down () {
    this.table('companies', (table) => {
      table.dropColumn('unlimited_balance')
      table.dropColumn('plan_value')
    })
  }
}

module.exports = PlanCompanySchema
