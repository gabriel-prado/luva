'use strict'

const clearDataBase = use('Test/utils/ClearDataBase')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('SolicitationType - GET /Solicitation-type')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should list all solicitation types and return status 200', async({ client, assert}) => {
  const user = await Factory.model('App/Models/User').create({ type: 'ADMIN' })
  const solicitationTypes = await Factory.model('App/Models/SolicitationType').createMany(10)

  const response = await client.get('solicitation-type').query({ all: true }).query({company_id: 1}).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should not list solicitations types for not logged user', async ({ client, assert }) => {
  const response = await client.get('solicitation-type').end()

  assert.equal(response.status, 401)
})
